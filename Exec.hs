-- Program : Prawf; version 2020.001
-- Module : Exec (core)
-- Authors : O.Petrovska
-- Date created : 13 Jan 2020
-- Description : Main module for the execution mode

{- Revision History  :
Date        Author      Ref    Description
-}

module Exec

 where

-- haskell
import Text.Read (readMaybe)

-- core
import OpsemEC
import Program

-- aux
import Perhaps
import ListAux
import Parser

-- gen
import ReadShow
import Indentprint 

-- ==================================
-- Main application module (for Execution of programs)

-- Functions 
 -- em loadProgs getProg
 -- applyme applymetimes 
 -- showme runme runmestepbystep
 -- addapp applyme' getnum 
 -- listgen numgen
 -- applyme1
 
 -- Contents
-- ========
-- Main execution mode function
-- Functions for showing and execution
-- Number and list generator

-- Main execution mode function
-- ==================================
em :: IO()
em = 
   do {
       putStrLn ("Available programs: ");
       putStrLn ("=================== ");
       progrecs <- loadProgs "progs.txt"; 
       putStrLn (showAvailProgs progrecs);
       putStrLn (showL [
                        "------------------- ",
                        "To view a program type: showme \"programname\"",
                        "You can run a program by typing:",
                        "applyme \"programname\" [args]",
                        "To generate a program for a number x use numgen x",
                        "To generate a list containing numbers x and y use listgen [x, y]"
                       ])
      }
      
-- reads programs from the progs.txt file
loadProgs :: String -> IO [ProgRec]
loadProgs fn = 
  do { 
       mtext <- mReadFileFlex [fn,fn ++ ".txt"];
       case mtext of
          {
           Just text ->
             let { nelines = filter (not.null) (lines text); 
                   myprogs = map (runP0 parseProgRec) nelines;
                 }
             in return myprogs;
           _ -> return [] 
              }
      }
      
-- finds program by its label
getProg :: String -> [ProgRec] -> Maybe Program
getProg name progrec = getLR progrec name


-- Functions for showing and execution
-- ==================================
-- applyme "addition" [numgen 2, numgen 3]
applyme :: String -> [Program] -> IO ()
applyme s ps = 
   do progrec <- loadProgs "progs.txt" 
      case getProg s progrec of
       {
        Just p -> 
             do {
                 putStr ("Type s to run the program step by step OR" ++ 
                   "\nenter a number to execute the program for a certain number of steps >> ");
                 input <- getLine;
                 if input == "s"
                 then runmestepbystep p ps
                 else do n <- getnum input;
                         applyme' p n ps;
                 } ;
        Nothing -> putStrLn "Program not found."
       }

-- apply program + how many times 
-- applymetimes "addition" [numgen 2, numgen 3] 1000      
applymetimes :: String -> [Program] -> Int -> IO ()
applymetimes s ps n = 
   do progrec <- loadProgs "progs.txt" 
      case getProg s progrec of
       {
        Just p -> applyme' p n ps;
        Nothing -> putStrLn "Program not found."
       }

-- prints indented program
-- showme "addition" 
showme :: String -> IO ()
showme s = 
  do progrec <- loadProgs "progs.txt" 
     case getProg s progrec of 
      {Just p -> indentedProg p;
       Nothing -> putStrLn "Program not found."
      }

runme :: Program -> Int -> IO ()
runme n p = demo p n

runmestepbystep :: Program -> [Program] -> IO ()
runmestepbystep p ps = stepbystep 1 (addapp p ps)

addapp :: Program -> [Program] -> Program
addapp p [] = p
addapp p (q:qs) = addapp (ProgApp p q) qs

applyme' :: Program -> Int -> [Program] -> IO ()
applyme' p n ps = 
   let prog = addapp p ps
   in runme prog n
   
getnum :: String -> IO (Int)
getnum s = 
  let s' = (readMaybe s :: Maybe Int)
  in case s' of
       {Just i -> return (read (takeWhile (/= ' ') s) :: Int);
        _   -> do {
                   putStr ("Invalid input. Please enter s or an integer >> ");
                   input <- getLine;
                   getnum input;
                  }
       }

-- Number and list generators
-- ==================================
-- this can be extended for other types to be represented as programs

-- converts list of integers into valid programs
listgen :: [Int] -> Program
listgen [] = ProgCon Lt [ProgCon Nil []]
listgen (x:xs) = ProgCon Rt [ProgCon Pair [numgen x, listgen xs]]

-- converts ints into valid programs
numgen :: Int -> Program
numgen 0 = ProgCon Lt [ProgCon Nil []]
numgen n = ProgCon Rt [numgen (n-1)]

---------------------------------------------------
---------------------------------------------------
---------------------------------------------------

-- Functions for showing and execution for argument programs given as names
-- ==================================
-- example:  applyme "smtos" "shalf"    (given S(1/2), calculate  -S(1/2)"

applyme1 :: String -> [String] -> IO ()
applyme1 s pss = 
   do {
      progrec <- loadProgs "progs.txt" ;
      let u = getProg s progrec ;
          psm = map (\x -> getProg x progrec) pss ;
          ps = map (\x -> case x of {Just p -> p}) psm ;
      in 
      case u of
       {
        Just p -> 
             do {
                 putStr ("Type s to run the program step by step OR" ++ 
                   "\nenter a number to execute the program for a certain number of steps >> ");
                 input <- getLine;
                 if input == "s"
                 then runmestepbystep p ps
                 else do n <- getnum input;
                         applyme' p n ps;
                 } ;
        Nothing -> putStrLn "Program not found."
       }
       }
