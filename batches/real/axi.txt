ax1 . All x:R (All y:R (((x+y) - 1) = (x+(y-1)))) 
ax2 . All x:R (All y:R ((y= 0) -> ((x+y = x))))
ax3 . All z:R (All y:R (All x:R ((y + z) < x -> y < (x - z))))
ax4 . All x:R (All y:R (x = (x+y)-y)) 
ax5 . All x:R ((x-x) = 0)
ax6 . abs(0) = 0
ax7 . 0 < 1
ax8 . All x:R ((x*0) = 0)
ax9 . All x:R (((2*x)-1)-1=2*(x-1))
ax10 . All x:R (All y:R (x+y = y+x)) 
ax11 . All x:R (All y:R (x*y = y*x)) 
ax12 . All x:R (x+0 = x) 
ax13 . All x:R (x-0 = x) 
ax14 . All x:R (x*1 = x) 
ax15 . 1+1 = 2
ax16 . All x:R All y:R All z:R (y-z < x - z -> y < x)
ax17 . All x:R (x - 1 < x)
ax18 . All t:R All y:R All z:R (t < y -> t + z < y + z)
ax19 . All t:R All y:R All z:R (t < y -> y < z -> t < z)
ax20 . All x:R All y:R All z:R (x < y -> y < z -> x < z)
ax21 . All x:R All y:R (x-y)+y = x 
ax22 . All x:R x-x = 0
ax23 . All x:R All y:R All z:R (x-y)-z=(x-z)-y
ax24 . All x:R All y:R All z:R (x-y)-z=x-(y+z)
ax25 . All x:R All y:R All z:R x-(y-z)=(x+z)-y
ax26 . All x:R All y:R All z:R x-(y-z)=(x-y)+z
ax27 . All x:R All y:R All z:R (x+y)-z=x+(y-z)
ax28 . All x:R All y:R (x-y=0 -> x=y)
ax29 . All x:R All y:R All z:R (y-(x-z)=0 -> x-y=z)
ax30 . All x:R All y:R All z:R ((y-x)+z=0 -> x-y=z)
ax31 . All x:R All y:R (x-y)+y = x 
ax32 . All x:R All y:R All z:R (x-z)-(y-z) = x-y 
ax33 . All x:R All y:R All z:R (x+y)+z = x+(y+z) 
ax34 . All x:R All y:R All z:R (x*y)*z = x*(y*z) 
ax35 . All x:R All y:R All z:R x*(y+z)=(x*y)+(x*z) 
ax36 . All x:R All y:R All z:R x*(y-z)=(x*y)-(x*z) 

m1 . m*m=1
m2 . m*0= 0
m3 . m*1= m
m4 . All x:R All e:R abs ((2*(m*x))-e) =abs ((2*x)-(m*e)) 
m5 . All x:R (All e:R (2*(m*x))-e=m*((2*x)-(m*e)))
m6 . All x:R All y:R (x <= y -> not (y < x))
m7 . All x:R All y:R ((not (y < x)) -> x <= y)
m8 . All x:R (((m <= m*x) and (m*x <= 1)) -> ((m <= x) and (x <= 1)))
m9 . All x:R (m*x <= 0 -> 0 <= x)
m10 . All x:R (0 <= m*x -> x <= 0)
m11 . All x:R (m*x = 0 -> x = 0) 
m12 . All x:R All y:R ((y << x) -> (2*abs(x) <= 1 and y = 2*x))
m13 . All x:R All y:R ((2*abs(x) <= 1 and y = 2*x) -> (y << x))
m14 . All x:R ((not (x = 0)) -> Accp(x))

abs1 . All x:R All y:R abs(x) <= y -> m * y <= x and x <= y
claim11 . All x:R  2*x + 1 <= 1 -> x <= 0
claim12 . All x:R  x - m   = x + 1

claim13 . All x:R  m  <= 2*x - 1 -> 0 <= x
claim14.  All x:R 2*x <= 0 -> x <= 0
claim15.  All x:R 0 <= 2*x -> 0 <= x
claim16.  All x:R abs(2*x) = 2*abs(x)

claim21 . All x:R t(x) = 1-2*abs(x)
claim22 . All x:R x <= 0 -> t(x) = 2*x+1
claim23 . All x:R 0 <= x -> t(x) = 1-2*x
claim24 . All x:R abs(2*x+1) <= 1 -> m <= x & x <= 0
claim25 . All x:R abs(2*x-1) <= 1 -> 0 <= x & x <= 1
claim26 . All x:R All y:R All z:R (x*(y+z) = x*y + x*z)
claim27 . All x:R  x - 1   = x + m
claim28 . All x:R All y:R  x - y   = m*(y-x)
claim29 . All x:R 0 <= x  -> abs(x) = x
claim30 . All x:R x <= 0 -> abs(x) = m

claim31 . All x:R abs(2*x) <= 1 -> abs(2*t(x)-1)  <= 1
claim32 . All x:R 2*t(x) - 1 = t(2*x)
claim33 . All x:R x - 0 = x


claim34 .  All x:R All y:R All z:R  (x <= y and y  <= z ) -> x <= z
claim35.   All x:R All y:R x < y -> x <= y 
claim36 . All x:R  x - m   = x + 1
claim37 . m < 0
claim38 . All x:R abs (2*x) <=1 -> m <= x & x <= 1

s0 . abs ((2*0)-0) <=1
stoc1. All x:R  (m<=x & x<=0) -> abs (x) <=1
