-- Program : Prawf; version 2020.001
-- Module : Prover (core)
-- Authors : U.Berger, O.Petrovska
-- Date created : 14 Jan 2020
-- Description : Main module for the Prover mode

{- Revision History  :
Date        Author      Ref    Description
14 Jan 20    OP          1     commented out some removed or deprecated functions,
                               simplified interactions and removed duplicated code.
02 Feb 20    OP        prev 145c2a5  implemented experimental theorem loading,
                                     which should make loading quicker    
05 Feb 20    OP                      updated program saving, added comments with formula  
-}                                      

module Prover -- core

  where

-- haskell
import Control.Exception
import GHC.IO
import System.Process
import System.Directory
import Text.Read (readMaybe)
import Data.List (intercalate)

-- aux
import Perhaps    -- Control.Applicative
import ListAux as LA -- Data.List, Perhaps
import Parser   -- Control.Monad, Perhaps

-- core
import Language   -- Data.Map.Strict, Data.List, 
                  -- Control.Applicative, ListAux, MapAux, Perhaps
import Proof      -- Data.List, Perhaps, ListAux as LA, Language
import Extraction -- Data.List, Perhaps, ListAux, Program, 
                  -- Indentprint, Proof, Language

-- gen
import ParseLang  -- Control.Monad, Data.List, Data.Map.Strict,
                  -- Parser, MapAux, ListAux, Perhaps, Language
import PlProof    -- Data.List, Perhaps, ListAux, Language, Proof
import Step       -- Data.List, Perhaps, ListAux, Language, Proof,
                  -- Parser, PlProof, ParseLang, Occ 
import ReadShow   -- Indentprint,Perhaps, ListAux, Language, Proof,
                  -- Parser, PlProof, Step, ParseLang, Occ
import Langandctxpreload -- GHC.IO, System.Process, Data.List.Split,
                  -- Perhaps, ListAux, Language, Proof, Extraction
                  -- ParseLang, Parser, Step, ReadShow
import Indentprint 

-- ==================================
-- Main application module (for Prover)

-- controls the user's interaction with the system allowing the user to apply 
-- the rules of natural deduction in the backwards fashion
-- ==================================

-- Functions 
 -- prover
 -- loop useinput
 -- batchlist loadbatchloop loadbatch loadbatchnew
 -- preloadLangCtxProcess 
 -- addAxFProcess preloadAxiProcess
 -- modAxiProcess addAxiomProcess
 -- preloadDeclProcess
 -- preloadThmsProcess saveThmProcess
 -- displayState displayGoals displayLangCtx
 -- displayGoalInfo displayGlobalAxi displayGlobalDecls
 -- displayThms displayError displayProof
 -- displaytacsProcess displaytacProcess
 -- runtactics runtacProcess savetacProcess
 -- displayExPProcess extractProcess
 -- saveProofProcess checkProofProcess
 -- quitProcess newProcess helpProcess
 -- multi undostep

-- Contents
-- ========

-- * prover (main)
-- * main prover loop and user input
-- * loading and preloading functions
   -- batches
   -- batch content 
      -- (language, context, axioms, declarations, theorems)
-- * displaying functions
   -- display:    state
               -- goals
               -- language and context
               -- goal information
               -- global axioms
               -- global declarations
               -- theorems
               -- proof 
               -- tactics list
               -- tactic content
-- * working with tactics
-- * extracting programs (display and saving)
-- * saving proofs and theorems (ThmE and ThmI Versions)
-- * basic processes: quit, new and help
-- * minor helper functions


  
-- ============ MAIN ================================================
-- starts the prover
prover :: IO () 
prover = 
   do {
       let {state = startState lEmpty emptycontext};
       availb <- batchlist;
       putStr ("Type the batch code (" ++ availb ++ ") >>");
       loadbatchloop state
      }
    

-- ============ MAIN PROVER MODE LOOP and USER INPUT =======================================

loop :: (State,[(State,String)]) -> IO ()
loop sh@(state,his) =
   displayState state  >>
   case ips state of
     {
      QuitS -> quitProcess state ;
      ExProgS -> quitProcess state ;
      _     -> getLine >>= flip useinput sh >>= loop ;
     }

useinput :: String -> (State,[(State,String)]) -> IO (State,[(State,String)])
useinput s sh@(state,his) =
    case words s of
       {
         ("?":ws)       -> helptext ws >>= putStrLn >> return sh ; -- ReadShow 
         ("help":ws)    -> helptext ws >>= putStrLn >> return sh ; -- ReadShow 
         ("h":ws)       -> helptext ws >>= putStrLn >> return sh ; -- ReadShow
         ["?p"]         -> helptext ["?p"] >>= putStrLn >> return sh ; -- ReadShow
         ["?c"]         -> helptext ["?c"] >>= putStrLn >> return sh ; -- ReadShow
         ["quit"]       -> return (state {ips = QuitS},his) ; 
         ["exit"]       -> return (state {ips = QuitS},his) ;
         ["new"]        -> newProcess state ;
         ["set-goal"]   -> newProcess state ;
         ["check"]      -> checkProofProcess state >> return sh;
         ["langandctx"] -> displayLangCtx state >> return sh;
         ["lc"]         -> displayLangCtx state >> return sh;
         ["loaddecls"]  -> putStrLn "\nUsage: loaddecls batchname" >> return sh ;
         ["loaddecls",bn] -> preloadDeclProcess state 
                             ("batches/" ++ bn ++ "/decls.txt") >>= \s-> return (s,his);  
         -- loading axiom file axi.txt from batch bn
         ["addaxf"] -> putStrLn "\nUsage: addaxf batchname" >> return sh ;
         ["addaxf",bn] -> addAxFProcess state 
                       ("batches/" ++ bn ++ "/axi.txt") >>= \s-> 
                           return (s,his);
         -- adding an axiom, quick solution to be improved!
         -- addaxi <batchname> 
         ["addaxi"] -> putStrLn "\nUsage: addaxi batchname\n After that you will need to enter an axiom name and formula\n (e.g.: u . All (x:R) y=y)" >> return sh ;         
         ["addaxi",bn] -> do { state' <- addAxiomProcess state bn;
                               return (state',his)};
                               
         ["loadaxi"]    -> putStrLn "\nUsage: loadaxi batchname" >> return sh ;
         ["loadaxi",bn] -> preloadAxiProcess state 
                         ("batches/" ++ bn ++ "/axi.txt") >>= \s->  
                           return (s,his);
         ["loadthms"]    -> putStrLn "\nUsage: loadthms batchname" >> return sh ;
         ["loadthms",fn] -> preloadThmsProcess state fn >>= \s->  
                            return (s,his);
         -- experimental loading of theorems
         ["loadthmsB",bn] -> preloadThmsProcess' state ("batches/" ++ bn ++ "/theorems.txt") >>= \s->  
                            return (s,his);
         ["showthms"]   -> displayThms state >> return sh;
         ["showaxi"]    -> displayGlobalAxi state >> return sh;
         ["showdecls"]  -> displayGlobalDecls state >> return sh;
         ["showgoals"]  -> displayGoals state >> return sh;
         ["sgs"]        -> displayGoals state >> return sh;
         ["goalinfo"]   -> displayGoalInfo state >> return sh;
         ["saveproof",fn] -> saveProofProcess state fn >> return (state, his);
         ["showproof"]  -> displayProof state >> return sh;
         ["savethm"]    -> putStrLn "\nUsage: savethm fn thmn. Theorem name should only contain alphanumeric characters" >> return sh ;
         ["savethm",fn] -> putStrLn "\nPlease include both file name and theorem name: savethm fn thmn" >> return sh ;
         -- current way of saving theorems (label.formula.proofrecord)                       
         ["savethm",fn,thmn] -> saveThmProcess state fn thmn >> return sh ;
         -- experimental new way of saving theorems (label.formula.proof)
         -- to be used in a batch (prescribed name theorems.txt)
         ["savethmB",bn,thmn] -> saveThmProcess' state bn thmn >> return sh ;
         ["extract",pn]  -> putStrLn "\nWaiting for program to extract ..." >> 
                         extractProcess state pn >> return sh;     
         ["extract"]    -> putStrLn "\nWaiting for program to extract ..." >>
                           displayExPProcess state >> 
                           putStrLn "\nTo save this program into your progs.txt, type \"extract\" followed by a program name"
                             >> return sh; 
         ["savetac"]    -> putStrLn "\nType \"savetac\" followed by file name"
                            >> return sh;
         ["savetac",fn] -> savetacProcess sh fn >>
            return (state, his) ;
         ["runtac"]     -> 
            putStrLn "\nType \"runtac\" followed by file name" >> return sh;
         ["runtac",fn]  -> runtacProcess sh fn;
         ["showtacs"]   -> displaytacsProcess >> return sh;
         ["showtac"]    -> putStrLn "\nType \"showtac\" followed by file name (without file extension) to view a specific tactic"
                            >> return sh;
         ["showtac",fn] -> displaytacProcess fn >> return sh;
         ["exec"]       -> return (state {ips = ExProgS},his) ; 
         ["undo"]       -> return (undostep sh);
         ["undo",n]     -> let {
                                 ns = words n;
                                 n' = head ns; 
                                 numcheck = readMaybe n' :: Maybe Int
                                }                                
                            in case numcheck of
                                 {
                                  Just n'' -> return (multi n'' undostep sh);
                                  Nothing -> return (undostep sh)
                                 };
         _              -> return (steps (str2Inputs s) state, (state,s):his) 
       }

-- ============ LOADING AND PRELOADING FUNCTIONS =============================

-- =========== BATCHES ================================ 
-- a batch is used to create proof environment
-- it consists of
  -- * language
  -- * context
  -- * declarations
  -- * axioms 
-- available batches are stored in the batches folder

-- returns a list of available batches         
batchlist :: IO String
batchlist = 
  do {
      batches <- getDirectoryContents "batches/";
      let {blist = [x | x <- batches, x /= ".", x /= ".."];
           availb = intercalate ", " blist};
      return availb
    }
  
-- initial batch loading 
loadbatchloop :: State -> IO()
loadbatchloop state = 
   do {
       state' <- loadbatch state;
       loop (state',[])
       }

loadbatch :: State -> IO State       
loadbatch state =
  do { 
      input <- getLine;
      batches <- getDirectoryContents "batches/";
      let {blist = [x | x <- batches, x /= ".", x /= ".."];
           b = input `elem` blist};
      if b 
      then do { state' <- preloadLangCtxProcess state
                ("batches/" ++ input ++ "/lang.txt") 
                ("batches/" ++ input ++ "/ctx.txt");
                state'' <- preloadDeclProcess state' 
                ("batches/" ++ input ++ "/decls.txt");
                state'''<- preloadAxiProcess state''
                ("batches/" ++ input ++ "/axi.txt");
                putStr ("\n\nThe following is loaded: \n");
                displayLangCtx state''';
                displayGlobalAxi state''';
                displayGlobalDecls state''';
                tacfiles <- getDirectoryContents ("batches/" ++ input);
                if "theorems.txt" `elem` tacfiles -- checks if there are theorems associated with the batch
                then do {
                         state'''' <- preloadThmsProcess' state''' 
                           ("batches/" ++ input ++ "/theorems.txt"); -- loading from the batch
                         return state''''
                        }
                else return state'''
               }
      else do {
               putStrLn "A batch with this name is not available, please enter a valid batch name>>";
               loadbatch state
              }
      }

-- batch loading process for newProcess
loadbatchnew :: State -> String -> IO (State,[(State,String)])
loadbatchnew state availb = 
  do { putStr ("Type the batch code (" ++ availb ++ ") >>");
       state' <- loadbatch state;
       return (state',[])
     }
       
-- =============== BATCH CONTENT ===============================

-- preload language and context 
preloadLangCtxProcess :: State -> String -> String -> IO State
preloadLangCtxProcess state lfn cfn = 
   do { 
       text <- readFile lfn;
       text' <- readFile cfn;
       let { 
             l = concat $ reducelines $ filter (not.null) (map remcomment (lines text)); 
             ctx = concat $ reducelines $ filter (not.null) (map remcomment (lines text'));
             parsedlang = runP0 planguage l;  -- planguage in Langandctxpreload.hs 
             parsedctx = runP0 pcontext ctx; -- pcontext in Langandctxpreload.hs 
             state' = state {language = parsedlang,
                             stcontext = parsedctx}        
            }
       in return state'
      }

addAxFProcess, preloadAxiProcess :: State -> String -> IO State
addAxFProcess = modAxiProcess True
preloadAxiProcess = modAxiProcess False
                     
-- adding multiple axioms from a file
modAxiProcess :: Bool -> State -> String -> IO State
modAxiProcess b state fn = 
  do { 
       mtext <- mReadFileFlex [fn,fn ++ ".txt"] ;
       case mtext of
         {
           Just text ->
             let { 
                   assu = filter (not.null) (map remcomment (lines text)) ; 
                   l = language state ;
                   c = stcontext state ;
                   parsedassu = map (runP0 (parseAF l c)) assu ;  
                   plp = stplproof state ;
                   passu = if b then plpassumpts plp else [] ;
                   plp' = plp {plpassumpts = (parsedassu ++ passu)} ;
                   state' = (state {stplproof = plp'})             
                 }
             in return state';
           _ -> return (state {output = ErrorO FileNotFoundE}) 
         }
     }

-- adding a single axiom
addAxiomProcess :: State -> String -> IO State
addAxiomProcess state bn =
   do {
       putStrLn ("Enter axiom name and formula (e.g.: u . All (x:R) y=y) >>");
       anax <- getLine;
       let {fn = "batches/" ++ bn ++ "/axi.txt"};
       appendFile fn ("\n" ++ anax);
       let {l = language state ;
            c = stcontext state;
            newax = runP0 (parseAF l c) anax; 
            plp = stplproof state ;
            passu = plpassumpts plp;
            plp' = plp {plpassumpts = (passu ++ [newax])};                   
            state' = (state {stplproof = plp'})};
       putStrLn (showL ["Updated assumption list",
                      (showAssumpts (plpassumpts plp'))]);
       return state'
       
      }

preloadDeclProcess :: State -> String -> IO State
preloadDeclProcess state fn = 
   do { 
       mtext <- mReadFileFlex [fn,fn ++ ".txt"];
       case mtext of
         {
           Just text -> 
             let { 
                   decls = filter (not.null) (lines text); 
                   l = language state;
                   ctx = stcontext state;
                   (parseddecls,l',ctx') = parseDs l ctx decls; 
                   plp = stplproof state;
                   plpdeclars = plpdecls plp;
                   plp' = 
                     plp {plpdecls = (parseddecls ++ plpdeclars)};
                   state' = state { 
                                    language = l', 
                                    stcontext = ctx', 
                                    stplproof = plp'
                                  }             
                 }
             in do {
                     putStr ("Available declarations:\n" ++ 
                             showDecls (plpdecls plp') ++ "\n");
                     return state'
                   };
           _ -> return (state {output = ErrorO FileNotFoundE})  
         }
      }
       
-- preloading theorems into the partial proof   
preloadThmsProcess :: State -> String -> IO State
preloadThmsProcess state fn = 
   do { 
       mtext <- mReadFileFlex [fn,"theorems/" ++ fn ++ ".txt"];
       case mtext of
         {
           Just text ->
             let { nelines = filter (not.null) (lines text); 
                   l = language state ;
                   c = stcontext state ;
                   newthmsE = map (runP0 (parseThmE l c)) nelines;
                   newthmsI = thmE2thmI newthmsE;
                   plp = stplproof state;
                   thms = plpthms plp;
                   plp' = plp {plpthms = newthmsI ++ thms};
                   state' = state {stplproof = plp'}             
                  }
             in do {putStr ("Available thms:\n" ++
                             showThms (plpthms plp'));
                    return state' 
                   };
           _ -> return (state {output = ErrorO FileNotFoundE}) 
         }
      }

preloadThmsProcess' :: State -> String -> IO State
preloadThmsProcess' state bn = 
   do { 
       mtext <- mReadFileFlex [bn,"batches/" ++ bn ++ "/theorems.txt"];
       case mtext of
         {
           Just text ->
             let { 
                   nelines = reducelines $ filter (not.null) (map remcomment (lines text)); 
--                   nelines = filter (not.null) (map remcomment (lines text)); 
                   l = language state ;
                   c = stcontext state ;
                   newthmsR = map (runP0 parseThmR) nelines;
                   newthmsI = thmR2thmI newthmsR;
                   plp = stplproof state;
                   thms = plpthms plp;
                   plp' = plp {plpthms = newthmsI ++ thms};
                   state' = state {stplproof = plp'}             
                  }
             in do {putStr ("Available thms:\n" ++
                             showThms (plpthms plp'));
                    return state' 
                   };
           _ -> return (state {output = ErrorO FileNotFoundE}) 
         }
      }                                                  
               
-- ============ DISPLAYING FUNCTIONS=========================================    
    
displayState :: State -> IO ()
displayState state =  
  putStr (showState state ++ (case ips state of { QuitS -> ""; _-> "> "}))  
     
displayGoals :: State -> IO ()
displayGoals state = 
  do {putStrLn ("=========================== \n" ++
                 showGoals (plpglstack (stplproof state)))     
     }
     
displayLangCtx :: State -> IO ()
displayLangCtx state =
  do {putStrLn ("=========================== \n" ++
                 showLang (language state) ++ 
                 "\n CONTEXT (global) : " ++ 
                 showCtx (stcontext state))
     }
     
displayGoalInfo :: State -> IO ()
displayGoalInfo state =
  do {putStrLn ("=========================== \n" ++
                 showPerhapsGoal (currentGoal (stplproof state)))
     }     
                 
displayGlobalAxi :: State -> IO ()
displayGlobalAxi state =
  do {putStrLn ("=========================== \n Axioms / global assumptions: \n" ++
                 showAssumpts (plpassumpts ((stplproof state))))}
  
displayGlobalDecls :: State -> IO ()
displayGlobalDecls state =
  do {putStrLn ("=========================== \n Global declarations: \n" ++
                 showDecls (plpdecls ((stplproof state))))}    
                 
displayThms :: State -> IO ()
displayThms state =
  do {putStrLn ("=========================== \n Available theorems: \n" ++
                 showThms (plpthms (stplproof state)))}               
                 
displayProof :: State -> IO ()
displayProof st =
   let {
        plp = stplproof st;
        proof = plpproof plp;
       }
   in do {
          putStrLn ("\n================================ \nProof: \n================================ \n" ++
          showPlp plp)
         } 
    
-- shows all tactics available in the tactics folder
displaytacsProcess :: IO ()
displaytacsProcess = 
  do {
      tacfiles <- getDirectoryContents "tactics/";
      let {
           tacslist = showL tacfiles;
           tacnames = [(take ((length x)-4) x) |
             x <- tacfiles, x /= ".", x /= "..", 
             not ('.' `elem` (take ((length x)-4) x)) ];
           tacswf = [(x ++ " : ") | x <- tacnames];
           tacswf' = [(x,get1stline x) | x <- tacnames]
           };
      putStrLn ("===================\nAvailable tactics: ");
      st2e' tacswf';
      putStrLn ("To view a specific tactic type showtac <tacname>");
     }

-- shows the contents of a specific tactic file     
displaytacProcess :: String -> IO ()
displaytacProcess name = 
  do { tacfiles <- getDirectoryContents "tactics/";
       if (name ++ ".txt") `elem` tacfiles
       then do {tf <- readFile ("tactics/" ++ name ++ ".txt") ; 
                let {tactext = lines tf};
                putStr("===================\nTactic: " ++ show name ++ showL tactext);
               }
       else do {putStr("===================\nUnable to show " ++ name ++ ". Tactic not found in the tactics directory. \nType showtacs to see available tactics.\n");
               }       
       }    

-- ============ WORKING WITH TACTICS ===========================================  

runtactics :: [String] -> (State,[(State,String)]) -> IO(State,[(State,String)]) 
runtactics [] sh = let state = fst sh 
                   in do {
                          displayProof state;
                          return sh}
runtactics (t:ts) sh = useinput t sh >>= runtactics ts

-- re-plays a tactic (safe version, checking that the file exists)
runtacProcess :: (State,[(State,String)]) -> String -> IO (State,[(State,String)])
runtacProcess sh@(state,his) name = 
  do { tacfiles <- getDirectoryContents "tactics/";
       if (name ++ ".txt") `elem` tacfiles
       then do {
                tf <- readFile ("tactics/" ++ name ++ ".txt") ; 
                let {tactext = lines tf; provenf = head tactext;};
                putStr("===================\nRunning proof of: \n    " ++ show provenf ++ "\n...\n");
                runtactics (lines tf) (state, []);
               }
       else do {
                putStr("===================\nUnable to run " ++ name ++ ". Tactic not found in the tactics directory. \nType showtacs to see available tactics or enter your goal formula below\n");
                runtactics [] sh;
               }       
       }

savetacProcess :: (State,[(State,String)]) -> String -> IO ()
savetacProcess sh@(state, his) name = 
  let {tactext = reverse (map snd his);
       provenf = head tactext} 
  in do { dircheck <- doesDirectoryExist "tactics"; 
         createDirectoryIfMissing dircheck ("tactics");
         writeFile ("tactics/" ++ name ++ ".txt") (unlines tactext); -- (unlines (reverse (map snd his)));
         putStr("===================\nTactic for proving: \n    " ++ show provenf ++ "\nis saved into the tactics directory.\n");         
       }
       
-- ============ EXTRACTING PROGRAMS ===================================================

-- extracts a program and displays a simplified version of it 
-- without saving into the progs file
displayExPProcess :: State -> IO ()
displayExPProcess st =
  let {
       l = language st;
       ctx = stcontext st;
       plp = stplproof st;
       decls = plpdecls plp;
       axis = plpassumpts plp;
       proof = plpproof plp;
       program = pe l ctx decls axis proof;
       programsimp = extract l ctx decls axis proof
      }
  in do {
         checkifextracted "simplified" programsimp; 
         case programsimp of 
             { Success prog -> putStr ("Extraction completed.\n");
               Failure msg -> 
                   putStr ("Prover.hs displayExPProcess:\n" ++
                           msg ++ "\n" ++
                           "Program failed to extract.")      
             }
           }

-- extracts full program, converts it into simplified and saves both version to progs.txt
extractProcess :: State -> String -> IO ()
extractProcess st name = 
  let {
       l = language st;
       ctx = stcontext st;
       plp = stplproof st;
       decls = plpdecls plp;
       axis = plpassumpts plp;
       proof = plpproof plp;
       program = pe l ctx decls axis proof;
       programsimp = extract l ctx decls axis proof
      }
  in do {
         checkifextracted "full" program; 
         checkifextracted "simplified" programsimp; 
         case programsimp of 
             { Success prog -> 
                  do { let {
                            fullprog = drop 8 (show program);
                            simpprog = drop 8 (show programsimp)};
                       appendFile "progs.txt" 
                         ("\n\n" ++ name ++ "fp . " ++ fullprog ++
                          "\n\n" ++name ++ "sp . " ++ simpprog);
                       putStr (showL ["Records generated:",
                          (name ++ "fp") , (name ++ "sp")])
                     };
                Failure msg -> 
                   putStr ("Prover.hs extractProcess:\n" ++
                           msg ++ "\n" ++
                           "Since program failed to extract, no program added to progs database.")      
             }
           }
           
-- ============ SAVING PROOFS AND THEOREMS  =====================================
               
--saving a proof log
-- for internal use (to be substituted by theorems in future versions)
saveProofProcess :: State -> String -> IO ()
saveProofProcess st name = 
  let {l = language st;
       ctx = stcontext st;
       plp = stplproof st;
       plpaxi = plpassumpts plp;
       decls = plpdecls plp;
       proof = plpproof plp;
       proofrec = PrRec {prlang = l, prctx = ctx, praxi = plpaxi,
                         prdecl = decls, prproof = proof };
      }
  in do {
         dircheck <- doesDirectoryExist "plogs"; 
         createDirectoryIfMissing dircheck ("plogs");
         writeFile ("plogs/" ++ name ++ "_pview.txt") 
                   (showL [showLang l, 
                         "\nContext:", showCtx ctx, 
                          "\nAssumptions:", showAssumpts plpaxi, 
                          "\nDeclarations:", showDecls decls, 
                          "\nProof:", show proof]);
         writeFile ("plogs/" ++ name ++ "plog.txt") (show proofrec);
         putStr ("\nFiles :\n  " ++ name ++ "_pview.txt \n  " ++ 
          name ++ "_plog.txt\n are saved into the plogs directory.\n")
        }

-- checks if proof is correct        
checkProofProcess :: State -> IO()
checkProofProcess st = 
  let {l = language st;
       ctx = stcontext st;
       plp = stplproof st;
       plpaxi = plpassumpts plp;
       decls = plpdecls plp;
       proof = plpproof plp;
       proof' = letsProof decls proof;
      }
  in do {
         case (endFormula' l ctx decls plpaxi proof') of
           { 
            Success f -> putStr ((indentf 0 f) 
                      ++ "The proof of the above is correct.");
            Failure s  -> putStr ("Incorrect proof: " ++ s)
           }
        }     
       
-- saving a theorem into a file (ThmE)
saveThmProcess :: State -> String -> String -> IO ()
saveThmProcess st fn thmn = 
     let {
           l = language st ;
           c = stcontext st ;
           plp = stplproof st ;
           ans = plpassumpts plp ;
           d = plpproof plp;
           ansd = [ (u,f) | (u,f) <- ans, 
                             u `elem` freeassumptlabels d ];
           decls = plpdecls plp ; 
           prec = PrRec { prlang = l, prctx = c, praxi = ansd, 
                          prdecl = decls, prproof = d }           
         }
     in case endFormula' l c decls ans d of
          {
            Success f -> 
              let { thm = (thmn, (f, prec)) }
              in if take 4 (reverse fn) == "txt."
                 then do { 
                           appendFile ("theorems/" ++ fn) 
                                      ("\n\n" ++ showThmE thm) ;
                           putStr ("Theorem "  ++ thmn ++ 
                                   " is added to " ++ fn);
                         }
                 else do {
                           appendFile ("theorems/" ++ fn ++ ".txt") 
                                      ("\n\n" ++ showThmE thm) ;
                           putStr ("Theorem " ++ thmn ++ 
                                   " added to " ++ fn ++ ".txt");
                         };
            Failure str -> fail ("saveThmProcess:\n" ++ 
                                 show st ++ "\n" ++ str)
          } 

-- new way of saving theorems, as above but saved into a specific batch in theorems.txt 
saveThmProcess' :: State -> String -> String -> IO ()
saveThmProcess' st bn thmn = 
     let {
           l = language st ;
           c = stcontext st ;
           plp = stplproof st ;
           ans = plpassumpts plp ;
           d = plpproof plp;
           ansd = [ (u,f) | (u,f) <- ans, 
                             u `elem` freeassumptlabels d ];
           decls = plpdecls plp ; 
           p = plpproof plp;         
         }
     in case endFormula' l c decls ans d of
          {
            Success f -> 
              let { thmrec = ThmRec {tn = thmn, 
                                     tf = f, 
                                     tp = p};}
                   -- thm = (thmn, thmrec) }
              in do { 
                      appendFile ("batches/" ++ bn ++ "/theorems.txt") 
                                 ("\n\n" ++ show thmrec) ;
                      putStr ("Theorem "  ++ thmn ++ 
                              " is added to the batch " ++ bn);
                    };
            Failure str -> fail ("saveThmProcess':\n" ++ 
                                 show st ++ "\n" ++ str)
          } 
-- ============ QUIT, NEW AND HELP ===========================================

quitProcess :: State -> IO ()
quitProcess _ = return ()

newProcess :: State -> IO (State,[(State,String)])
newProcess state = 
  do {
      putStr ("\nKeep language, context, axioms and declarations? y/n >> ");
      input <- getLine;
      if input == "n"
      then let {state' = startState lEmpty emptycontext}
           in do { availb <- batchlist;
                   loadbatchnew state' availb}
      else do {
                let { state' = mkNewState state startFormula } ;
                displayLangCtx state';
                displayGlobalAxi state';
                displayGlobalDecls state';
                putStr("Loaded. \n");
                return (state',[])
              }
    }

helpProcess :: IO ()
helpProcess = 
 do { 
     putStrLn "Please consult the User Manual for full information.";
     putStr fullhelp
    }      
    
-- ============ HELPER FUNCTIONS ================================================
    
-- used for applying undo multiple times 
multi :: Int -> (a -> a) -> a -> a
multi 0 f x = x
multi n f x = multi (n-1) f (f x)
   
undostep :: (State,[(State,String)]) -> (State,[(State,String)])
undostep sh@(state,his) = case his of
              { 
               ((state0,t):his') -> (state0,his') ;
               []                -> (mkStartState0' state,[])
              } ;