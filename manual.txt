<?p>   
Proof commands:
type help commandname for details, e.g. help andi
---------------
use u       use assumption labeled u
usw u       use assumption labeled u to derive goal using elimination rules
ust <name>  use theorem; theorem formula should match the goal formula 
ustw <name> use theorem labelled <name> to derive goal using elimination rules
andi        and introduction 
andel f     and elimination left, f is the missing formula 
ander f     and elimination right, f is the missing formula 
                                                                       
impi u      implication introduction, u is a fresh label for the assumption
impe        implication elimination 
                                                                        
oril        disjunction introduction left 
orir        disjunction introduction right 
ore f       disjunction elimination, f is the missing disjunction
                                                                       
ora u       disjunction elimination, u is an assumption label of a disjunction
efq         ex-falso-quodlibet 
raa         reductio-ad-absurdum 
alli        forall introduction 
alle t      forall elimination, every occurrence of t is generalised
falle f     forall elimination, f is the generalised formula
exi t       exists introduction, t is the witnessing term
exe f       exists elimination, f is the missing existential formula
leti        let 'introduction'
ind         induction 
hsind       half-strong induction
sind        strong induction
cl          closure 
coind       coinduction 
hscoind     half-strong coinduction
scoind      strong coinduction
cocl        coclosure 
refl        reflexivity of equality 
sym         symmetry of equality 
cong f      new goal = f which must be same as old goal up to equations (to be proven)
congf f     same as cong f but without equations in terms
congru p eqs  eqs = s=t ..., old goal = p(s,...), new goals = p(t,...) plus eqs
norm f      set goal formula to f which must be same as current goal up to normalization
unfold op   unfold operator op
decl d      adds a new local declaration d
</?p>

<?c>
Control commands:
-----------------
new            start a new proof (current attempt is discarded)
set-goal       same as the new command
undo           go back one step
undo n         go back n steps
check          checks correctness of the proof
loadlang       load language
addaxf <bn>    add axioms from the axi.txt from the batch named <bn> 
addaxi <bn>    add a new axiom to a batch <bn>
loadaxi <bn>   like addaxi but existing axioms are deleted
loaddecls <bn> load global declarations from the batch <bn> 
loadthms <fn>  load theorems from the file named <fn> 
showgoals      show all open goals (brief sgs)
goalinfo       show local context, assumptions and declarations of the current goal
langandctx     show the current language and context (brief lc)
showaxi        show all global assumptions/axioms 
showdecls      show all global declarations
saveproof      save a proof record
showproof      display current state of the proof
showthms       shows all available theorems
               (only shows the theorems that have been loaded earlier)
showtacs       display available tactics from the tactics folder
showtac <name> display contents of a tactic
savetac <name> save proof tactic in a separate file
runtac <name>  run tactic from a separate file
savethm <fn> <thmn>  
               saves proof as theorem named thmn in file named fn
               Theorems in the same file have to be created in the same batch
extract <name> extracts a program, which can be loaded and run from the exec mode
exec           switch to execution mode
quit           leave the prover mode
</?c>

<use>
     
use assumption labelled u

                                       
                        
</use>

<usw>
     
use assumption labeled u to derive goal using elimination

                                        
                        
</usw>

<ust>
use theorem labeled <name> to derive goal using elimination rules
</ust>

<andi>
    
and introduction:

  Gamma |- A     Gamma |- B
--------------------------------  andi
       Gamma |- A & B

is shorthand for


  Gamma1 |- A1     Gamma2 |- A2
--------------------------------  andi
       Gamma |- A

with the conditions:

  A =alpha= A1 & A2,  
  Gamma1 subset Gamma,
  Gamma2 subset Gamma
</andi>

<andel>
       
and elimination left:

       Gamma |- A & B    
--------------------------------  andel B
       Gamma |- A

is shorthand for

      Gamma1 |- C1    
-------------------------------- andel B
       Gamma |- C

with the conditions:

  C1 alpha= A & B,  
  Gamma1 subset Gamma
</andel>

<ander>
       
and elimination right:

       Gamma |- A & B    
-------------------------------- ander A
       Gamma |- B
        

is shorthand for
         
                                                             

      Gamma1 |- C1    
-------------------------------- ander A
       Gamma |- C
          

with the conditions:

  C1 alpha= A & B,  
  Gamma1 subset Gamma
</andel>

<oril>
    
or introduction left

           Gamma |- A   
-------------------------------- oril
         Gamma |- A v B

is shorthand for


          Gamma1 |- A1  
-------------------------------- oril
           Gamma |- C

with the conditions:

  A1 =alpha= A 
  C  =alpha= A v B, 
  Gamma1 subset Gamma
</oril>

<orir>
    
or introduction right

           Gamma |- B   
-------------------------------- orir
         Gamma |- A v B

is shorthand for


          Gamma1 |- A1  
-------------------------------- orir
           Gamma |- C

with the conditions:

  A1 =alpha= B 
  C  =alpha= A v B, 
  Gamma1 subset Gamma
</orir>

<ore>
         
or elimination

 Gamma |- A v B   Gamma |- A -> C  Gamma |- B -> C 
-------------------------------------------------- ore A v B
                  Gamma |- C

is shorthand for

 Gamma1 |- A1   Gamma2 |- A2  Gamma3 |- A3
-------------------------------------------------- ore A1
                  Gamma |- A

with the conditions:

  A  =alpha= C,
  A1 =alpha= A v B, 
  A2 = alpha A -> C, 
  A3 =alpha= B -> C, 
  Gamma1 subset Gamma,
  Gamma2 subset Gamma,
  Gamma3 subset Gamma
</ore>

        
        
                                                                              

                                                               
                                                                        
                                       
         


<ora>
      
or elimination (called with assumption label instead of a formula)

 Gamma |- A v B   Gamma |- A -> C  Gamma |- B -> C 
-------------------------------------------------- ora (u:A v B)
                  Gamma |- C

is shorthand for

 Gamma1 |- A1   Gamma2 |- A2  Gamma3 |- A3
-------------------------------------------------- ora (u:A1)
                  Gamma |- A

with the conditions:

  A  =alpha= C,
  A1 =alpha= A v B, 
  A2 = alpha A -> C, 
  A3 =alpha= B -> C, 
  Gamma1 subset Gamma,
  Gamma2 subset Gamma,
  Gamma3 subset Gamma
</ora>

<impi>
      
implication introduction

       Gamma, u:A |- B   
-------------------------------- impi (u,A)
       Gamma |- A -> B

is shorthand for


      Gamma1 |- B    
-------------------------------- impi (u,A)
       Gamma |- C

with the conditions:

  C =alpha= A -> B,  
  Gamma1 subset Gamma union (u:A)
</impi>

<impe>
      
implication elimination

  Gamma |- A -> B    Gamma |- A   
-------------------------------- impe A
           Gamma |- B
       

is shorthand for
         
                                                                  

                                                                 
                                                                         
                                       
          

  Gamma1 |- A1    Gamma2 |- A2  
-------------------------------- impe A
       Gamma |- C

with the conditions:

  A1 =alpha=  A->B,
  A2 =alpha= A, 
  C  =alpha= B, 
  Gamma1 subset Gamma,
  Gamma1 subset Gamma
</impe>

<efq>
   
ex falso quodlibet 

             Gamma |- bot 
--------------------------------------  efq
             Gamma |- A
</efq>

<raa>
   
reductio ad absurdum

     Gamma |- (A -> bot) -> bot 
--------------------------------------  raa
             Gamma |- A
</raa>

<alli>
    
all introduction

       Gamma |- A   
--------------------------------  alli (x:s)
       Gamma |- All (x:s) A

is shorthand for


      Gamma1 |- C1    
--------------------------------  alli (x:s)
       Gamma |- C

with the conditions:

  C =alpha= All (y:s) A
  x not in FV(Gamma1)   (possibly and preferrably x = y)
  C1 =alpha= A[x/y]  
  Gamma1 subset Gamma 
</alli>

<alle>
      
all elimination

       Gamma |- All (x:s) A(x)
--------------------------------  alle t
       Gamma |- A(t)
       

is shorthand for
                
                                                                 

      Gamma1 |- C1    
--------------------------------  alle t
       Gamma |- C
        

with the conditions:

  C1 =alpha All (x:s) A
  C =alpha= A(t), x is instantiated with t 
  Gamma1 subset Gamma 
</alle>

<falle>
falle f 
forall elimination, f is the generalised formula
to see the forall elimination rule check help alle.
</falle>

<exi>
     
existence introduction

       Gamma |- A(t)
--------------------------------  exi t
       Gamma |- Ex (x:s) A(x)

is shorthand for

      Gamma1 |- C1    
--------------------------------  exi t
       Gamma |- C

with the conditions:

  C1 =alpha= A(t)
  C =alpha= Ex (x:s) A(x)
  Gamma1 subset Gamma 
</exi>

<exe>
existence elimination

  Gamma |- Ex x A(x)  Gamma |- All x (A(x) -> B)
------------------------------------------------  exe Ex x A(x)
       Gamma |- B
      

is shorthand for

           Gamma1 |- A1      Gamma2 |- A2
------------------------------------------------  exe A1
                    Gamma |- A
                                                                

with the conditions:
                                                             
                                 
         

  A =alpha= B
  A1 =alpha= Ex (x:s) A(x)
  A2 =alpha= All (x:s) (A(x) -> B), x is not free in B
  Gamma1 subset Gamma,
  Gamma2 subset Gamma  
</exe>

<ind>
induction

      Gamma |- Phi(P) subseteq P
------------------------------------  ind
      Gamma |- Mu Phi subseteq P

is shorthand for

           Gamma1 |- A1 
------------------------------------- ind
            Gamma |- A

with the conditions:

  A =alpha= Mu Phi subseteq P,
  A1 =alpha= Phi(P) subset P,
  Gamma1 subset Gamma,
  Phi is strictrly positive  
</ind>


<hsind>
induction

  Gamma |- Phi(P \cap (Mu Phi)) subseteq P
--------------------------------------------  hsind
           Gamma |- Mu Phi subseteq P
 
</hsind>


<sind>
induction

  Gamma |- (Phi(P) \cap (Mu Phi)) subseteq P
-------------------------------------------__  sind
      Gamma |- Mu Phi subseteq P
 
</sind>

<coind>
coinduction

      Gamma |- P subseteq Phi(P) 
------------------------------------  coind
      Gamma |- P subseteq Nu Phi

is shorthand for

           Gamma1 |- A1 
------------------------------------- coind
            Gamma |- A

with the conditions:

  A =alpha= P subseteq Nu Phi,
  A1 =alpha= P subseteq Phi(P),
  Gamma1 subset Gamma,
  Phi is strictrly positive  
</coind>

<hscoind>
Half strong induction 


 Gamma |- P subseteq (Phi(P)) union (Nu Phi)) 
---------------------------------------------  hscoind
      Gamma |- P subseteq Nu Phi


</hscoind>

<scoind>
Strong induction 


 Gamma |- P subseteq Phi(P union (Nu Phi)) 
---------------------------------------------  scoind
      Gamma |- P subseteq Nu Phi


</scoind>

<cl>
closure

--------------------------------------  cl
 Gamma |- Phi (Mu Phi) subseteq Mu Phi

with the condition:

  Phi is strictrly positive  
</cl>

<cocl>
coclosure

--------------------------------------  cocl
 Gamma |- Nu Phi subseteq Phi (Nu Phi) 

with the condition:

  Phi is strictrly positive  
</cocl>

<refl>
reflexivity of equality

--------------------------------------  refl
             Gamma |- t = t 
</refl>

<sym>
symmetry of equality

             Gamma |- s = t
--------------------------------------  sym
             Gamma |- t = s 
</sym>

<cong>
congruence of equality

cong f
new goal = f which must be same as old goal up to equations (to be proven)

   Gamma |- A [s/x]   Gamma |- s = t
--------------------------------------  cong A
             Gamma |- A [t/s] 

s = t stands for all the substitutions that are done

with the condition:
   sorts if s and t should match
</cong>

<congf>
congruence of equality

congf f
same as cong f but without equations in terms

   Gamma |- A [s/x]   Gamma |- s = t
--------------------------------------  cong A
             Gamma |- A [t/s] 

s = t stands for all the substitutions that are done

with the condition:
   sorts if s and t should match
</congf>

<congru>
congruence of equality

congru p eqs
eqs = s=t ..., old goal = p(s,...), new goals = p(t,...) plus eqs

   Gamma |- A [s/x]   Gamma |- s = t
--------------------------------------  cong A
             Gamma |- A [t/s] 

s = t stands for all the substitutions that are done

with the condition:
   sorts if s and t should match
</congru>

<norm>
norm A normalises the formula A.
</norm>

<decl>
decl d adds a new declaration to the proof environment.
</decl>

<unfold>
unfold Phi unfolds the definition of Phi.
</unfold>

<leti>
strips let formulas of declarations and adds the declarations to the context.
</leti>

-- sequent calculus

<andleft>
AndLeft (a sequent calculus rule)

       Gamma, u:A, v:B |- C   
-------------------------------- AndLeft(A,B,u,v,w)
       Gamma, w:A&B |- C

is shorthand for

      Gamma1 |- C1    
-------------------------------- AndLeft(A,B,u,v,w)
       Gamma |- C

with the conditions:

  C1 =alpha= C,
  w:A&B in Gamma,
  Gamma1 subset Gamma union {(u:A),v:B)}
</andleft>

<impleft>
ImpLeft (a sequent calculus rule)

  Gamma, u:B |- C   Gamma |- A 
----------------------------------- AndLeft(A,B,u,v)
       Gamma, v:A->B |- C

is shorthand for

      Gamma1 |- C1   Gamma2 |- C2   
-----------------------------------  AndLeft(A,B,u,v)
       Gamma |- C

with the conditions:

  C1 =alpha= C,
  C2 =alpha= A
  v:A->B in Gamma,
  Gamma1 subset Gamma union {(u:B)},
  Gamma2 subset Gamma
</impleft>


