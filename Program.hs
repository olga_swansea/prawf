-- Program : Prawf; version 2020.001
-- Module : Program (core)
-- Authors : U.Berger, O.Petrovska
-- Date created : 19 Jan 2020
-- Description : Defines data type of a program and 
                 -- main functions to work with programs

{- Revision History  :
Date        Author      Ref    Description

-}
module Program 
  where  -- core

-- haskell
import Data.List (nub)

-- aux
import Perhaps
import ListAux
import Syntax
import Parser

-- ==================================
-- Definitions: 
  -- data Constructor Program D
  -- type Clause Environment ProgRec
  
-- Instances
   -- Show D | Eq D | Syntax Program

-- Functions 
 -- idProg nilProg noLR projLt projRt
 -- conArity conApp
 -- dfix dbot dapp matchCon matchClauses eval
 -- fvProg fvProg' fvProgCase fvClause fvClauses
 -- replacevProg allvProg comp funpair funsum proj
 -- getclause isnormp normProgCon convertcase normProg 
 -- substprog substprog' appProginCl
 -- domPsubst ranPsubst varPsubst substCl

-- Contents
-- ========
  -- Definitions
    -- Program 
    -- basic programs
    -- Domain
  -- Basic functions (to evaluate the program)
  -- Free variables
  -- Replacing program
  -- Beta reduction for programs
  -- Substitution of programs (!!!to update the order)
  -- Showing
  -- Parsing programs

-- Definitions
-- ====================================

data Constructor = Nil
                 | Lt   
                 | Rt   
                 | Pair
                 deriving (Show, Read, Eq)

-- [String] in Clause stands for local bound variables.
-- Ex: ProgCase (ProgCon Pair [prog1, prog2]) [(Pair, ["a", "b"], prog)].
-- "a" and "b" in prog are bound.

type Clause = (Constructor, [String], Program) -- C(xs) -> M

data Program = ProgVar   String
             | ProgCon   Constructor [Program]
             | ProgCase  Program     [Clause] -- case M of {...; C(xs) -> N; ...}
             | ProgApp   Program     Program
             | ProgAbst  String      Program -- lambda x M
             | ProgRec   Program  -- ProgRec M is the lfp of M
--             | ProgRec String Program  -- ProgRec x M
--             deriving (Show, Eq)
             deriving (Show, Read, Eq)
             
instance Syntax Program
  where
    allv = allvProg
    freev = fvProg
    replacev = replacevProg
    
-- basic programs
idProg, nilProg :: Program
idProg = ProgAbst "a_id" (ProgVar "a_id")
nilProg = ProgCon Nil []
noLR = ProgAbst "a_nlr" (ProgCase(ProgVar "a_nlr") 
            [(Lt,["a_nl"], ProgVar "a_nl"),
            (Rt,["a_nr"], ProgVar "a_nr")])
projLt = ProgAbst "a_projL" (ProgCase (ProgVar "a_projL")
            [(Pair, ["a_a","a_b"], ProgVar "a_b")])
projRt = ProgAbst "a_projL" (ProgCase (ProgVar "a_projL")
            [(Pair, ["a_a","a_b"], ProgVar "a_a")])
-- Domain
data D = DNil | DLeft D | DRight D | DPair D D | DFun (D->D) 
       | DBot    -- terminating replacement for bottom 

instance Show D where
  {
    show DNil           = "Nil";
    show (DLeft  d)     = "Lt(" ++ show d ++ ")";
    show (DRight d)     = "Rt(" ++ show d ++ ")";
    show (DPair  d1 d2) = "Pair(" ++ show d1 ++ ", " ++ show d2 ++ ")";
    show (DFun   f)     = "<function>";
    show DBot           = "bot"
  }
-- Careful: show a may be infinite;
-- use only in the form take n (show a)

instance Eq D where
  {
    DNil == DNil = True;
    DLeft d1 == DLeft d2 = d1 == d2;
    DRight d1 == DRight d2 = d1 == d2;
    DPair d11 d12 == DPair d21 d22 = d11 == d21 && d12 == d22;
    DFun f == DFun g = True;
    _ == _ = False;
  }
  
-- Environment
type Environment = [(String, D)]

-- TODO: small step reduction

-- Basic functions
-- ==================================
-- for constructors
conArity :: Constructor -> Int
conArity c = case c of { Nil -> 0 ; Lt -> 1 ; Rt -> 1 ; Pair -> 2 }

conApp :: Constructor -> [D] -> D
conApp c ds = if conArity c == length ds
              then case c of
                     {
                       Nil -> DNil ;
                       Lt  -> DLeft (head ds) ;
                       Rt  -> DRight (head ds) ;
                       Pair -> DPair (ds !! 0) (ds !! 1)
                     }
              else error ("conApp: " ++ show c ++ take 100 (show ds))

dfix :: (D -> D) -> D
dfix f = f (dfix f)

dbot :: D
dbot = dfix id

dapp :: D -> D -> D
dapp a b = case a of { DFun f -> f b ; _ -> dbot }

matchCon :: D -> Perhaps (Constructor,[D])
matchCon a = case a of
               {
                 DNil      -> Success (Nil,[]) ;
                 DLeft b   -> Success (Lt,[b]) ;
                 DRight b  -> Success (Rt,[b]) ;
                 DPair b c -> Success (Pair,[b,c]) ;
                 _         -> Failure "matchCon: not a data"
               }

matchClauses :: D -> [Clause] -> Perhaps (Environment,Program)
matchClauses a clauses =
  do {
       (c,as)    <- matchCon a ;
       (xs,prog) <- look3 c clauses ;
       if length xs == length as
       then return (zip xs as,prog)
       else fail ("matchClauses: " ++ take 100 (show a) ++ show clauses)
     }

-- evaluates a program (not used)
eval :: Environment -> Program -> D
eval env prog = 
  case prog of
    {
      ProgVar a 
        -> case look a env of 
             { Success d -> d ; Failure s -> error s } ;
      ProgCon c progs 
        -> if length progs == conArity c
           then conApp c (map (eval env) progs) 
           else error ("eval: " ++ show c) ;
      ProgCase prog' clauses    
        ->  case matchClauses (eval env prog') clauses of
              {
                Success (env',prog'') -> eval (env' ++ env) prog'' ;
                Failure _             -> dbot
              } ; 
      ProgApp prog1 prog2
        -> dapp (eval env prog1) (eval env prog2) ;
      ProgAbst x prog'
        -> DFun (\ a -> eval ((x,a):env) prog') ;
      ProgRec prog'
        -> case eval env prog' of
             {
               DFun f -> dfix f ;
               _      -> dbot
             }
    }
-- eval can be used to check implementation of smallstep semantics
-- of closed programs: take n (show (eval [] prog))   
-- works only if value is total

-- Free variables
-- ===================================
-- List of free program variables in a given program.
fvProg :: Program -> [String]
fvProg prog = nub $ fvProg' [] prog

fvProg' :: [String] -> Program -> [String]
fvProg' xs prog = 
  case prog of
    {
      ProgVar a           -> if notElem a xs
                             then [a]
                             else [];
      ProgCon _ progs     -> concat $ map (fvProg' xs) progs;
      ProgCase prog1 csps -> fvProg' xs prog1
                             `union` unions [ fvProg' (as `union` xs) p |
                                                  (_,as,p) <- csps ] ;
      ProgApp prog1 prog2 -> fvProg' xs prog1
                             `union` fvProg' xs prog2;
      ProgAbst a prog1    -> fvProg' (a:xs) prog1;
      ProgRec  prog1      -> fvProg' xs prog1;
    }

-- free variables of programs in case
fvProgCase :: [String] -> [(Constructor, [String], Program)] -> [String]
fvProgCase xs [] = []
fvProgCase xs ((_, as, prog):csps) = fvProg' (as `union` xs) prog
                                      `union` fvProgCase xs csps

fvClause :: Clause -> [String]
fvClause (_,xs,p) = removes xs (fvProg p)

fvClauses :: [Clause] -> [String]
fvClauses = concat . map fvClause

-- replacing programs
-- =================================
replacevProg :: [(String,String)] -> Program -> Program
replacevProg xys prog =
  case prog of
    {
      ProgVar x 
        -> case getLR xys x of { Just y -> ProgVar y ; _ -> prog } ;
      ProgCon c progs  
        -> ProgCon c (map (replacevProg xys) progs) ;
      ProgCase prog cls  
        -> ProgCase 
             (replacevProg xys prog)
             [(c,zs,replacevProg 
                      [(x,y) | (x,y) <- xys, not (x `elem` zs)] 
                      p) | (c,zs,p) <- cls ] ;
      ProgApp prog1 prog2 
        -> ProgApp (replacevProg xys prog1) (replacevProg xys prog2) ;
      ProgAbst z prog0 
        -> let { xys' = [(x,y) | (x,y) <- xys, not (x == z)] }
           in ProgAbst z (replacevProg xys' prog0) ;
      ProgRec prog0 
        -> ProgRec (replacevProg xys prog0) 
    }
    
-- returns all program variables
allvProg :: Program -> [String]
allvProg prog =
  case prog of
    {
      ProgVar x -> [x] ;
      ProgCon _ progs -> unions (map allvProg progs) ;
      ProgCase prog cls   
        -> allvProg prog `union` 
           unions [ zs `union` allvProg p | (_,zs,p) <- cls ] ;
      ProgApp prog1 prog2 -> allvProg prog1 `union` allvProg prog2 ;
      ProgAbst x prog0 -> [x] `union` allvProg prog0 ;
      ProgRec prog0 -> allvProg prog0
    } 
     
-- composition of programs.
-- Ex: prog1 * prog2 = \a . prog1 (prog2 a) where a: fresh.
comp, funpair, funsum :: Program -> Program -> Program
comp prog1 prog2 = ProgAbst a
                   (ProgApp
                     prog1
                     (ProgApp prog2 (ProgVar a)))
  where
    {
      vs = nub $ fvProg prog1 `union` fvProg prog2;
      a  = freshVar "a_comp" vs;
    }

funpair prog1 prog2 = 
   ProgAbst a (ProgCon Pair [ProgApp prog1 (ProgVar a),
                             ProgApp prog2 (ProgVar a)]) 
  where
    {
      vs = nub $ fvProg prog1 `union` fvProg prog2;
      a  = freshVar "a_fpair" vs;
    }
    
funsum prog1 prog2 = 
   ProgAbst a (ProgCase (ProgVar a) 
              [(Lt, [al], ProgApp prog1 (ProgVar al)),
               (Rt, [ar], ProgApp prog2 (ProgVar ar))]) 
  where
    {
      vs = nub $ fvProg prog1 `union` fvProg prog2;
      a  = freshVar "a_fsum" vs;
      al  = freshVar "al_fsum" vs;
      ar  = freshVar "ar_fsum" vs;
     }
  
-- projection
proj :: Int -> Program -> Program
proj i prog
  | i == 0    = ProgCase prog [(Pair, [a, b], ProgVar a)]
  | i == 1    = ProgCase prog [(Pair, [a, b], ProgVar b)]
  | otherwise = error "proj"
  where
    { a = "a_proj"; b = "b_proj"; }


-- Beta reduction for programs
-- ===================================================

getclause :: Constructor -> [Clause] -> Perhaps Clause
getclause c [] = fail "getclause: no matching clause"
getclause c ((c',x,prog):csps) =
                       if c == c'
                       then Success (c',x,prog)
                       else getclause c csps

-- checking if a program is normalised
isnormp :: Program -> Bool
isnormp prog = 
  case prog of
    {
     ProgVar pv          -> True;
     ProgCon lr progs    -> and (map isnormp progs);
     ProgCase prog1 csps -> 
       case prog1 of
         {
          ProgCon _ _ -> False;
          ProgCase _ _ -> False;
          ProgVar x -> not (x `elem` fvProgCase [] csps) && (and [isnormp p| (_,_,p) <- csps]);
          _ -> isnormp prog1 && (and [isnormp p| (_,_,p) <- csps])
         };
     ProgApp prog1 prog2 -> 
       case (prog1,prog2) of
         {
          (ProgAbst a prog3,_) -> False;
          (ProgCase prog3 csps,_) -> False; 
          (_,ProgCase prog3 csps) -> False; 
          _ -> isnormp prog1 && isnormp prog2
         };
     ProgAbst a prog1    -> isnormp prog1;
     ProgRec prog1       -> isnormp prog1
    }

-- ???
normProgCon :: Constructor -> [Program] -> Program
normProgCon Lt [ProgCon Pair [p1,p2]] = normProg p1
normProgCon Rt [ProgCon Pair [p1,p2]] = normProg p2
normProgCon c ps = ProgCon c (map normProg ps)


-- new program normalisation
convertcase :: Constructor -> [Program] -> [Clause] -> Program
convertcase lr progs csps = 
  case getclause lr csps of
    {
      Success (c,xs,p) ->
        if length progs == length xs
        then substprog' xs progs p 
        else error ("convertcase: mismatch of programs and variables length.") ;
      _ -> error ("convertcase: no matching clause\n" ++ show lr ++ "\n" ++
                  show progs ++ "\n" ++ show csps)
    }

-- main normalisation function
normProg :: Program -> Program
normProg prog =  
  if isnormp prog 
  then prog
  else case prog of 
    {
     ProgCon lr progs  -> 
          ProgCon lr (map normProg progs);
     ProgCase prog1 cls -> 
       let {prog1' = normProg prog1}
       in case prog1' of
            {
              ProgCon lr progs 
                -> normProg (convertcase lr progs cls); 
              ProgCase prog2 cls2 
                -> normProg 
                     (ProgCase prog2
                               [(c,xs', ProgCase p' cls) | 
                                  (c,xs,p) <- cls2,
                                  (xs',p') <- [rename (xs,p) (fvClauses cls)]]) ;
              ProgVar x ->  
                let {cls1 = [(c,xs, 
                              normProg 
                                (if x `elem` xs 
                                 then p 
                                 else substProg [(x,ProgCon c (map ProgVar xs))] p)) 
                              | (c,xs,p) <- cls]}
                in (ProgCase prog1' cls1);
              _ -> ProgCase prog1' [(c,xs,normProg p)|(c,xs,p) <- cls] ;
             } ;
     ProgApp prog1 prog2 -> 
       let {prog1' = normProg prog1; prog2' = normProg prog2}
       in case (prog1',prog2') of
            {(ProgAbst a prog3,_) -> 
                normProg (substProg [(a,prog2')] prog3);
             (ProgCase prog3 csps3,_) -> 
                normProg (ProgCase prog3 
                         [(c,xs',normProg (ProgApp p' prog2')) |
                            (c,xs,p) <- csps3,
                            (xs',p') <- [rename (xs,p) (freev prog2')]] );
             (_, ProgCase prog3 csps3) ->
                normProg (ProgCase prog3 
                         [(c,xs',normProg (ProgApp prog1' p')) |
                           (c,xs,p) <- csps3,
                           (xs',p') <- [rename (xs,p) (freev prog1')]] );
             _ -> ProgApp prog1' prog2'
             };
     ProgAbst a prog1    -> 
       let {prog1' = normProg prog1} 
       in case prog1' of 
            { ProgApp prog2 (ProgVar pv)  ->  
                if not (pv `elem` (fvProg prog2)) && pv == a
                then prog2                -- eta conversion
                else ProgAbst a prog1'; 
              _ -> ProgAbst a prog1'
            };
     ProgRec prog1       -> ProgRec (normProg prog1);
     _ -> prog
    }
    
-- helper function form normProg in case of ProgApp
-- appProginCl "l" csps prog
-- prog should already be normalised when this function is called
-- "l" / "r" is used to distinguish whether we mean p1 or p2 in ProgApp p1 p2
appProginCl :: String -> [Clause] -> Program -> [Clause]
appProginCl lr [] prog = []
appProginCl lr ((c,xs,p):cl) prog =
  if not (xs `elems` (fvProg prog))
  then case lr of
    {"l" -> (c,xs,normProg(ProgApp p prog)):(appProginCl lr cl prog);
     "r" -> (c,xs,normProg(ProgApp prog p)):(appProginCl lr cl prog)}
  else let {newxs = freshVars (createVars (length xs)) xs;
            substpairs = zip xs newxs;
            prog' = foldr (\(xs',newxs') prog0 -> 
              (substprog xs' (ProgVar newxs') prog0)) prog substpairs }
        in case lr of 
             {"l" -> (c,xs,normProg(ProgApp p prog')):(appProginCl lr cl prog');
              "r" -> (c,xs,normProg(ProgApp prog' p)):(appProginCl lr cl prog')}
  
  
-- Substitution of programs
-- ===================================
domPsubst, ranPsubst, varPsubst :: [(String,Program)] -> [String]
domPsubst theta = map fst theta
ranPsubst theta = nub $ concat $ map freev $ map snd theta 
varPsubst theta = nub (domPsubst theta ++ ranPsubst theta) 


restPsubst :: [(String,Program)] -> [String] -> [(String,Program)]
restPsubst theta xs = [(a,p) | (a,p) <- theta, a `elem` xs]

substProg :: [(String,Program)] -> Program -> Program
substProg theta p =
  let { 
        theta' = restPsubst theta (freev p) ;
        zs = varPsubst theta' 
      }
  in if null theta'
     then p
     else case p of 
            {
              ProgVar a -> 
                case getLR theta' a of { Just q -> q ; _ -> p } ;
              ProgCon c ps -> ProgCon c (map (substProg theta') ps) ;
              ProgCase q cls -> 
                ProgCase (substProg theta' q) (map (substCl theta') cls) ;
              ProgApp q r -> 
                ProgApp (substProg theta' q) (substProg theta' r) ;
              ProgAbst a q ->    
                let { ([a'],q') = rename ([a],q) zs }
                in ProgAbst a' (substProg theta' q') ; 
              ProgRec q -> ProgRec (substProg theta' q)
            }

substCl ::  [(String,Program)] -> Clause -> Clause
substCl theta (c,xs,p) = 
  let { 
        theta' = restPsubst theta [x | x <- freev p, not (x `elem` xs)] ;
        (xs',p') = rename (xs,p) (varPsubst theta') 
      }
  in (c,xs',substProg theta' p')
        

-- old (no longer used)
substprog :: String -> Program -> Program -> Program
substprog a prog2 prog1 =
  case prog1 of 
    {
      ProgVar a'           -> if a == a'
                              then prog2
                              else prog1;
      ProgCon c progs      -> 
          let { progs' = map (substprog a prog2) progs }
          in ProgCon c progs';
      ProgCase prog cls   ->
          ProgCase
             (substprog a prog2 prog)
             [ (c,zs',substprog a prog2 p') | 
                     (c,zs,p) <- cls,
                     (zs',p') <- [rename (zs,p) (a : freev prog2)] ] ;
      ProgApp prog1' prog2' -> 
          let {prog1'' = substprog a prog2 prog1';
               prog2'' = substprog a prog2 prog2'}
          in ProgApp prog1'' prog2'';
      ProgAbst b prog     ->
          let { ([b'],prog') = rename ([b],prog) (a : freev prog2) }
          in ProgAbst b' (substprog a prog2 prog') ; 
      ProgRec prog        -> ProgRec (substprog a prog2 prog)
    }
  
-- (used in convertcase) 
-- here it is assumed that the length of the first two arguments is the same 
substprog' :: [String] -> [Program] -> Program -> Program
substprog' [] [] prog1 = prog1
substprog' (x:xs) (prog2:prog2s) prog1 = 
      substprog' xs prog2s (substprog x prog2 prog1)
-- substrpog' xs prog2s prog1 = foldr (uncurry substprog) prog1 (zip xs prog2s)
         
-- Showing
-- ====================================

type ProgRec = (String, Program)

-- displays a program name
showAvailProgs :: [ProgRec] -> String
showAvailProgs progs = showL (map fst progs)

-- Parsing programs
-- ====================================
parseProg :: Parser Program
parseProg = P reads :: Parser Program
      
parseProgRec :: Parser ProgRec
parseProgRec = 
  do {
      label <- lab;
      tstring "." ;
      eprog <-  parseProg;
      return (label, eprog) 
     }

