-- Program : Prawf; version 2020.001
-- Module : Occ (gen)
-- Authors : U.Berger, O.Petrovska
-- Date created : 17 Jan 2020
-- Description : 

{- Revision History  :
Date        Author      Ref    Description
-}

module Occ -- gen

where

-- haskell
import qualified Data.Map.Strict as Map
import Data.List (intercalate)
import Control.Applicative
import Control.Monad (liftM, ap)

-- aux
import ListAux
import Parser

-- core
import Language

-- ==================================

-- Definitions
 -- type: St

-- Instances 
  -- Monad / Functor / Applicative (St s)
  
-- Functions 
 -- get put ev evalSt
 -- replaceTCond replaceFCond replacePCond replaceDCond replaceOCond
 -- replaceTOcc replaceTAll replaceFOcc replaceFAll antisubstFOcc
 -- antisubstFAll antisubstFOccDefault 

newtype St s a = S (s -> (a,s))

get :: St s s
get = S (\s -> (s,s))

put :: s -> St s ()
put s = S (\_ -> ((),s))

evalSt :: St s a -> s -> a
evalSt (S f) s = fst (f s) 

instance Monad (St s) where
  return a = S (\s -> (a,s))
  S f >>= g = S (\s -> let {(a,s') = f s ; S f' = g a } in f' s')

instance Functor (St s) where
  fmap = liftM
instance Applicative (St s) where
  pure = return
  (<*>) = ap


replaceTCond :: (Int -> Bool) -> Term -> Term -> Term -> St Int Term
replaceTCond test old new = stc 
   where 
     stc t = if t == old
             then do { 
                       s <- get ; let {s' = s + 1} ; put s' ;
                       return (if test s' then new else t)
                     } 
             else case t of
                   {
                     Fun f ts -> do { 
                                      ts' <- sequence (map stc ts) ; 
                                      return (Fun f ts')
                                    } ; 
                     _        -> return t
                   }

replaceFCond :: (Int -> Bool) -> Term -> Term -> Formula -> St Int Formula
replaceFCond test old new = sfc 
  where 
   stc = replaceTCond test old new
   sdc = replaceDCond test old new
   spc = replacePCond test old new
   sfc f = case f of
             {
               Predic p ts -> do { ts' <- sequence (map stc ts); 
                                   p' <- spc p; 
                                   return (Predic p' ts')
                                  };
               Or f g    -> do { f' <- sfc f ; g' <- sfc g ; return (Or f' g') } ;
               And f g   -> do { f' <- sfc f ; g' <- sfc g ; return (And f' g') } ;
               Imp f g   -> do { f' <- sfc f ; g' <- sfc g ; return (Imp f' g') } ;
               Bot       -> return Bot ;
               All x f   -> do { f' <- sfc f ; return (All x f') } ;
               Ex x f    -> do { f' <- sfc f ; return (Ex x f') };
               LetF d f  -> do { f' <- sfc f; d' <- sdc d; return (LetF d' f')}
             }
             
replacePCond :: (Int -> Bool) -> Term -> Term -> Predicate -> St Int Predicate
replacePCond test old new = spc 
  where 
   stc = replaceTCond test old new
   sdc = replaceDCond test old new
   sfc = replaceFCond test old new
   soc = replaceOCond test old new
   spc p = case p of
    {
     Compr xss f -> do {f' <- sfc f; return (Compr xss f')};
     Mu op -> do {op' <- soc op; return (Mu op')};
     Nu op -> do {op' <- soc op; return (Nu op')} ;
     OpApp op q -> do {op' <- soc op; q' <- spc q; return (OpApp op' q')};
     _ -> return p
    }
    
replaceDCond :: (Int -> Bool) -> Term -> Term -> Decl -> St Int Decl
replaceDCond test old new = sdc
  where 
   spc = replacePCond test old new
   soc = replaceOCond test old new
   sdc d = case d of
    {
     FunDecl xt p -> do {p' <- spc p; return (FunDecl xt p')}; 
                     --how should xt,xa be replaced?
     PredDecl xa p -> do {p' <- spc p; return (PredDecl xa p')}; 
     OpDecl xa o -> do {o' <- soc o; return (OpDecl xa o')}
    }
 
replaceOCond :: (Int -> Bool) -> Term -> Term -> Op -> St Int Op
replaceOCond test old new = soc
  where 
   spc = replacePCond test old new
   soc o = case o of
    {
     OpA xa p -> do {p' <- spc p; return (OpA xa p')}; 
     OpV x -> return o
    }

-- replaceOcc [1,3] old new t = 
-- replace in t the first and third occurrence of old by new (from left to right)
replaceTOcc :: [Int] -> Term -> Term -> Term -> Term
replaceTOcc is old new t = evalSt (replaceTCond (`elem` is) old new t) 0

replaceTAll :: Term -> Term -> Term -> Term
replaceTAll old new t = evalSt (replaceTCond (return True) old new t) 0

replaceFOcc :: [Int] -> Term -> Term -> Formula -> Formula 
replaceFOcc is old new f = evalSt (replaceFCond (`elem` is) old new f) 0

replaceFAll :: Term -> Term -> Formula -> Formula
replaceFAll old new f = evalSt (replaceFCond (return True) old new f) 0

antisubstFOcc :: Language -> Context -> [Int] -> Term -> String -> Formula -> Maybe Formula
antisubstFOcc l ctx is old x f =
     let {
           g  = replaceFOcc is old (Var x) f ;
           f' = substituteFormula x old g
         }
     in if alphaequFor l ctx f f' then Just g else Nothing -- declarations not checked!!

antisubstFAll :: Language -> Context -> Term -> String -> Formula -> Maybe Formula
antisubstFAll l ctx old x f =
     let {
           g  = replaceFAll old (Var x) f ;
           f' = substituteFormula x old g
         }
     in if alphaequFor l ctx f f' then Just g else Nothing

antisubstFOccDefault :: Language -> Context -> [Int] -> Term -> Formula -> Maybe (String,Formula)
antisubstFOccDefault l ctx is old f =
     let {
           x  = freshVar "x" (allVarsF f) ; 
           g  = replaceFOcc is old (Var x) f ;
           f' = substituteFormula x old g
         }
     in if alphaequFor l ctx f f' then Just (x,g) else Nothing 



