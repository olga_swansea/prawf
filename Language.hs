-- Program : Prawf; version 2020.001
-- Module : Language (core)
-- Authors : U.Berger, O.Petrovska
-- Date created : 15 Jan 2020
-- Description : Language and syntax

{- Revision History  :
Date        Author      Ref    Description
14/2/2020    UB         N/A    fv case LetF fixed 
                               separate treatment of eqns in arityF
-}

module Language -- core

  where

-- haskell
import qualified Data.Map.Strict as Map  
import Data.List (intercalate) 

-- aux
import ListAux as LA  
import Perhaps
import Syntax

-- ==================================
-- Definitions
  -- Sort Arity Type 
  -- Context Language
  -- Term Formula Predicate Op Decl
  -- Substitution
  
-- Instances Syntax
  -- Term Formula Predicate Op Decl

-- Functions 
 -- startFormula
 -- allVarsT vars allVarsF allVarsP allVarsD allVarsO
 -- free fv fvInSubst termInSubst opInSubst prInSubst
 -- fvInDecls fvdec decvardec alldeclvars fvop fvpred
 -- replacevT replacevF replacevP replacevO replacevD
 -- fpvpred fpvform fpvop fovpred fovform fovop fvpred'
 -- bv bvdecl bvop bvpred

 -- getDecl getDeclV getPDecl getPDecl' getODecl getODecl' getOPDecl
 -- getSorts 

 -- extendCtx ctxFromDecls ctxDiff mergeCtxs 
 -- conflictcheck (to be completed) declvarinctx 

 -- addLSorts addLConsts addLFuns addLPreds
 -- symbolsL addCVars addCPVars addCOVars
 -- premiseImp conclusionImp
 -- sortsTy isSortL usedsortsL usedsortsC
 -- correctL correctC
 -- typeFunSymb
 -- arityPSymb arityOSymb arityPVar arityOVar
 -- sortVar sortConst sortT arityTs
 -- correctF correctFs correctP correctD
 -- arityF arityP arityO arityDeclsO
 -- TO DO : strict positivity check
 -- deleteT, deleteP, deleteO, deleteS
 -- addT addP addO
 -- emptysubst mksubstT mksubstP mksubstO freshSubst

 -- substituteFormula substT substF substP substO substD
 -- varInTSubst varInPSubst varsInPSubst varInOSubst inFVofSubst 

 -- predApp operApp
 -- equation checkedEquation mkEquation

 -- equalDeclFormula equFor alphaequFor alphaequPred
 -- freeTerm objVarStr predVarStr
 -- equT equTs equF equP equOp equFor0 equTs0 equT0

 -- normF normP normO normDeclF normDeclP normDeclF' lets
 -- letAppF letAppP letAppO letAppD unfold
 -- allArity quant splitquant
 -- checkdecl lookupOp lookupOpV lookupPredV lookupPred lookupOperator 
 -- smartCompr incl freshVarF matchVarFormula
 -- harropF harropF' harropP harropO
 -- lEmpty emptycontext
 
 
-- Contents
-- ========
-- Language (terms, formulas, predicates, operators, declarations)
-- Variables 
  -- Free and bound variables
  -- Free variables
  -- Replacing free variables 
  -- Free predicate variables
  -- Free operator variables
-- Instances of Syntax
-- Helper function (to move)
-- Working with context
-- Creation
-- Correctness and arity
-- Substitution
-- Creating and editing substitutions
-- Applying a substitution 
-- Application (terms to predicate, predicate to operator)
-- Equations
-- Equality
   -- TODO (avoid repeated equations)  
-- Normalization 
-- Unfolding declarations
-- Working with quantified formulas
-- Checking and finding a predicate and operator
-- Matching
-- Harrop check 
-- Empty language and context




-- Language (terms, formulas, predicates, operators, declarations)
-- ===============================================================

type Sort = String
type Arity = [Sort]
type Type = (Arity,Sort)

data Language = Lang { 
                       sorts      :: [String] ,
                       constants  :: [(String,Sort)] ,
                       functions  :: [(String,Type)] ,
                       predicates :: [(String,Arity)] ,
                       operators  :: [(String,Arity)]
                     }
           deriving (Show, Read)
-- a language determines predefined sorts, constants, 
-- function symbols, predicate constants and operator constants

data Context = Ctxt {
                      variables :: [(String,Sort)] ,
                      pvars     :: [(String,Arity)] ,
                      ovars     :: [(String,Arity)] 
                    }
           deriving (Show, Read)

data Term = Var String | Const String | Fun String [Term]
           deriving (Show,Eq,Read)
       
data Formula = Predic Predicate [Term] 
             | And Formula Formula
             | Or Formula Formula
             | Imp Formula Formula
             | Bot
             | All (String,Sort) Formula 
             | Ex (String,Sort) Formula
             | LetF Decl Formula 
          deriving (Show,Eq,Read)
            
data Predicate = PrVar String 
               | PrConst String
               | Compr [(String,Sort)] Formula
               | Mu Op
               | Nu Op
               | OpApp Op Predicate
          deriving (Show,Eq,Read)

data Op = OpV String 
        | OpA (String,Arity) Predicate   -- \X.P, P is s.p. in X.
 deriving (Show,Eq,Read)

data Decl = FunDecl (String,Type) Predicate 
-- FunDecl (f,(a,s)) p introduces a new function symbol f satisfying
-- the Skolem axiom all (xs,a) all (y,s) (p(xs,y) -> p(xs,f(xs)))
          | PredDecl (String,Arity) Predicate
-- PredDecl (P,a) p introduces a new predicate variable P
          | OpDecl (String,Arity) Op
-- OpDecl (Phi,a) phi introduces a new operator variable Phi
     deriving (Show,Eq,Read)

-- Warning: Only FunDecl extends the language. 
-- The other declarations introduce variables and hence extend the context.

startFormula :: Formula
startFormula = Predic (PrVar "FORMULA") []

-- Variables
-- =========

-- Warning!!!
-- Variable names (strings) for different syntactic categories must be disjoint!
-- Suggested conventions:
-- Object variables begin with a lower case letter.
-- Predicate variables begin with an upper case letter 
--     and are different from operator variables (see next).
-- Operator variables begin with (the translation into ASCII of) 
--    a capital greek letter (Phi, Psi, Xhi, PhiN, ...).

-- Free and bound variables

allVarsT :: Term -> [String]
allVarsT (Var x) = [x]
allVarsT (Const _) = []
allVarsT (Fun _ ts) = unions (map allVarsT ts)

vars :: Term -> [String]
vars = allVarsT

allVarsF :: Formula -> [String]
allVarsF f =
 case f of
  { 
    Predic p ts -> 
         union (allVarsP p) (unions (map allVarsT ts)) ;
    And g h -> union (allVarsF g) (allVarsF h) ;
    Or g h -> union (allVarsF g) (allVarsF h) ;
    Imp g h -> union (allVarsF g) (allVarsF h) ;
    All (x,s) g -> union [x] (allVarsF g) ;
    Ex (x,s) g -> union [x] (allVarsF g) ;
    LetF d g -> union (allVarsD d) (allVarsF g);
    Bot -> []     
  }

allVarsP :: Predicate -> [String]
allVarsP p =
 case p of
  { 
    PrVar x -> [x] ;
    PrConst _ -> [] ;
    Compr xss f -> union (map fst xss) (allVarsF f) ;
    Mu op -> allVarsO op ;
    Nu op -> allVarsO op ;
    OpApp op q -> union (allVarsO op) (allVarsP q) 
  }

allVarsD :: Decl -> [String]
allVarsD (FunDecl (g,ty) p)= allVarsP p 
allVarsD (PredDecl (x,ar) p) = union [x] (allVarsP p) 
allVarsD (OpDecl (x,ar) op) = union [x] (allVarsO op) 

allVarsO :: Op -> [String]
allVarsO (OpA (x,ar) p) = union [x] (allVarsP p)
allVarsO (OpV x) = [x]


-- Free variables
free :: String -> Formula -> Bool
free x f = x `elem` (fv f)

-- Free variables of a formula
fv :: Formula -> [String]
fv f = case f of
  {
   Predic p ts -> fvpred p `LA.union` unions (map vars ts);
   And f1 f2 -> fv f1 `LA.union` fv f2;
   Or f1 f2  -> fv f1 `LA.union` fv f2;
   Imp f1 f2 -> fv f1 `LA.union` fv f2;
   Bot       -> [];
   All (x,s) g   -> remove x (fv g);
   Ex (x,s) g    -> remove x (fv g);
   -- LetF decl g -> (fvdec decl) `LA.union` fv g 
   LetF decl g -> fvdec decl `LA.union` remove (decvardec decl) (fv g)    
  }
 
-- free variables of terms, predicates and ops in Substitution
fvInSubst :: Substitution -> [String]
fvInSubst theta = 
          (unions $ map vars (termInSubst theta)) `LA.union` 
          (unions $ map fvpred (prInSubst theta)) `LA.union` 
          (unions $ map fvop (opInSubst theta))

termInSubst :: Substitution -> [Term]
termInSubst theta = map snd (termsubst theta)

opInSubst :: Substitution -> [Op]
opInSubst theta = map snd (opsubst theta)

prInSubst :: Substitution -> [Predicate]
prInSubst theta = map snd (predsubst theta)

-- free variables of predicates and operators
fvInDecls :: [Decl] -> [String]
fvInDecls decls = concat $ map fvdec decls

fvdec :: Decl -> [String]
fvdec decl = case decl of
  {
   FunDecl (g,ty) p   -> fvpred p ;
   PredDecl (pv,ar) p -> fvpred p ;
   OpDecl (ov,ar) o   -> fvop o  
  }
  
decvardec :: Decl -> String
decvardec decl = case decl of
  {
   -- FunDecl g _        -> error ("decvardecl: " ++ show g) ; 
   FunDecl (f,_) _   -> f;  
   PredDecl (pv,ar) p -> pv;
   OpDecl (ov,ar) o   -> ov  
  }
  
alldeclvars :: Decl -> [String]
alldeclvars decl = case decl of
  {
   FunDecl _ p        -> fvpred p ;
   PredDecl (pv,ar) p -> [pv] `LA.union` fvpred p ;
   OpDecl (ov,ar) o -> [ov] `LA.union` fvop o  
  }

fvop :: Op -> [String]
fvop o = case o of
  {
   OpA (pv,ar) p -> remove pv (fvpred p);
   OpV ov   -> [ov]
  }
  
fvpred :: Predicate -> [String]
fvpred p = case p of
  {
   PrVar _ -> [];  
   PrConst _ -> [];
   Compr xss f -> fvpred' (map fst xss) f;
   Mu o -> fvop o;
   Nu o -> fvop o;
   OpApp o p -> fvop o `LA.union` fvpred p 
  }
  
  
-- Replacing free variables without avoiding variable capture

replacevT :: [(String,String)] -> Term -> Term
replacevT xys t =
  case t of
    {
      Var x -> case getLR xys x of { Just y -> Var y; _ -> t } ;
      Const c -> t ;
      Fun f ts -> Fun f (map (replacevT xys) ts) 
    }

replacevF :: [(String,String)] -> Formula -> Formula
replacevF xys f =
  case f of
    {
      Predic p ts   -> Predic (replacevP xys p) (map (replacevT xys) ts) ;
      And f1 f2     -> And (replacevF xys f1) (replacevF xys f2) ;
      Or  f1 f2     -> Or  (replacevF xys f1) (replacevF xys f2) ;
      Imp f1 f2     -> Imp (replacevF xys f1) (replacevF xys f2) ;
      Bot           -> Bot ;
      All (x,ar) f0 -> All (x,ar) (replacevF (deleteL xys [x]) f0) ; 
      Ex  (x,ar) f0 -> Ex  (x,ar) (replacevF (deleteL xys [x]) f0) ; 
      LetF d f0     -> LetF (replacevD xys d) (replacevF (deleteL xys [decvardec d]) f0)
     }

replacevP :: [(String,String)] -> Predicate -> Predicate
replacevP xys p = 
  case p of
    {
      PrVar x    -> case getLR xys x of { Just y -> PrVar y; _ -> p } ;
      PrConst c  -> PrConst c ;
      Compr xs f -> Compr xs (replacevF (deleteL xys (map fst xs)) f) ;
      Mu o       -> Mu (replacevO xys o) ;
      Nu o       -> Nu (replacevO xys o) ;
      OpApp o p0  -> OpApp (replacevO xys o) (replacevP xys p0)
    }

replacevO :: [(String,String)] -> Op -> Op
replacevO xys o = 
  case o of
    {
      OpV x    -> case getLR xys x of { Just y -> OpV y; _ -> o } ;
      OpA (x,ar) p -> OpA (x,ar) (replacevP (deleteL xys [x]) p)
    }

replacevD :: [(String,String)] -> Decl -> Decl
replacevD xys d =
  case d of
    { 
      FunDecl f p       -> FunDecl f (replacevP xys p) ;
      PredDecl (x,ar) p -> PredDecl (x,ar) (replacevP xys p) ;
      OpDecl (ov,ar) o  -> OpDecl (ov,ar) (replacevO xys o) 
    }   


-- free predicate variables
fpvpred :: Predicate -> [String]
fpvpred p = 
 case p of
  {
   PrVar x -> [x];
   PrConst _ -> [];
   Compr xss f -> fpvform f;
   Mu o -> fpvop o;
   Nu o -> fpvop o;
   OpApp o p -> fpvop o `LA.union` fpvpred p 
  }

fpvform :: Formula -> [String]
fpvform f = 
 case f of
  {
   Predic p ts -> fpvpred p;
   And f1 f2 -> fpvform f1 `LA.union` fpvform f2;
   Or f1 f2  -> fpvform f1 `LA.union` fpvform f2;
   Imp f1 f2 -> fpvform f1 `LA.union` fpvform f2;
   Bot       -> [];
   All (x,s) g   -> fpvform g;
   Ex (x,s) g    -> fpvform g;
   LetF (PredDecl (x,ar) p) g -> 
           fpvpred p `LA.union` (remove x (fpvform g));
   LetF (OpDecl (_,ar) o) g -> fpvop o `LA.union` fpvform g
  }
  
fpvop :: Op -> [String]
fpvop o =
  case o of
   { 
    OpA (pv,ar) p -> remove pv (fpvpred p);
    OpV ov   -> []    
   }
 
-- free operator variables
fovpred :: Predicate -> [String]
fovpred p = 
 case p of
  {
   PrVar x -> [];
   PrConst x -> [];
   Compr xss f -> fovform f;
   Mu o -> fovop o;
   Nu o -> fovop o;
   OpApp o p -> fovop o `LA.union` fovpred p 
  }

fovform :: Formula -> [String]
fovform f =
 case f of
  {
   Predic p ts -> fovpred p;
   And f1 f2 -> fovform f1 `LA.union` fovform f2;
   Or f1 f2  -> fovform f1 `LA.union` fovform f2;
   Imp f1 f2 -> fovform f1 `LA.union` fovform f2;
   Bot       -> [];
   All x g   -> fovform g;
   Ex x g    -> fovform g;
   LetF (PredDecl (x,ar) p) g -> fovpred p `LA.union` fovform g;
   LetF (OpDecl (x,ar) o) g -> fovop o `LA.union` (remove x (fovform g))
  }
  
fovop :: Op -> [String]
fovop o =
  case o of
   { 
    OpA (pv,ar) p -> fovpred p;
    OpV ov   -> [ov]    
   }

fvpred' :: [String] -> Formula -> [String]
fvpred' xs f = removes xs (fv f)  

-- Bound variables
 
bv :: Formula -> [String]
bv f = case f of
  {
   Predic p ts -> bvpred p;
   And f1 f2 -> bv f1 `LA.union` bv f2;
   Or f1 f2  -> bv f1 `LA.union` bv f2;
   Imp f1 f2 -> bv f1 `LA.union` bv f2;
   Bot       -> [];
   All (x,s) g   -> [x] `LA.union` bv g;
   Ex (x,s) g    -> [x] `LA.union` bv g;
   LetF decl g -> (bvdecl decl) `LA.union` bv f
  }

-- Bound variables of predicates and operators

bvdecl :: Decl -> [String]
bvdecl decl = case decl of
  {
   PredDecl (pv,ar) p -> [pv] `LA.union` bvpred p;
   OpDecl (ov,ar) o -> [ov] `LA.union` bvop o
  }

bvop :: Op -> [String]
bvop o = case o of
 {
  OpA (pv,ar) p -> [pv] `LA.union` bvpred p;
  OpV ov   -> []
 }
  
bvpred :: Predicate -> [String]
bvpred p = case p of
  {
   PrVar x -> [];
   PrConst x -> [];
   Compr xss f -> (map fst xss) `LA.union` bv f;
   Mu o -> bvop o;
   Nu o -> bvop o;
   OpApp o p -> bvop o `LA.union` bvpred p
  }

-- Instances of Syntax
-- ===================

instance Syntax Term
  where
    allv = allVarsT
    freev = allVarsT
    replacev = replacevT

instance Syntax Formula
  where
    allv = allVarsF
    freev = fv
    replacev = replacevF

instance Syntax Predicate
  where
    allv = allVarsP
    freev = fvpred
    replacev = replacevP

instance Syntax Op
  where
    allv = allVarsO
    freev = fvop
    replacev = replacevO

instance Syntax Decl
  where
    allv = allVarsD
    freev = fvdec
    replacev = replacevD


-- Helper functions (to be moved)
-- =============================

getDecl :: String -> [Decl] -> Perhaps Decl
getDecl v [] = Failure "no declaration"
getDecl v (d:ds) = case d of
  {
   FunDecl _ _ -> getDecl v ds;
   PredDecl (v',_) _ -> getD v v' d ds;
   OpDecl (v',_) _ -> getD v v' d ds;
  }
  where getD v v' d ds = 
         if v == v'
         then Success d
         else getDecl v ds;

getDeclV :: Decl -> String
getDeclV d = case d of
  {
   FunDecl (fc,ty) p -> fc;
   PredDecl (pv,a) p -> pv;
   OpDecl (ov,a) o -> ov
  }

-- helper functions for unfolding 
-- finds declaration P = lambda x A(x)
-- returns the first predicate matching p
getPDecl :: String -> [Decl] -> Perhaps Predicate
getPDecl pv' [] = fail "No matching declaration"
getPDecl pv' (d:decls) = case d of
  {
   FunDecl (fc,ty) p -> getPDecl pv' decls;
   PredDecl (pv,a) p -> if (pv == pv')
                        then Success p
                        else getPDecl pv' decls;
   OpDecl (ov,a) o -> getPDecl pv' decls;
  }

-- as above but returns the whole declaration
-- instead of just the predicate
getPDecl' :: String -> [Decl] -> Perhaps Decl
getPDecl' pv' [] = fail "No matching declaration"
getPDecl' pv' (d:decls) = case d of
  {
   FunDecl (fc,ty) p -> getPDecl' pv' decls;
   PredDecl (pv,a) p -> if (pv == pv')
                        then Success d
                        else getPDecl' pv' decls;
   OpDecl (ov,a) o -> getPDecl' pv' decls;
  }

getODecl :: String -> [Decl] -> Perhaps Op
getODecl ov' [] = fail "No matching declaration"
getODecl ov' (d:decls) = case d of
  {
   FunDecl (fc,ty) p -> getODecl ov' decls;
   PredDecl (pv,a) p -> getODecl ov' decls;
   OpDecl (ov,a) o -> if (ov == ov')
                      then Success o
                      else getODecl ov' decls;
  }
  
-- as above but returns the whole declaration
-- instead of just the operator
getODecl' :: String -> [Decl] -> Perhaps Decl
getODecl' ov' [] = fail "No matching declaration"
getODecl' ov' (d:decls) = case d of
  {
   FunDecl (fc,ty) p -> getODecl' ov' decls;
   PredDecl (pv,a) p -> getODecl' ov' decls;
   OpDecl (ov,a) o -> if (ov == ov')
                      then Success d
                      else getODecl' ov' decls;
  }
  
getOPDecl :: String -> [Decl] -> (Perhaps Predicate, Perhaps Op)
getOPDecl v decls = ((getPDecl v decls),(getODecl v decls))

-- gets corresponding sorts for variables
getSorts :: Language -> Context -> [String] -> Perhaps [(String,Sort)] 
getSorts l ctx [] = Success [] 
getSorts l ctx xss@(x:xs) = 
   let {varsorts = variables ctx;
        strs = map fst varsorts}
   in if x `elem` strs
      then let {ssrts = findmatch xss varsorts;
                strs' = map fst ssrts}
           in if strs' `elems` xss
              then Success ssrts
              else fail ("sorts not found for all variables")
      else fail ("variable " ++ x ++ " has no sort declared in the context")

-- Working with context
-- =============================

-- special helper function when declarations are introduced
-- in let formulas. Adds newly declared predicate and ops tp ctx             
extendCtx :: Context -> Decl -> Context
extendCtx ctx decl = 
  case decl of
    {
     PredDecl (pv,ar) pred -> let {pvars' = (pv,ar):pvars ctx;}
                              in ctx{pvars = pvars'};
     OpDecl (ov,ar) op -> let {ovars' = (ov,ar):ovars ctx;}
                          in ctx{ovars = ovars'};
     _                 -> ctx;
    }  

ctxFromDecls :: [Decl] -> Context
ctxFromDecls [] = emptycontext
ctxFromDecls (d:ds) =
  case d of
    {
     FunDecl _ _       -> ctxFromDecls ds;
     PredDecl (pv,a) p -> addCPVars (ctxFromDecls ds) [(pv,a)];
     OpDecl (ov,a) o   -> addCOVars (ctxFromDecls ds) [(ov,a)]
    }

-- compares whether there are duplications
-- ctx1 is meant to be the goal context
-- ctx2 is the global context
-- if some variables in ctx1 are already in global
-- then they are removed     
ctxDiff :: Context -> Context -> Context
ctxDiff ctx1 ctx2 = 
  let {
       c1pv = pvars ctx1;
       c2pv = pvars ctx2;
       c1ov = ovars ctx1;
       c2ov = ovars ctx2;
       cpv = [pv | pv <- c1pv, pv `notElem` c2pv];
       cov = [ov | ov <- c1ov, ov `notElem` c2ov];
      }
  in Ctxt {
           variables = [],  -- this is just to create a context; 
           pvars = cpv,     -- no term variables are expected
           ovars = cov
          }

-- check for conflicts, TO UPDATE
-- quick solution for testing
mergeCtxs :: Context -> Context -> Context
mergeCtxs ctx1 ctx2 = 
  let {
       cvars = (variables ctx1) ++ (variables ctx2);
       cpvars = (pvars ctx1) ++ (pvars ctx2);
       covars = (ovars ctx1) ++ (ovars ctx2) 
      }
  in Ctxt {variables = cvars, pvars = cpvars, ovars = covars}

-- helper function for checking conflicts
conflictcheck :: Eq a => [(String,a)] -> [(String,a)] ->
                 [(String,a)] 
conflictcheck [] [] = []
conflictcheck xss [] = xss
conflictcheck ((x,x'):xs) yss = 
    if (x,x') `elem` yss
    then conflictcheck xs yss -- throws out duplicates
    else if x `elem` (map fst yss)
         then undefined -- conflict! to update
         else [(x,x')] ++  conflictcheck xs yss  

-- checks if a declaration is in the context
declvarinctx :: Decl -> Context -> Bool
declvarinctx decl ctx = 
  case decl of
    {
     PredDecl (pv,ar) pred -> (pv,ar) `elem` (pvars ctx);
     OpDecl (ov,ar) op -> (ov,ar) `elem` (ovars ctx);
     _                 ->  True -- this will not be called;
    }  
    
-- Creation
-- ========

addLSorts :: Language -> [String] -> Language
addLSorts l ss = l { sorts = ss ++ sorts l }

addLConsts :: Language -> [(String,Sort)] -> Language
addLConsts l cts = l { constants = cts ++ constants l }

addLFuns :: Language -> [(String,Type)] -> Language
addLFuns l fss = l { functions = fss ++ functions l }

addLPreds :: Language -> [(String,Arity)] -> Language
addLPreds l pas = l { predicates = pas ++ predicates l }

symbolsL :: Language -> [String]
symbolsL l = map fst (constants l) ++ 
             map fst (functions l) ++
             map fst (predicates l)
             
addCVars :: Context -> [(String,Sort)] -> Context
addCVars c sxs = c {variables = sxs ++ variables c}  

addCPVars :: Context -> [(String,Arity)] -> Context
addCPVars c sxs = c {pvars = sxs ++ pvars c}  

addCOVars :: Context -> [(String,Arity)] -> Context
addCOVars c sxs = c {ovars = sxs ++ ovars c}  

-- New: Analysis of formulas
-- =========================

premiseImp :: Formula -> Perhaps Formula
premiseImp f = case f of 
                 { 
                   Imp f1 _ -> Success f1 ; 
                   _ -> Failure ("premiseImp (Language.hs):\n" ++ show f)
                 }

conclusionImp :: Formula -> Perhaps Formula
conclusionImp f = case f of 
                   { 
                     Imp _ f2 -> Success f2 ; 
                     _ -> Failure ("conclusionImp (Language.hs):\n" ++ show f)
                   } 
   
-- Correctness and arity
-- =====================

sortsTy :: Type -> [Sort]
sortsTy (a,s) = a `union` [s]

isSortL :: Language -> String -> Bool
isSortL l s = s `elem` sorts l

usedsortsL :: Language -> [String]
usedsortsL  l = unions [ map snd (constants l),
                         unions (map (sortsTy.snd) (functions l)),
                         unions (map snd (predicates l)),
                         unions (map snd (operators l))]
                         
usedsortsC :: Context -> [String]
usedsortsC  ctx = unions [ map snd (variables ctx),
                         unions (map snd (pvars ctx)),
                         unions (map snd (ovars ctx))]
                        
correctL :: Language -> Bool
correctL l = usedsortsL l `subset` sorts l

correctC :: Language -> Context -> Bool
correctC l ctx = usedsortsC ctx `subset` sorts l

typeFunSymb :: Language -> String -> Maybe Type
typeFunSymb = getLR . functions   

arityPSymb :: Language -> String -> Maybe Arity
arityPSymb = getLR . predicates  

arityOSymb :: Language -> String -> Maybe Arity
arityOSymb = getLR . operators    

arityPVar :: Context -> String -> Maybe Arity
arityPVar = getLR . pvars   

arityOVar :: Context -> String -> Maybe Arity
arityOVar = getLR . ovars    

sortVar :: Context -> String -> Maybe Sort
sortVar = getLR . variables    

sortConst :: Language -> String -> Maybe Sort
sortConst = getLR . constants    

sortT :: Language -> Context -> Term -> Maybe Sort
sortT l c t =
  case t of
    {
      Var x -> sortVar c x ;
      Const u -> sortConst l u ;
      Fun f ts -> 
         do { 
              ss <- arityTs l c ts ;
              (ss',s) <- typeFunSymb l f ; 
              if ss == ss' 
              then return s 
              else fail "sortT: sorts don't fit"
            }  
    }

arityTs :: Language -> Context -> [Term] -> Maybe Arity
arityTs l c = sequence . map (sortT l c) 
 
correctF :: Language -> Context -> Formula -> Bool
correctF l c f = case arityF l c f of { Just _ -> True; _ -> False}

correctFs :: Language -> Context -> [Formula] -> [Bool]
correctFs l c = map (correctF l c)

correctP :: Language -> Context -> Predicate -> Bool
correctP l c p = case arityP l c p of { Just _ -> True; _ -> False}

correctD :: Language -> Context -> Decl -> Bool
correctD l c d =
  case d of 
   {
     FunDecl (f,(ar,s)) p -> 
       case arityP l c p of 
         { Just a -> a == ar ++ [s] ; Nothing -> False } ;
     PredDecl (pv,ar) p -> 
       case arityP l c p of 
         { Just a -> a == ar ; Nothing -> False } ;
     OpDecl (ov,ar) o -> 
       case arityO l c o of 
         { Just a -> a == ar ; Nothing -> False } 
   }

arityF :: Language -> Context -> Formula -> Maybe Arity -- always []?
arityF l c f = 
  case f of
    {
      Predic (PrConst "=") ts ->
        case ts of
          {
            [t1,t2] -> 
               do { 
                    s1 <- sortT l c t1 ; 
                    s2 <- sortT l c t2 ;
                    if s1 == s2 then return [] else fail "different sorts in eqn"
                  } ;
            _ -> fail ("incorrect number of arguments for eqn:" ++ show (length ts))
          } ;                       
      Predic p ts -> 
        do { 
             a1 <- arityP l c p; 
             a2 <- arityTs l c ts;
             if a1 == a2 then return [] else fail ""
           } ;
      And f g -> arityF l c f >> arityF l c g >> return [] ;
      Or f g -> arityF l c f >> arityF l c g >> return [] ;
      Imp f g -> arityF l c f >> arityF l c g >> return [] ;
      Bot -> return [] ;
      All (x,s) f -> arityF l (addCVars c [(x,s)]) f ;
      Ex (x,s) f -> arityF l (addCVars c [(x,s)]) f ;
      LetF (FunDecl (g,t) p) f -> 
        do {
             a1 <- arityP l c p ;
             if not (null a1) 
             then let { t1 =  (init a1,last a1) }
                  in if t == t1 
                     then arityF (addLFuns l [(g,t)]) c f
                  else fail ""
             else fail ""
           } ;
      LetF (PredDecl (x,a) p) f -> 
        do {
             a1 <- arityP l c p ;
             if a == a1
             then arityF l (addCPVars c [(x,a)]) f
             else fail ""
           } ;
      LetF (OpDecl (x,a) o) f -> 
        do {
             a1 <- arityO l c o ;
             if a == a1
             then arityF l (addCOVars c [(x,a)]) f
             else fail ""
           }
    }

arityP :: Language -> Context -> Predicate -> Maybe Arity
arityP l c p = case  p of
  {
   PrVar x -> arityPVar c x ;
   PrConst a -> arityPSymb l a ;
   Compr sxs f -> arityF l (addCVars c sxs) f >> return (map snd sxs) ;
   Mu o -> arityO l c o;
   Nu o -> arityO l c o;
   OpApp o p' -> do { a <- arityO l c o; b <- arityP l c p';
                      if a == b then return a else fail "fail arityP"     
                    }
  }

arityO :: Language -> Context -> Op -> Maybe Arity
arityO l c (OpA (x,a) p) = 
  do {
       a1 <- arityP l (addCPVars c [(x,a)]) p ;
       if a == a1 then return a else fail "arity mismatch"
     }
arityO l c (OpV o) = toMaybe $ look o (ovars c) 

arityDeclsO :: Language -> Context -> [Decl] -> Op -> Perhaps Arity
arityDeclsO l c decls op = toPerhaps $ arityO l c (foldr letAppO op decls)


-- Substitution
-- ============

data Substitution = Subst {
                            termsubst :: [(String,Term)] ,
                            predsubst :: [(String,Predicate)] ,
                            opsubst   :: [(String,Op)]
                          }

-- Creating and editing substitutions

-- deleteL is defined in ListAux
deleteT, deleteP, deleteO, deleteS :: 
    Substitution -> [String] -> Substitution
deleteT theta xs = 
  theta {termsubst = deleteL (termsubst theta) xs} 
deleteP theta xs = 
  theta {predsubst = deleteL (predsubst theta) xs}  
deleteO theta xs = 
  theta {opsubst = deleteL (opsubst theta) xs}  
deleteS theta xs = (flip deleteT xs . flip deleteP xs . flip deleteO xs) theta 

addT :: Substitution -> [(String,Term)] -> Substitution
addT theta xts = theta {termsubst = xts ++ termsubst theta}  

addP :: Substitution -> [(String,Predicate)] -> Substitution
addP theta xts = theta {predsubst = xts ++ predsubst theta}  

addO :: Substitution -> [(String,Op)] -> Substitution
addO theta xts = theta {opsubst = xts ++ opsubst theta}  

emptysubst :: Substitution
emptysubst = Subst { termsubst = [], predsubst = [], opsubst = [] }

mksubstT :: [(String,Term)] -> Substitution
mksubstT = addT emptysubst

mksubstP :: [(String,Predicate)] -> Substitution
mksubstP = addP emptysubst

mksubstO :: [(String,Op)] -> Substitution
mksubstO = addO emptysubst

freshSubst :: (String, Substitution) -> (String,Substitution)
freshSubst (x,theta) = 
  let {theta' = deleteT theta [x];
       x' = freshVar x (fvInSubst theta')}
  in (x',theta')

-- Applying a substitution 
-- =======================

substituteFormula :: String -> Term -> Formula -> Formula
substituteFormula x t f = substF (mksubstT [(x,t)]) f

substT :: Substitution -> Term -> Term
substT theta t =
   case t of
     {
       Var x  -> case getLR (termsubst theta) x of
                   { Just r -> r ; _ -> t } ;
       Const x -> Const x ;
       Fun x ts -> Fun x (map (substT theta) ts)
     }

substF :: Substitution -> Formula -> Formula
substF theta f =
   case f of
     {
       Predic p ts -> 
          Predic (substP theta p) (map (substT theta) ts) ;
       And g1 g2 -> And (substF theta g1) (substF theta g2);
       Or g1 g2 -> Or(substF theta g1) (substF theta g2);
       Imp g1 g2 -> Imp (substF theta g1) (substF theta g2);
       All (x,s) g -> 
         let { 
               theta' = deleteT theta [x] ;
               ([x'],g') = rename ([x],g) (fvInSubst theta')
             }
         in All (x',s) (substF theta' g') ;
       Ex (x,s) g -> 
         let { 
               theta' = deleteT theta [x] ;
               ([x'],g') = rename ([x],g) (fvInSubst theta')
             }
         in Ex (x',s) (substF theta' g') ;
       LetF (FunDecl f p) g -> 
         LetF (FunDecl f (substP theta p)) 
              (substF theta g) ;
       LetF (PredDecl (x,ar) p) g ->
         let {
               theta' = deleteT theta [x] ;
               ([x'],g') = rename ([x],g) (fvInSubst theta')     
             }
         in LetF (PredDecl (x',ar) (substP theta p)) 
                 (substF theta' g') ;
       LetF (OpDecl (ov,ar) o) g ->
         let {
               theta' = deleteT theta [ov] ;
               ([ov'],g') = rename ([ov],g) (fvInSubst theta')      
             }
         in LetF (OpDecl (ov',ar) (substO theta o)) 
                 (substF theta' g') ;
       Bot -> Bot
      }

substP :: Substitution -> Predicate -> Predicate
substP theta p =
   case p of
     {
       PrVar x ->  case getLR (predsubst theta) x of
                     { Just q -> q ; _ -> p };
       PrConst _ -> p ;
       Compr xxs f -> 
         let {
               xs = map fst xxs ;
               srts = map snd xxs ;
               theta' = deleteT theta xs ;
               (xs',f') = rename (xs,f) (fvInSubst theta')
             }
         in Compr (zip xs' srts) (substF theta' f') ;
       Mu o -> Mu (substO theta o);
       Nu o -> Nu (substO theta o);
       OpApp op q -> OpApp (substO theta op) (substP theta q)  
     }

substO :: Substitution -> Op -> Op
substO theta o =
   case o of
     {
       OpV phi   -> case getLR (opsubst theta) phi of
                      { Just o' -> o' ; _ -> o } ;
       OpA (pv,a) p -> 
         let { 
               theta' = deleteT theta [pv] ;
               ([pv'],p') = rename ([pv],p) (fvInSubst theta')
             }
         in OpA (pv',a) (substP theta' p') 
     }
     
substD :: Substitution -> Decl -> Decl
substD theta d = 
  case d of 
    {
     FunDecl (x,ty) p -> FunDecl (x,ty) (substP theta p);   
     PredDecl (x,a) p -> PredDecl (x,a) (substP theta p);
     OpDecl (x,a) o -> OpDecl (x,a) (substO theta o)
   }
  
-- checks if x is in theta variables (keys)
varInTSubst :: Substitution -> String ->  Bool
varInTSubst theta x = 
   let {xts = map fst (termsubst theta)}
   in x `elem` xts  
   
varInPSubst :: Substitution -> String ->  Bool
varInPSubst theta x = 
   let {xps = map fst (predsubst theta)}
   in x `elem` xps 
   
varsInPSubst :: Substitution -> [String] -> [Bool]
varsInPSubst theta xs = map (varInPSubst theta) xs
   
varInOSubst :: Substitution -> String ->  Bool
varInOSubst theta x = 
   let {xos = map fst (opsubst theta)}
   in x `elem` xos 
   
-- takes a list of variables
-- returns a list of only those which are in fv of substitution
inFVofSubst :: Substitution -> [String] -> [String]
inFVofSubst theta [] = []
inFVofSubst theta (x:xs) = 
   if x `elem` (fvInSubst theta)
   then [x] ++ inFVofSubst theta xs
   else inFVofSubst theta xs

-- Application (terms to predicate, predicate to operator)
-- ======================================================

predApp :: Predicate -> [Term] -> Formula
predApp p ts = case p of
  {
    Compr sxs f -> substF (mksubstT (zip (map fst sxs) ts)) f;
    _          -> Predic p ts
  }
  
operApp :: Op -> Predicate -> Predicate
operApp o p = case o of
  {
   OpA (x,a) p' -> substP (mksubstP [(x,p)]) p';
   _            -> OpApp o p
  }

-- Equations
-- =========

equation :: Formula -> Perhaps (Term,Term)
equation f = case f of
              {
                Predic (PrConst "=") [t1,t2] -> Success (t1,t2) ;
                _                            -> Failure "equation"
              }

checkedEquation :: Language -> Context -> 
                            Formula -> Perhaps ((Term,Term),Sort)
checkedEquation l ctx f =
   do {
        t12@(t1,t2) <- equation f ;
        s1 <- toPerhaps (sortT l ctx t1) ;
        s2 <- toPerhaps (sortT l ctx t2) ;
        if s1 == s2
        then return (t12,s1)
        else fail "checkedEquation"
      }

mkEquation :: Term -> Term -> Formula
mkEquation r t = Predic (PrConst "=") [r,t]
 
-- Equality
-- =======================================================

equalDeclFormula :: Language -> Context -> [Decl] -> 
                                        Formula -> Formula -> Bool
equalDeclFormula l ctx decls f g =  
       alphaequFor l ctx (normDeclF decls f)  (normDeclF decls g)
                             
-- Calculating equations sufficient to make two formulas equivalent

-- If equFor l ctx (f,g) = Success (h,zip (zip rs ts) us),
-- then f =alpha= h[rs/us], g =alpha= h[ts/us] 
-- and therefore r1 = t1 & ... & rn = tn -> (f <-> g)
equFor :: Language -> Context -> (Formula,Formula) -> 
                  Perhaps (Formula,[((Term,Term),(String,Sort))]) 
equFor l ctx (f,g) = equF l ctx [] [] (f,g) u  
  where
    u = noinitsegString "u" (union (allVarsF f) (allVarsF g))

-- checks if formulas are alpha equal
alphaequFor :: Language -> Context -> Formula -> Formula -> Bool
alphaequFor l ctx f g =
 case equFor l ctx (f,g) of 
       { Success (_,[]) -> True; _ -> False }

alphaequPred :: Language -> Context -> 
                  Predicate -> Predicate -> Bool
alphaequPred l ctx pred1 pred2 =
 case equP l ctx [] [] (pred1,pred2) "u" of 
       { Success (_,[]) -> True; _ -> False }

-- auxiliary functions for equFor

-- t has no variables in xs
freeTerm :: [String] -> Term -> Bool
freeTerm xs t = not (elems xs (allVarsT t))

objVarStr :: String -> String
objVarStr u = "x" ++ u

predVarStr :: String -> String
predVarStr u = "X" ++ u


-- Specification similar as for equF below
equT :: Language -> Context -> 
        [((String,String),(String,Sort))] -> 
        (Term,Term) -> String -> 
           Perhaps (Term,[((Term,Term),(String,Sort))])
equT l ctx xyzs (r,t) u =
  do {
       sr <- toPerhaps (sortT l (addCVars ctx [(x,s) | ((x,y),(z,s)) <- xyzs]) r) ; 
       st <- toPerhaps (sortT l (addCVars ctx [(y,s) | ((x,y),(z,s)) <- xyzs]) t) ;
       if sr /= st
       then fail "equT different sorts"
       else
         case (r,t) of
           {
             (Var x, t) ->       
                case getBdx xyzs x of
                  {
                    Just (y,(z,s)) ->   -- x is bound
                      case t of
                        {
                          Var y' -> 
                            if y' == y
                            then return (Var z,[])
                            else fail "equT Var x Var" ;
                          _      -> fail "equT Var x other" 
                        } ;
                    Nothing ->      -- x is free
                      if freeTerm (getys xyzs) t 
                      then
                        if Var x == t
                        then return (t, [])
                        else 
                          let { xu = objVarStr u }
                          in return (Var xu, [((r,t),(xu,sr))])
                      else fail "equT x free t not free"
                  } ;
             (r, Var y) ->      
                case getBdy xyzs y of
                     {
                       Just (x,(z,s)) ->   -- y is bound
                         case r of
                           {
                             Var x' -> 
                               if x' == x
                               then return (Var z,[])
                               else fail "equT Var y Var" ;
                             _ -> fail "equT Var y other" 
                           } ;
                       Nothing ->      -- y is free
                         if freeTerm (getxs xyzs) r 
                         then if Var y == r
                              then return (r, [])
                              else 
                                let { xu = objVarStr u }
                                in return (Var xu, [((r,t),(xu,sr))])
                         else fail "equT y free r not free"
                     } ;
             (Const c, Const d) -> 
                if c == d
                then return (Const c, [])
                else return (Var u,[((r,t),(u,sr))]) ;
             (Fun f rs,Fun g ts) -> 
                if f == g && length rs == length ts 
                then 
                  do {
                       (ss,rtus) <- equTs l ctx xyzs (zip rs ts) u ;
                       return (Fun f ss,rtus)
                     }
                else 
                  if freeTerm (getxs xyzs) r && freeTerm (getys xyzs) t
                  then return (Var u, [((r,t),(u,sr))])
                  else fail "equT Fun other";
            _ -> let { xu = objVarStr u }
                 in return (Var xu, [((r,t),(xu,sr))])
           }
     }

equTs :: Language -> Context -> 
         [((String,String),(String,Sort))] -> 
         [(Term,Term)] -> String -> 
            Perhaps ([Term],[((Term,Term),(String,Sort))])
equTs l ctx xyzs [] u = return ([],[])
equTs l ctx xyzs ((r,t):rts) u =
  do {
       (s,rtus) <- equT l ctx xyzs (r,t) (tl u) ;
       (ss,rtus1) <- equTs l ctx xyzs rts (tr u) ;
       return (s:ss, rtus ++ rtus1)
     }

-- If equF l ctx (zip (zip ps qs) vs) (zip (zip xs ys) zs) (f,g) u = Success (h,zip (zip rs ts) us),
-- then lambda ps lambda xs f =alpha= lambda vs lambda zs h[rs/us]
-- and  lambda qs lambda ys g =alpha= lambda vs lambda zs h[ts/us] 
-- and r1 = t1 & ... & rn = tn -> all zs (h[rs/us] <-> h[ts/us]).
-- Ip if equF l ctx [] [] (f,g) u = Success (h,zip (zip rs ts) us),
-- then f =alpha= h[rs/us] and g =alpha= h[ts/us] 
-- and r1 = t1 & ... & rn = tn -> (f <-> g).
-- equF [] (f,g) u = Success (h,[]) means that f and g are alpha equal.
-- It is assumed that f and g are normalized (contain no Let).
-- The string u is used to generate fresh us.
equF :: Language -> Context -> 
        [((String,String),(String,Arity))] -> [((String,String),(String,Sort))] -> 
                                      (Formula,Formula) -> String -> 
                          Perhaps (Formula,[((Term,Term),(String,Sort))])
equF l ctx pqrs xyzs (f,g) u = 
 case (f,g) of
  {
   (Predic p1 ts1, Predic p2 ts2)
     -> if length ts1 == length ts2
        then do {
                 (p,rtus)   <- equP l ctx pqrs xyzs (p1,p2) (tl u) ; 
                 (ss,rtus1) <- equTs l ctx xyzs (zip ts1 ts2) (tr u) ;
                 return (Predic p ss,rtus ++ rtus1)
                }
        else fail "equF Predic" ; 
   (And f1 f2, And g1 g2)   
     -> do {
            (h1,rtus1) <- equF l ctx pqrs xyzs (f1,g1) (tl u) ;
            (h2,rtus2) <- equF l ctx pqrs xyzs (f2,g2) (tr u) ;
            return (And h1 h2, rtus1 ++ rtus2)
           } ;
   (Or f1 f2, Or g1 g2)   
     -> do {
            (h1,rtus1) <- equF l ctx pqrs xyzs (f1,g1) (tl u) ;
            (h2,rtus2) <- equF l ctx pqrs xyzs (f2,g2) (tr u) ;
            return (Or h1 h2, rtus1 ++ rtus2)
           } ;
   (Imp f1 f2, Imp g1 g2)   
     -> do {
            (h1,rtus1) <- equF l ctx pqrs xyzs (f1,g1) (tl u) ;
            (h2,rtus2) <- equF l ctx pqrs xyzs (f2,g2) (tr u) ;
            return (Imp h1 h2, rtus1 ++ rtus2)
           } ;
   (Bot,Bot) 
     -> return (Bot,[]) ;
   (All (x,s1) f, All (y,s2) g)   
     -> let { z = objVarStr u }
        in if s1 == s2
           then do {
                     (h,rtus) <- equF l ctx pqrs (((x,y),(z,s1)):xyzs) (f,g) (fwd u) ;
                     return (All (z,s1) h,rtus)
                   } 
           else fail "equF All" ;
   (Ex (x,s1) f, Ex (y,s2) g)   
     -> let { z = objVarStr u }
        in if s1 == s2
           then do {
                     (h,rtus) <- equF l ctx pqrs (((x,y),(z,s1)):xyzs) (f,g) (fwd u) ;
                     return (Ex (z,s1) h,rtus)
                   } ;
           else fail "equF Ex" ;
   _  -> fail "equF no match"
 
  }

-- Specification similar as for equF
equP :: Language -> Context -> 
        [((String,String),(String,Arity))] -> 
        [((String,String),(String,Sort))] -> 
       ( Predicate,Predicate) -> String -> 
              Perhaps (Predicate,[((Term,Term),(String,Sort))])
equP l ctx pqrs xyzs (p,q) u = 
 case (p,q) of
  { 
   (PrVar x, PrVar y) 
     -> if arityP l (addCPVars ctx [(p,a) | ((p,q),(r,a)) <- pqrs]) (PrVar x) == 
           arityP l (addCPVars ctx [(q,a) | ((p,q),(r,a)) <- pqrs]) (PrVar y)
        then case getBdx pqrs x of
              {
                Just (q,(r,a)) ->  -- x is bound
                  if y == q
                  then return (PrVar r,[])
                  else fail "equP PrVar x bound y free" ;
                Nothing ->      -- x is free
                  case getBdy pqrs y of
                    {
                      Just _  ->  -- y is bound
                         fail "equP PrVar x free y bound" ;
                      Nothing ->   -- y free
                        if x == y 
                        then return (PrVar x, [])
                        else fail "equP PrVar x /= y free"
                    }
              } 
        else fail "equP PrVar different arities" ;
   (PrConst a, PrConst b)
     -> if (length (arityP l ctx (PrConst a)) == 
            length (arityP l ctx (PrConst b))) && 
           (a == b)  
        then return (p,[])
        else fail "equP PrConst" ;
   (Compr xss f, Compr yss g) 
     -> let { a = map snd xss }
        in if map snd yss == a
           then let { (zs,u') = newStrs (length a) u ; 
                      zss = zip zs a }
                in do {
                        (h,rtus) <- 
                             equF l 
                                  ctx
                                  pqrs  -- [((String,String),(String,Arity))] 
                                  (zip (zip (map fst xss) 
                                            (map fst yss))
                                       zss
                                   ++ xyzs) 
                                  (f,g) 
                                  u' ;
                       return (Compr zss h, rtus)
                      } 
           else fail "equP Compr different arities" ; 
   (Mu o1, Mu o2)           
     -> do {
             (o,rtus) <- equOp l ctx pqrs xyzs (o1,o2) u ;
             return (Mu o, rtus)
           } ;
      (Nu o1, Nu o2)           
         -> do {
                 (o,rtus) <- equOp l ctx pqrs xyzs (o1,o2) u ;
                 return (Nu o, rtus)
               } ;
      (OpApp o1 p1,OpApp o2 p2)
         -> do {
                 (o,rtus) <- equOp l ctx pqrs xyzs (o1,o2) (tl u) ; 
                 (p,rtus1) <- equP l ctx pqrs xyzs (p1,p2) (tr u) ; 
                 return (OpApp o p, rtus ++ rtus1) 
               } ;
      _  -> fail "equP other"
               }

equOp :: Language -> Context -> 
         [((String,String),(String,Arity))] -> [((String,String),(String,Sort))] -> 
                (Op,Op) -> String -> Perhaps (Op,[((Term,Term),(String,Sort))])
equOp l ctx pqrs xyzs (o1,o2) u = 
 case (o1,o2) of
  { 
   (OpV ov1,OpV ov2)   
     -> if (length (arityOVar ctx ov1) == length (arityOVar ctx ov2)) && 
           (ov1 == ov2)  
        then return (o1,[])
        else fail "equOp OpV" ;
   (OpA (x,ar1) p,OpA (y,ar2) q)  
     -> if length ar1 == length ar2 
        then let { 
                   z = predVarStr u ;
                   u' = fwd u
                 }
             in do {
                     (r,rtus) <- equP l ctx (((x,y),(z,ar1)):pqrs) xyzs (p,q) u' ;
                     return (OpA (z,ar1) r,rtus)
                   }
        else fail "equOp OpA" ;
   _ -> fail "equOp other"           
    }

-- Functions for congf (similar but simpler than the above)

-- If equFor0 l ctx (f,g) = Success (h,zip (zip rs ts) us),
-- then f =alpha= h[rs/us], g =alpha= h[ts/us]
-- where us are not in the scope of a quantifier or abstraction 
-- and therefore r1 = t1 & ... & rn = tn -> (f <-> g)
equFor0 :: Language -> Context -> (Formula,Formula) ->  
                  Perhaps (Formula,[((Term,Term),(String,Sort))]) 
equFor0 l ctx (f,g) = 
  let { u = noinitsegString "u" (union (allVarsF f) (allVarsF g)) }
  in ef (f,g) u
 where 
  ef (f,g) u = 
   case (f,g) of
     {
       (Predic p1 ts1, Predic p2 ts2)
          -> if length ts1 == length ts2 && 
                alphaequPred l ctx p1 p2
             then do {
                       (ts,rtus) <- equTs0 l ctx (zip ts1 ts2) u ;
                       return (Predic p1 ts,rtus)
                     }
             else fail "Language equF0 Predic" ; 
       (And f1 f2, And g1 g2)   
          -> do {
                  (h1,rtus1) <- ef (f1,g1) (tl u) ;
                  (h2,rtus2) <- ef (f2,g2) (tr u) ;
                  return (And h1 h2, rtus1 ++ rtus2)
                } ;
       (Or f1 f2, Or g1 g2)   
          -> do {
                  (h1,rtus1) <- ef (f1,g1) (tl u) ;
                  (h2,rtus2) <- ef (f2,g2) (tr u) ;
                  return (Or h1 h2, rtus1 ++ rtus2)
                } ;
       (Imp f1 f2, Imp g1 g2)   
          -> do {
                  (h1,rtus1) <- ef (f1,g1) (tl u) ;
                  (h2,rtus2) <- ef (f2,g2) (tr u) ; 
                  return (Imp h1 h2, rtus1 ++ rtus2)
                } ;
       (Bot,Bot) 
          -> return (Bot,[]) ;
       _  -> if alphaequFor l ctx f g
             then return (f, [])
             else fail "Language equF0 other" 
     }
 
equTs0 :: Language -> Context -> [(Term,Term)] -> String -> 
            Perhaps ([Term],[((Term,Term),(String,Sort))])
equTs0 l ctx [] u = return ([],[])
equTs0 l ctx ((r,t):rts) u =
  do {
       (s,rtus) <- equT0 l ctx (r,t) (tl u) ;
       (ss,rtus1) <- equTs0 l ctx rts (tr u) ;
       return (s:ss, rtus ++ rtus1)
     }

equT0 :: Language -> Context -> (Term,Term) -> String -> 
           Perhaps (Term,[((Term,Term),(String,Sort))])
equT0 l ctx (r,t) u =
  do {
       sr <- toPerhaps (sortT l ctx r) ; 
       st <- toPerhaps (sortT l ctx t) ; 
       if sr /= st
       then fail "Language equT0 different sorts"
       else if r == t 
            then return (t, [])
            else let { xu = objVarStr u }
                 in return (Var xu, [((r,t),(xu,sr))])
     }

-- TODO: 
-- 1. Avoid repeated equations. 

-- end of definition of equFor

-- Normalization 
-- =======================================================

normF :: Formula -> Formula
normF f = case f of
  {
   Predic p ts -> predApp (normP p) ts;
   And g1 g2 -> And (normF g1) (normF g2);
   Or g1 g2 -> Or (normF g1) (normF g2);
   Imp g1 g2 -> Imp (normF g1) (normF g2);
   Bot -> Bot;
   All (x,s) g -> All (x,s) (normF g); -- !
   Ex (x,s) g -> Ex (x,s) (normF g);
   LetF d g -> normF (letAppF d g) 
   }    

normP :: Predicate -> Predicate
normP p = case p of
  {
   Compr xss f -> Compr xss (normF f);
   Mu o       -> Mu (normO o);
   Nu o       -> Nu (normO o);
   OpApp o p' -> 
       case o of 
          {
           OpA (pv,ar) p'' -> normP (substP (mksubstP [(pv,p')]) p''); 
           OpV ov          -> OpApp o (normP p')
           };
   _          -> p
  } 
  
normO :: Op -> Op
normO o = case o of
  {
   OpA (pv,ar) p -> OpA (pv,ar) (normP p); 
   OpV ov   -> OpV ov
  }

normDeclF  :: [Decl] -> Formula -> Formula 
normDeclF [] f = normF f
normDeclF decls f = normF (lets decls f)

normDeclP :: [Decl] -> Predicate -> Predicate
normDeclP decls pred = normP $ foldr letAppP pred decls                                       

normDeclO :: [Decl] -> Op -> Op
normDeclO decls op = normO $ foldr letAppO op decls                                       
  
-- special case of normalization
-- only normalises PrVars and OpApp 
-- used for endFormula'
normDeclF' :: [Decl] -> Formula -> Formula
normDeclF' [] f = f
normDeclF' decls f = 
  case f of
  {
   Predic p ts -> 
     case p of                              
       {
        Compr _ _ -> normDeclF' decls (predApp p ts) ;               
        PrVar pv -> 
           case getPDecl' pv decls of
             {
              Success d -> normF (lets [d] f);
              Failure _  -> f
             };
        OpApp o pred ->
           case o of
            {
             OpV ov ->
               case getODecl' ov decls of
                {
                 Success d1 -> normDeclF' decls (normF (lets [d1] f)); -- CHECK!
                 Failure _  -> f
                };
             OpA (ov,a) pred' ->
               case getODecl' ov decls of
                {
                 Success d1 ->                    
                   case pred of
                     {PrVar pv -> 
                        case getPDecl' pv decls of
                         {
                          Success d2 -> normF (lets [d1,d2] f);
                          Failure _  -> normF (lets [d1] f);
                         };
                      _ -> f
                      };
                 Failure _ -> f
                };
             };
        _          -> f
        };
   _ -> f
   }    
  
-- adds declarations to the formula using LetF
lets :: [Decl] -> Formula -> Formula
lets [] f = f
lets (d:decls) f = LetF d (lets decls f) 
  
-- Unfolding declarations 
-- ======================

letAppF :: Decl -> Formula -> Formula
letAppF d f = case f of 
  {
   Predic p ts -> Predic (letAppP d p) ts;
   And g h     -> And (letAppF d g) (letAppF d h);
   Or g h      -> Or (letAppF d g) (letAppF d h);
   Imp g h     -> Imp (letAppF d g) (letAppF d h);
   Bot         -> Bot;
   All (x,s) g -> 
     let { ([x'],g') = rename ([x],g) (freev d) }
     in All (x',s) (letAppF d g') ;
   Ex (x,s) g -> 
     let { ([x'],g') = rename ([x],g) (freev d) }
     in Ex (x',s) (letAppF d g') ;
   LetF e g  -> LetF (letAppD d e) (letAppF d g) 
       -- e and d must not declare different things 
       -- under the same name!
   }
  
letAppP :: Decl -> Predicate -> Predicate
letAppP d p = case p of 
  {
   PrVar x    -> case d of
                  { 
                   PredDecl (y,a) p1 -> if x == y
                                        then p1
                                        else p;
                    _ -> p
                  }; 
   PrConst _  -> p;
   Compr xss f ->
     let {
           xs = map fst xss ;
           srts = map snd xss ;
           (cs',f') = rename (xs,f) (freev d) 
         }
      in Compr (zip xs srts) (letAppF d f') ;
   Mu o       -> Mu (letAppO d o);
   Nu o       -> Nu (letAppO d o);
   OpApp o p' -> OpApp (letAppO d o) (letAppP d p')                  
  }
  
letAppO :: Decl -> Op -> Op
letAppO d o = 
  case o of
    {
      OpV ov    -> case d of
               { 
                 OpDecl (ov',a) o' -> if ov == ov' then o' else o;
                 _-> o 
               } ;
      OpA (pv,a) p -> 
        if pv == getDeclV d then o
        else let { ([pv'],p') = rename ([pv],p) (fvdec d) }
             in OpA (pv',a) (letAppP d p') 
   }

letAppD :: Decl -> Decl -> Decl
letAppD d (PredDecl (pv,ar) p) =
  PredDecl (pv,ar) (letAppP d p)
letAppD d (OpDecl (ov,ar) o) =
  (OpDecl (ov,ar) (letAppO d o))
letAppD d e = e

unfold :: String -> [Decl] -> Formula -> Formula
unfold v decls f = 
  let { Success d = getDecl v decls }
  in normF (letAppF d f)

-- Working with quantified formulas
-- =================================   

-- returns arity of a quantified formula
allArity :: Formula -> Arity
allArity f = let { (xss,_) = splitquant f } in map snd xss

-- adds quantifiers
quant :: [(String,Sort)] -> Formula -> Formula
quant [] f = f
quant ((x,s):xss) f = All (x,s) (quant xss f) 

-- splits a quantified formula into 
-- a list of all universally quantified variables with sorts and 
-- the kernel (stripped off quantifiers)
splitquant :: Formula -> ([(String,Sort)],Formula)
splitquant f = case f of 
  {
   All (x,s) g -> let (ys,h) = splitquant g
                  in ((x,s):ys,h);
   _           -> ([],f)
   } 

-- Checking and finding a predicate and operator
-- ========================================

-- check if a variable v is declared
checkdecl :: String -> [Decl] -> Bool
checkdecl v [] = False
checkdecl v (d:decl) = 
  case d of
    {
     FunDecl _ _ -> checkdecl v decl;
     PredDecl (v',ar) _ -> ch v v';
     OpDecl (v',ar) _ -> ch v v';
    }
  where ch v v' = if v == v'
                  then True
                  else checkdecl v decl

-- finds 1 matching operator
lookupOp :: [Decl] -> String -> Op
lookupOp [] ov = (OpV ov); 
lookupOp (d:ds) ov = case d of
  {
   OpDecl (ov',ar) o -> if ov == ov' 
                        then o
                        else lookupOp ds ov;
   _ -> lookupOp ds ov                        
  }

-- finds 1 matching operator
lookupOpV :: [Decl] -> String -> (Op, [Decl])
lookupOpV [] opv = (OpV opv, []); 
lookupOpV (d:ds) opv = case d of
  {
   OpDecl (opv',ar) o -> if opv == opv' 
                         then (o,ds)
                         else let (o',ds') = lookupOpV ds opv
                              in (o',d:ds');
   _ -> lookupOpV ds opv                        
  }
  
-- finds 1 matching predicate
-- lookupPredV ds "N"
-- Predicate (PrVar "M")
lookupPredV :: [Decl] -> String -> (Predicate, [Decl])
lookupPredV [] pv = (PrVar pv, []); 
lookupPredV (d:ds) pv = case d of
  {
   PredDecl (pv',ar) p -> if pv == pv' 
                          then (p,ds)
                          else let (p',ds') = lookupPredV ds pv
                               in (p',d:ds');
   _ -> lookupPredV ds pv                        
  }
  
-- finds the final version of a predicate
-- if we have N = M and M = (Mu o)
-- (Mu o) 
lookupPred :: [Decl] -> Predicate -> Predicate
lookupPred ds p = if null ds then p else
 case p of
  {
   PrVar x -> let (p',ds') = lookupPredV ds x
              in case p' of 
               {
                PrVar x' -> fst (lookupPredV ds x');
                _        -> p'
                };
   _       -> p
  }

-- finds the final version of an operator
lookupOperator :: [Decl] -> Op -> Op
lookupOperator ds o = if null ds then o else
 case o of
  {
   OpV x -> let {(o',ds') = lookupOpV ds x}
            in case o' of 
                {
                  OpV x' -> fst (lookupOpV ds x') ;
                  _      -> o'
                } ;
   _     -> o
  }
  
-- helper finction for refining induction and coinduction
smartCompr :: [(String,Sort)] -> Formula -> Predicate
smartCompr xss f = case f of
   {
    Predic pn ts -> if ts == map Var (map fst xss)
                    then pn
                    else Compr xss f;
    _ -> Compr xss f
   }

-- creates an inclusion formula
incl ::  Language -> Context -> Predicate -> Predicate -> (Perhaps Formula)
incl l ctx p1 p2 = do {li1 <- toPerhaps (arityP l ctx p1); 
                       li2 <- toPerhaps (arityP l ctx p2); 
                   let {i = length li1; 
                        j = length li2} 
                   in if i == j 
                      then let { 
                             Just sorts = arityP l ctx p1;
                             xs = createVars i; 
                             ys = fvpred p1 `union` fvpred p2;
                             zs = freshVars xs ys;
                             zss = zip zs sorts;
                             ts = map Var zs                          
                               }
                           in Success 
                               (quant zss 
                                      (Imp (Predic p1 ts) (Predic p2 ts)))
                      else fail ("incl fail")
                  }

freshVarF :: Formula -> String
freshVarF f = let { x = if null (bv f) then "x" else head (bv f) }
              in freshVar x (fv f `LA.union` bv f)

-- Matching
-- ========

-- If matchVarFormula l ctx x f g = Success t, then g = f[t/x] 
matchVarFormula :: Language -> Context -> (String,Sort) -> Formula -> Formula 
                                                                -> Perhaps Term
matchVarFormula l ctx (x,s) f g =
   do {
        (h,rtus) <- equFor l ctx (f,g) ;
        case rtus of
          {
            [((Var x', t),(u,s'))] ->
                if (x',s') == (x,s) && alphaequFor l ctx (All (u,s) h) (All (x,s) f)
                then return t
                else fail "matchVarFormula in Language.hs" ;
            _ -> fail "matchVarFormula in Language.hs" ;
          }
      }


-- Harrop checking
-- =====================      

harropF :: [String] -> [Decl] -> Formula -> Bool
harropF str decls f = harropF' str (normDeclF decls f)

-- [String] stands for a list of "Harrop predicate variables".
-- the argument f is supposed to be a decl-free formula (by normDeclF in harropF)
harropF' :: [String] -> Formula -> Bool
harropF' xs f =
  case f of
    {
      Predic pred ts -> harropP xs pred;
      And f1 f2      -> harropF' xs f1 && harropF' xs f2;
      Or  f1 f2      -> False;
      Imp  _ f2      -> harropF' xs f2;
      Bot            -> True;
      All  x f       -> harropF' xs f;
      Ex   x f       -> harropF' xs f;
      LetF _ _       -> error "LetF in harropF'";
    }

-- we always use harropP after getting decl-free expression.
harropP :: [String] -> Predicate -> Bool
harropP xs p =
  case p of
    {
      PrVar x                 -> x `elem` xs;
      PrConst _               -> True;
      Compr _ f               -> harropF' xs f;
      Mu (OpA (pv,_) pred)    -> harropP (pv:xs) pred;
      Nu (OpA (pv,_) pred)    -> harropP (pv:xs) pred;
      OpApp op@(OpA _ _) p    -> harropP xs $ operApp op p;
      _                       -> error "harropP";
    }

harropO :: [String] -> Op -> Bool
harropO xs op =
  case op of
    {
      OpA (pv,_) pred -> harropP (pv:xs) pred;
      OpV _           -> error "OpV in harropO."
    }


-- Empty language and context
-- ========

lEmpty :: Language         
lEmpty = Lang {
                sorts = [], 
                constants  = [], 
                functions  = [], 
                predicates = [], 
                operators  = []
              }
              
emptycontext :: Context
emptycontext = Ctxt { variables = [], pvars = [], ovars = [] }


