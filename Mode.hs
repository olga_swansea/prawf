-- Program : Prawf; version 2020.001
-- Module : Mode (core)
-- Authors : O.Petrovska
-- Date created : 13 Jan 2020
-- Description : Main application module linking Prover and Exec

{- Revision History  :
Date        Author      Ref    Description
-}

module Mode

  where 
  
-- haskell
import GHC.IO

-- core
import Prover
import Exec
    
-- gen
import ReadShow  

main :: IO ()
main = 
  do {
      putStr logo;
      putStr ("\nType exec to enter the program execution mode or press enter to launch the prover >> ");
      input <- getLine;
      if input == "exec" 
      then do { em;
                putStrLn execmodetext}
      else prover
     }
     

     
