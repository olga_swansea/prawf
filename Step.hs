-- Program : Prawf; version 2020.001
-- Module : Step (gen)
-- Authors : U.Berger, O.Petrovska, H.Tsuiki
-- Date created : 19 Jan 2020
-- Description : Defines data types of a rule, a state of a proof; 
                 -- executes user commands, etc.
{- Revision History  :
Date        Author      Ref    Description

-}

module Step  -- gen

  where

-- aux
import Perhaps
import Parser
import ListAux as LA 

-- core
import Language
import Proof

-- gen
import PlProof
import ParseLang 
import Occ 

-- ==================================
-- Definitions: 
  -- data Rule Input Output Err IPS State
  
-- Functions 
 -- mkState ok startState mkStartState0 mkStartState0' mkStartState mkNewState
 -- startPPF startPP startPP' rep steps step check
 -- stepI (works with processes for various commands) 
 -- stepR completeCheck 

-- Contents
-- ========
  -- Definitions
  -- Basic functions
  -- Step change
    

-- Definitions
-- ======================

data Rule = AssuR | UseWithR | UseThmR | UseThmWithR
          | AndiR | AndelR | AnderR 
          | OrilR | OrirR  | OreR | OreAssuR 
          | ImpiR | ImpeR  
          | EfqR  | RaaR  
          | AlliR | AlleR | AlleRF
          | ExiR  | ExeR
          | AndLeftR | OrLeftR | ImpLeftR | CutR 
          | LetR | DeclR | NormR | UnfoldR
          | IndR | HSIndR | SIndR | ClR | CoclR
          | CoiR | HSCoiR | SCoiR
          | EqReflR | EqSymR | EqCongR | EqCongfR | EqCongruR
  deriving (Show, Read)

data Input = TextI String | RuleI Rule 
  deriving (Show, Read)

data Output = OkO | StartO | ErrorO Err 
  deriving (Show, Read)

data Err = IncorrectTermE |IncorrectFormE | IncorrectPredE | IncorrectDeclE
         | NoTextExpE | UnknownAssuE | RuleNotApplicableE  
         | UseWithE String | UseThmE | UseThmWithE 
         | CongE | CongfE | CongruE  
         | ProofCompleteE | TextExpE | RefinementE String | FileNotFoundE
  deriving (Show, Read)

data IPS = OkS | StartS | CompleteS   -- Interactive Proof State
         | UseS | ImpiS | ImpeS | AndelS | AnderS | OreS | OreAssuS
         | AlleS | AlleSF | ExiS | ExeS | DeclS
         | AndLeftS | OrLeftS | ImpLeftS | CutS 
         | EqCongS | EqCongfS | EqCongruS | NormS | UnfoldS
         | QuitS | UseWithS | UseThmS | UseThmWithS
         | ExProgS -- state for executing programs
  deriving (Show, Read)
  
data State = State { 
                     output    :: Output ,
                     ips       :: IPS ,
                     stplproof   :: PlProof ,
                     language  :: Language ,
                     stcontext   :: Context , -- This field should be removed in the future versions
                     counter   :: Int
                   }
       deriving Show

-- Basic functions
-- ======================

mkState :: (Output, IPS, PlProof, Language, Context, Int) -> State
mkState (o,i,pp,l,ctx,n) = 
  State { 
          output   = o ,
          ips      = i ,
          stplproof  = pp ,
          language = l ,
          stcontext  = ctx , 
          counter  = n
        }

ok :: State -> State
ok state = state { output = OkO, ips = OkS }

startState :: Language -> Context -> State
startState l ctx = mkState (StartO,StartS,startPP,l, ctx,0)

mkStartState0 :: State -> State
mkStartState0 state = startState (language state) (stcontext state) 

mkStartState0' :: State -> State
mkStartState0' state = 
  let {plp = stplproof state;
       plpassu = plpassumpts plp;
       plpds = plpdecls plp;
       plpgs = plpglstack plp;
       plpts = plpthms plp;}
  in ((ok . mkStartState0) state){ stplproof = (startPP' plpassu plpds plpts) } 

mkStartState :: State -> Formula -> State
mkStartState state f = 
  let {plp = stplproof state;
       plpassu = plpassumpts plp;
       plpds = plpdecls plp;
       plpts = plpthms plp;}
  in ((ok . mkStartState0) state){ stplproof = (startPPF plpassu plpds plpts f) } 

mkNewState :: State -> Formula -> State
mkNewState state f = 
  let {l = language state;
       ctx = stcontext state;
       plp = stplproof state;
       plpassu = plpassumpts plp;
       plpds = plpdecls plp;
       plpts = plpthms plp;
       plp' = startPP' plpassu plpds plpts;
       state' = mkState (StartO,StartS,plp',l, ctx,0);
       pr = startPPF plpassu plpds plpts f; 
       state'' = state'{ stplproof = pr }}
  in state' 
  
startPPF :: [Assumpt] -> [Decl] -> [ThmI] -> Formula -> PlProof
startPPF assu decls thms = start assu decls thms startGoalSymbol 
                           -- start defined in PlProof.hs

startPP :: PlProof
startPP = startPPF [] [] [] startFormula

startPP' :: [Assumpt] -> [Decl] -> [ThmI] -> PlProof
startPP' assu decls thms = startPPF assu decls thms startFormula

rep :: (a -> b -> b) -> [a] -> b -> b
rep f (x:xs) y = rep f xs (f x y)
rep f []     y = y

-- Step change
-- ====================================
steps :: [Input] -> State -> State
steps = rep step 

step :: Input -> State -> State
step inp state = check state'
  where
   state' =
     case inp of
      {
       TextI str  -> stepI (unwords (words str)) state ;
       RuleI rule -> stepR rule state 
      }

check :: State -> State
check state =
  let {
       pp = stplproof state;
       l = language state;
       ctx = stcontext state; 
       n = counter state;
      }      
  in if null (plpglstack pp) 
     then mkState (OkO,CompleteS,pp,l,ctx,n) 
     else state


stepI :: String -> State -> State
stepI str state =
   let {
         out = output state;
         ipstate = ips state;
         pp = stplproof state;
         l = language state;
         ctx = stcontext state; 
         n = counter state;                           
         Success cgctx = currentGoalCtx pp;
         ctx' = mergeCtxs cgctx ctx;
         pf0 = runPerhaps (parseFormula l ctx) ;  
                -- Parser, ParseLanguage 
         pf = runPerhaps (parseFormula l ctx') ;
         pt = runPerhaps (parseTerm l ctx') ;
         pd = runPerhaps (parseDecl' l ctx');
         parsePredAndEqns = 
           runPerhaps (parsePredicate l ctx' `bindp`  -- Parser 
                       many (parseEquation l ctx')) ;
         use par err process s = 
          case par s of
            { 
              Success x -> process x ; 
              Failure _ -> state { output = ErrorO err }
            } ;
         useF  = use pf IncorrectFormE ;
         useT  = use pt IncorrectTermE ;
         useD  = use pd IncorrectDeclE ;

   impeProcess f = 
     let { [gl1,gl2] = newGoalSymbols n 2 }
     in case refine l ctx' pp (ImpElimBW gl1 gl2 f) of
          {
           Success pp' -> mkState (OkO,OkS, pp',l,ctx, n+2);
           Failure s   -> error s -- should never be called
          } ;
   andelProcess f = 
     let { [gl] = newGoalSymbols n 1 }
     in case refine l ctx' pp (AndElilBW gl f) of 
          { 
           Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
           Failure s   -> error s -- should never be called
          } ;
   anderProcess f = 
     let { [gl] = newGoalSymbols n 1 }
     in case refine l ctx' pp (AndElirBW gl f) of 
         { 
          Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
          Failure s   -> error s -- should never be called
         } ;
   oreProcess f = 
     let { [gl1,gl2,gl3] = newGoalSymbols n 3 }
     in case refine l ctx' pp (OrElimBW gl1 gl2 gl3 f) of
         {
          Success pp' -> mkState (OkO,OkS, pp',l,ctx, n+3);
          Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
         } ;  
   oreAssuProcess av = 
     let { [gl2,gl3] = newGoalSymbols n 2 }
     in case refine l ctx' pp (OrElimBWAssu gl2 gl3 av) of
         {
          Success pp' -> mkState (OkO,OkS, pp',l,ctx, n+3);
          Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
         } ;

   orLeftProcess  str = let{ [gl1,gl2] = newGoalSymbols n 2;
                            us = map fst (plpthms pp) ++ map fst (plpassumpts pp);
                            u = freshVar (str++"L") us;
                            v = freshVar (str ++ "R") us; }
                  in case refine l ctx' pp (OrLeftBW gl1 u gl2 v str) of
                          {
                            Success pp' ->  mkState (OkO,OkS, pp',l,ctx, n+2);
                            Failure _   -> mkState (ErrorO RuleNotApplicableE,ipstate,pp,l,ctx,n)
                          };

   impLeftProcess str = let{ [gl1,gl2] = newGoalSymbols n 2;
                            us = map fst (plpthms pp) ++ map fst (plpassumpts pp);
                            u = freshVar (str++"New") us; }
                 in case refine l ctx' pp (ImpLeftBW gl1 gl2 u str) of
                         {
                            Success pp' ->  mkState (OkO,OkS, pp',l,ctx, n+2);
                            Failure _   -> mkState (ErrorO RuleNotApplicableE,ipstate,pp,l,ctx,n)
                         };
   cutProcess f = let { [gl1,gl2] = newGoalSymbols n 2;
                        us = map fst (plpthms pp) ++ map fst (plpassumpts pp);
                        u = freshVar "uCut" us; }
                in case refine l ctx' pp (CutBW gl1 gl2 (u,f) ) of
                        {
                           Success pp' ->  mkState (OkO,OkS, pp',l,ctx, n+2);
                           Failure _   -> mkState (ErrorO RuleNotApplicableE,ipstate,pp,l,ctx,n)
                        };

--  check!
   alleFProcess f = 
     case f of
      {
        All (x,s) f1 -> 
          case currentGoal pp of
            {
              Success cg ->
                let { g = goal_formula cg}
                in case matchF [x] f1 g of  -- matchF in PlProof.hs
                     {
                       Just [(_,t)] -> 
                         let { [gl] = newGoalSymbols n 1} 
                         in case refine l ctx' pp (AllElimBW gl f t) of 
                              {
                                Success pp' -> 
                                      ok state { 
                                                stplproof = pp', 
                                                counter = n+1 
                                                } ;
                                Failure _   -> 
                                      state {output = ErrorO IncorrectFormE}
                              }; 
                       _ -> state {output = ErrorO IncorrectFormE}
                     };
              Failure _ -> error ("No current goal.")
            };
        _ -> state {output = ErrorO IncorrectFormE}
      } ;

   alleProcess t = 
    case currentGoal pp of
     {
      Success cg ->
        let { 
              g = goal_formula cg ; 
              x = case t of { Var x -> x ; _     -> freshVarF g } ;
              Just s = sortT l ctx' t
            }
        in case antisubstFAll l ctx' t x g of  -- Occ.hs
         {
          Just f -> let { [gl] = newGoalSymbols n 1} 
            in case refine l ctx' pp (AllElimBW gl (All (x,s) f) t) of -- sort!
             {
              Success pp' -> ok state { stplproof = pp',
                                        counter = n + 1
                                      };
              Failure _   -> state {output = ErrorO IncorrectFormE}
             }; 
          Nothing -> state {output = ErrorO IncorrectFormE}
         };
      Failure _ -> error ("No current goal.")
     } ;

   exiProcess t = 
      let { [gl] = newGoalSymbols n 1}
      in case refine l ctx' pp (ExIntrBW gl t) of 
          {
           Success pp' -> mkState (OkO,OkS, pp',l,ctx, n+1);
           Failure s   -> mkState (ErrorO (RefinementE s),ipstate,pp,l,ctx,n)
          } ;  
   exeProcess f = 
      let { [gl1,gl2] = newGoalSymbols n 2}
      in case refine l ctx' pp (ExElimBW gl1 gl2 f) of 
          {
           Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+2);
           Failure s   -> mkState (ErrorO (RefinementE s),ipstate,pp,l,ctx,n)
          }  ;
   declProcess d = 
      let { [gl] = newGoalSymbols n 1}
      in case refine l ctx' pp (DeclPBW gl d) of 
          {
           Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
           Failure s   -> mkState (ErrorO (RefinementE s),ipstate,pp,l,ctx,n)
          }  ;
   eqCongProcess f = 
      let { [gl] = newGoalSymbols n 1}
      in case refine l ctx' pp (EqCongBW gl f) of
          {
           Success pp' -> 
              let { k = length (plpglstack pp') - length (plpglstack pp) }
              in ok state { stplproof = pp', counter = n+1+k } ;
           Failure _   -> state { output = ErrorO IncorrectFormE }
          } ;
   eqCongfProcess f = 
      let { [gl] = newGoalSymbols n 1}
      in case refine l ctx' pp (EqCongfBW gl f) of
         {
          Success pp' -> 
              let { k = length (plpglstack pp') - length (plpglstack pp) }
              in ok state { stplproof = pp', counter = n+1+k } ;
          Failure _   -> state { output = ErrorO IncorrectFormE }
         } ;
   eqCongruProcess p eqs = 
      let { [gl] = newGoalSymbols n 1}
      in case refine l ctx' pp (EqCongruBW gl p eqs) of
          {
            Success pp' -> 
              let { k = length (plpglstack pp') - length (plpglstack pp) }
              in ok state { stplproof = pp', counter = n+1+k } ;
            Failure _   -> state { output = ErrorO IncorrectFormE }
          } ;
   useWithProcess str = 
      let { gl = newGoalSymbol n }
      in case refine l ctx' pp (UseWithBW gl str) of
           {
             Success pp' -> 
               let { k = length (plpglstack pp') - length (plpglstack pp) }
               in ok state { stplproof = pp', counter = n+1+k } ;
             Failure s   -> state { output = ErrorO (UseWithE s) } 
           } ;
   useThmProcess str = 
      let { gl = newGoalSymbol n }
      in case refine l ctx' pp (UseThmBW gl str) of
           {
             Success pp' -> 
               let { k = length (plpglstack pp') - length (plpglstack pp) }
               in ok state { stplproof = pp', counter = n+1+k } ;
             Failure _   -> state { output = ErrorO UseThmE } 
           } ;
   useThmWithProcess str = 
      let { gl = newGoalSymbol n }
      in case refine l ctx' pp (UseThmWithBW gl str) of
           {
             Success pp' -> 
               let { k = length (plpglstack pp') - length (plpglstack pp) }
               in ok state { stplproof = pp', counter = n+1+k } ;
             Failure _   -> state { output = ErrorO UseThmWithE } 
           } ;
   normProcess f = 
      case refine l ctx' pp (NormPBW f) of
         {
          Success pp' -> mkState (OkO,OkS, pp', l, ctx, n);
          Failure s   -> mkState (ErrorO (RefinementE s),ipstate,pp,l,ctx,n)
         }  ;
       }
   in case ipstate of
       {
         StartS    -> use pf0 IncorrectFormE (mkStartState state) str ;
         OkS       -> state { output = ErrorO NoTextExpE } ;
         UseWithS  -> useWithProcess str;
         UseThmS   -> useThmProcess str;
         UseThmWithS  -> useThmWithProcess str;
         UseS      -> case refine l ctx' pp (AssumptBW str) of
                        {
                          Success pp' -> ok state { stplproof = pp' } ;
                          Failure _   -> state { output = ErrorO UnknownAssuE } 
                        } ;
         AndLeftS  -> let { [gl] = newGoalSymbols n 1;
                            us = map fst (plpthms pp) ++ map fst (plpassumpts pp);
                            u = freshVar (str++"L") us;
                            v = freshVar (str ++ "R") us; }
                      in case refine l ctx' pp (AndLeftBW gl u v str) of
                           {
                             Success pp' -> ok state { stplproof = pp' } ;
                             Failure _   -> state { output = ErrorO RuleNotApplicableE } 
                                        
                           };
         ImpiS     -> let { [gl] = newGoalSymbols n 1 }
                      in case refine l ctx' pp (ImpIntrBW gl str) of
                           {
                             Success pp' -> ok state { stplproof = pp', counter = n+1 } ;
                             Failure _   -> state { output = ErrorO RuleNotApplicableE }  
                           } ;
         ImpeS     -> useF impeProcess str ;
         ImpLeftS  -> impLeftProcess str;
         AndelS    -> useF andelProcess str ;
         AnderS    -> useF anderProcess str ;
         OreS      -> useF oreProcess str ;
         OreAssuS  -> oreAssuProcess str ;
         OrLeftS   -> orLeftProcess str;
         AlleSF    -> useF alleFProcess str ;
         AlleS     -> useT alleProcess str ;
         ExiS      -> useT exiProcess str ;
         ExeS      -> useF exeProcess str ;
         CutS      -> useF cutProcess str;
         DeclS     -> useD declProcess str ;
         NormS     -> useF normProcess str ;
         UnfoldS   -> 
           case refine l ctx' pp (UnfoldBW str) of
             {
               Success pp' -> mkState (OkO,OkS, pp', l, ctx, n);
               Failure s   -> 
                 mkState (ErrorO (RefinementE s),ipstate,pp,l,ctx,n)
             } ;
         EqCongS   -> useF eqCongProcess str ;
         EqCongfS   -> useF eqCongfProcess str ;
         EqCongruS -> 
           case parsePredAndEqns str of 
             {
               Success (p,eqns) -> eqCongruProcess p eqns ;
               Failure s -> state { output = ErrorO CongruE }
             } ; 
         CompleteS -> state { output = ErrorO ProofCompleteE }
       }
       

stepR :: Rule -> State -> State
stepR rule state =
  let { 
        out = output state;
        ipstate = ips state;
        pp = stplproof state;
        l = language state;
        ctx = stcontext state; 
        n = counter state;
        Success cgctx = currentGoalCtx pp;
        ctx' = mergeCtxs cgctx ctx;
        
      }
  in case rule of
       {
         AssuR  -> state { output = OkO, ips = UseS } ;
         UseWithR  -> state { output = OkO, ips = UseWithS } ;
         UseThmR  -> state { output = OkO, ips = UseThmS } ;
         UseThmWithR  -> state { output = OkO, ips = UseThmWithS } ;
         AndiR  -> let { [gl1,gl2] = newGoalSymbols n 2 }
                   in case refine l ctx' pp (AndIntrBW gl1 gl2) of
                        {
                          Success pp' -> mkState (OkO,OkS,pp',l, ctx,n+2);
                          Failure _   -> state {output = ErrorO RuleNotApplicableE} 
                        } ;
         AndelR -> mkState (OkO,AndelS,pp,l,ctx,n);
         AnderR -> mkState (OkO,AnderS,pp,l,ctx,n);
         AndLeftR -> mkState (OkO,AndLeftS,pp,l,ctx,n);
         OrilR  -> let { [gl] = newGoalSymbols n 1 }
                   in case refine l ctx' pp (OrIntrlBW gl) of
                        {
                          Success pp' -> ok state {stplproof = pp', counter = n + 1}; -- add increment by a number for state
                          Failure _   -> mkState (ErrorO RuleNotApplicableE,ipstate,pp,l,ctx,n)
                        } ;
         OrirR  -> let { [gl] = newGoalSymbols n 1 }
                   in case refine l ctx' pp (OrIntrrBW gl) of
                        {
                          Success pp' -> mkState (OkO,OkS,pp',l,ctx,n+1);
                          Failure _   -> mkState (ErrorO RuleNotApplicableE,ipstate,pp,l,ctx,n)
                        } ;
         OreR   -> mkState (OkO,OreS,pp,l,ctx,n);
         OreAssuR -> mkState (OkO,OreAssuS,pp,l,ctx,n);
         OrLeftR  -> mkState (OkO,OrLeftS,pp,l,ctx,n);
         ImpiR  -> mkState (OkO,ImpiS,pp,l,ctx,n);
         ImpeR  -> mkState (OkO,ImpeS,pp,l,ctx,n);
         ImpLeftR -> mkState (OkO,ImpLeftS,pp,l,ctx,n);
         EfqR   -> let { [gl] = newGoalSymbols n 1 }
                   in case refine l ctx' pp (ExFQBW gl) of
                        {
                          Success pp' -> ok state { stplproof = pp', counter = n+1 } ;
                          Failure s   -> error s -- should never be called
                        } ;
         RaaR   -> let { [gl] = newGoalSymbols n 1 }
                   in case refine l ctx' pp (ReAABW gl) of
                             {
                               Success pp' -> ok state { stplproof = pp', counter = n+1 } ;
                               Failure s   -> error s -- should never be called
                             } ;
         AlliR  ->   -- CHECK! TO UPDATE
                   let { 
                         [gl] = newGoalSymbols n 1 ;
                       } 
                   in case refine l ctx' pp (AllIntrBW gl) of
                         {
                          Success pp' -> ok state { 
                                                    stplproof = pp', 
                                                    counter = n + 1
                                                  } ; 
                          Failure _   -> state {output = ErrorO RuleNotApplicableE}
                         } ;
         AlleR  -> mkState (OkO,AlleS,pp,l,ctx,n);
         AlleRF -> mkState (OkO,AlleSF,pp,l,ctx,n);
         ExiR   -> mkState (OkO,ExiS,pp,l,ctx,n); 
         ExeR   -> mkState (OkO,ExeS,pp,l,ctx,n);
         CutR   -> mkState (OkO,CutS,pp,l,ctx,n);
         LetR   -> let { [gl] = newGoalSymbols n 1 }
                   in case refine l ctx' pp (LetPBW gl) of 
                        { 
                          Success pp' -> mkState (OkO,OkS,pp',l,ctx,n+1);
                          Failure s   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n) --error s -- should never be called
                        } ;
         DeclR  -> mkState (OkO,DeclS,pp,l,ctx,n);
         NormR  -> mkState (OkO,NormS,pp,l,ctx,n);
         UnfoldR -> mkState (OkO,UnfoldS,pp,l,ctx,n);
         IndR   -> let { [gl] = newGoalSymbols n 1}
                   in case refine l ctx' pp (IndBW gl) of 
                        {
                          Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
                          Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
                        };
         HSIndR   -> let { [gl] = newGoalSymbols n 1}
                     in case refine l ctx' pp (HSIndBW gl) of 
                        {
                          Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
                          Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
                        };
         SIndR   -> let { [gl] = newGoalSymbols n 1}
                    in case refine l ctx' pp (SIndBW gl) of 
                        {
                          Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
                          Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
                        };
         CoiR ->   let { [gl] = newGoalSymbols n 1}
                   in case refine l ctx' pp (CoiBW gl) of 
                        {
                          Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
                          Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
                        };
         HSCoiR ->   let { [gl] = newGoalSymbols n 1}  
                   in case refine l ctx' pp (HSCoiBW gl) of 
                        {
                          Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
                          Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
                        };    
         SCoiR ->  let { [gl] = newGoalSymbols n 1}   
                   in case refine l ctx' pp (SCoiBW gl) of 
                        {
                          Success pp' -> mkState (OkO,OkS, pp', l, ctx, n+1);
                          Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
                        };                        
         ClR   ->  case refine l ctx' pp CloBW of 
                      {
                        Success pp' -> mkState (OkO,OkS, pp', l, ctx, n);
                        Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l,ctx,n)
                      };
         CoclR  -> case refine l ctx' pp CoclBW of 
                     {
                       Success pp' -> mkState (OkO,OkS, pp', l, ctx, n);
                       Failure _   -> mkState (ErrorO IncorrectFormE,ipstate,pp,l, ctx,n)
                     } ;
         EqReflR -> case refine l ctx' pp EqReflBW of
                      {
                        Success pp' -> ok state {stplproof = pp'} ;
                        Failure _   -> state { output = ErrorO IncorrectFormE }
                      } ;
         EqSymR  -> let { [gl] = newGoalSymbols n 1 }
                    in case refine l ctx' pp (EqSymBW gl) of
                         {
                           Success pp' -> ok state { stplproof = pp', counter = n+1 };
                           Failure _   -> state { output = ErrorO IncorrectFormE }
                         } ;
         EqCongR  -> state { output = OkO, ips = EqCongS } ;
         EqCongfR  -> state { output = OkO, ips = EqCongfS } ; 
         EqCongruR  -> state { output = OkO, ips = EqCongruS } 
      }

completeCheck :: State -> State
completeCheck state = if null (plpglstack (stplproof state)) 
                      then state { output = OkO, ips = CompleteS } 
                      else state  
 