# Praωf

Praωf is an interactive proof assistant for program extraction 
based on Intuitionistic Fixed-Point Logic.

Version: 2020.001

Website: https://prawftree.wordpress.com/

Authors: U.Berger, O.Petrovska, H.Tsuiki

System requirements: 

	Supported OS: Linux, Windows, Mac OS

	Glasgow Haskell Compiler: recommended version 8.4.4; 
	    support for 8.8+ will be added shortly.
	
	User interface:
	    Emacs is a common user interface for Praωf.
	    For Windows, Praωf can run using WinGHCi.
		
Getting started:

Tutorial : https://prawftree.files.wordpress.com/2020/01/tutorial.pdf

Manual : https://prawftree.files.wordpress.com/2020/01/prawf_manual.pdf
	

