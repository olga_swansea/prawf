-- Program : Prawf; version 2020.001
-- Module : ListAux (aux)
-- Authors : U.Berger, O.Petrovska
-- Date created : 17 Jan 2020
-- Description : auxiliary functions to work on lists

{- Revision History  :
Date        Author      Ref    Description
-}

module ListAux -- aux

  where
  
-- haskell
import Data.List (intercalate)

-- aux
import Perhaps

-- Functions 
 -- union unions subset remove removes redumps mfirst ifnotnull showL
 -- getLR getLREq getRL deleteL getBdx getBdy getxs getys getzs 
 -- groupy grshowAssoc
 -- unionAssoc unionAssocs unionAssocEq 
 -- clashfix tl tr fwd initseg noinitseg noinitsegString
 -- freshVar freshVars createVars addPrimes 
 -- listToBool elems newStrs
 -- safetail findmatch checkmatch
 -- selectsegment dropmatch keepmatch dropstring xlist
 
-- Contents
-- ========
-- Operations on lists
-- Association lists
-- Creating fresh strings
-- Fresh variables


-- Operations on lists
-- ===================

-- union of lists
-- append to xs all elements of ys that are not already in xs
union :: Eq a => [a] -> [a] -> [a]
union xs ys = xs ++ [y | y <- ys, not (y `elem` xs)]

unions :: Eq a => [[a]] -> [a]
unions = foldr union []

-- subset relation
subset :: Eq a => [a] -> [a] -> Bool
subset xs ys = and [x `elem` ys | x <- xs]

-- removes x from xs
remove :: Eq a => a -> [a] -> [a]
remove x xs = [y | y <- xs, y /= x]

removes :: Eq a => [a] -> [a] -> [a]
removes xs ys = [y | y <- ys, not (y `elem` xs)]

-- remove duplicates
remdups :: Eq a => [a] -> [a]
remdups [] = []
remdups (x:xs) = x : remdups (filter (/= x) xs)

-- safe first
mfirst :: [a] -> Maybe a
mfirst xs = case xs of { (x:ys) -> Just x; _ -> Nothing }

-- empty vs nonempty
ifnotnull :: ([a] -> String) -> [a] -> String
ifnotnull f xs = if not (null xs) then '\n' : f xs else ""    

-- showing 
showL :: [String] -> String
showL strs = concat (map ('\n':) strs) 
-- Grouping

-- group test [x(1),...,x(n)] = [[x(i1),x(i1+1),...,],[x(i2),x(i2+1),...],...]
-- where test x(i) = True exactly if i = ik for some k.
group :: (a -> Bool) -> [a] -> [[a]]
group test xs = aux (dropWhile ntest xs)
  where
    ntest = not . test
    aux [] = []
    aux (x:xs) = (x:takeWhile ntest xs) : aux (dropWhile ntest xs)

-- Association lists
-- =================

getLR :: Eq a => [(a,b)] -> a -> Maybe b
getLR xys x = case [y | (x',y) <- xys, x' == x] of 
                {y:_ -> Just y ; _ -> Nothing}

getLR' :: Eq a => [(a,b)] -> a -> String -> Perhaps b
getLR' xys x s = case [y | (x',y) <- xys, x' == x] of 
                {y:_ -> Success y ; _ -> Failure s} 

getLREq :: (a -> a -> Bool) -> [(a,b)] -> a -> Maybe b
getLREq eqa xys x = case [y | (x',y) <- xys, x' `eqa` x] of 
                      {y:_ -> Just y ; _ -> Nothing} 

getRL :: Eq b => [(a,b)] -> b -> Maybe a
getRL xys y = case [x | (x,y') <- xys, y' == y] of 
                {x:_ -> Just x ; _ -> Nothing} 

deleteL :: Eq a => [(a,b)] -> [a] -> [(a,b)]
deleteL xys xs = [(x,y) | (x,y) <- xys, not (x `elem` xs)] 

-- getBdx xyzs x = first (y,z) such that ((x,y),z) in xyzs 
getBdx, getBdy :: [((String,String),a)] -> String -> 
                                        Maybe (String,a)
getBdx xyzs x = mfirst [(y,z) | ((x',y),z) <- xyzs, x' == x] 
getBdy xyzs y = mfirst [(x,z) | ((x,y'),z) <- xyzs, y' == y] 

getxs, getys :: [((String,String),(String,a))] -> [String]
getxs = map fst . map fst
getys = map snd . map fst

getzs :: [((String,String),(String,a))] -> [(String,a)]
getzs = map snd

groupy :: Eq b => [(a,b)] -> [([a],b)]
groupy xys = [([x | (x,y') <- xys, y' == y],y) 
                            | y <- remdups (map snd xys)] 

grshowAssoc :: Eq b => (a -> String) -> (b -> String) -> [(a,b)] -> String
grshowAssoc showx showy xys = intercalate ";  " (map aux (groupy xys))
  where aux (xs,y) = intercalate "," (map showx xs) ++ ": " ++ showy y

unionAssoc :: (Eq a, Eq b) => [(a,b)] -> [(a,b)] -> Maybe [(a,b)]
unionAssoc [] xys = Just xys
unionAssoc xys [] = Just xys
unionAssoc ((x,y):xys) (xys') = 
  case getLR xys' x of
    {
      Just y' -> if y == y' then unionAssoc xys xys' else Nothing ;
      Nothing -> do { xys'' <- unionAssoc xys xys' ; return ((x,y):xys'') }
    }

unionAssocs :: (Eq a, Eq b) => [[(a,b)]] -> Maybe [(a,b)]
unionAssocs xyss = foldr (mapply unionAssoc) (Just []) (map Just xyss) 

unionAssocEq :: (a -> a -> Bool) -> (b -> b -> Bool) -> [(a,b)] -> [(a,b)] -> Maybe [(a,b)]
unionAssocEq eqa eqb [] xys = Just xys
unionAssocEq eqa eqb xys [] = Just xys
unionAssocEq eqa eqb ((x,y):xys) (xys') = 
  case getLREq eqa xys' x of
    {
      Just y' -> if eqb y y' then unionAssocEq eqa eqb xys xys' else Nothing ;
      Nothing -> do { xys'' <- unionAssocEq eqa eqb xys xys' ; return ((x,y):xys'') }
    }

-- Creating fresh strings
-- ======================

clashfix :: String -> [String] -> String
clashfix av avs = if av `elem` avs 
                  then clashfix (av ++ "'") avs
                  else av

tl, tr, fwd :: String -> String -- turn left, turn right
tl u = u ++ "l"
tr u = u ++ "r"
fwd u = u ++ "f"

-- initseg xs ys means that xs is an initial segment of ys 
initseg :: Eq a => [a] -> [a] -> Bool
initseg (x:xs) (y:ys) = x == y && initseg xs ys
initseg [] _ = True
initseg _ _ =False

noinitseg :: [String] -> String -> Bool
noinitseg xs y = and [not (initseg y x) | x <- xs]

-- noinitsegString u xs = u' where u' is a variant of u which
-- is not an initial segment of any member of xs
noinitsegString :: String -> [String] -> String
noinitsegString u xs =
  head (filter (noinitseg xs) (iterate fwd u))
  

-- Fresh variables
-- =================
-- freshVar v vs is v'...' with enough '...' so that it is not in vs
freshVar :: String -> [String] -> String
freshVar v vs =
  if not (v `elem` vs)
     then v
  else
     let {
          freshVar' v vs n = if addPrimes v n `elem` vs
                             then freshVar' v vs (n + 1)
                             else addPrimes v n
          }
     in freshVar' v vs 0
     
-- updates all predicate variables and checks if they
-- don't clash with variables in (vars newt) ???
freshVars :: [String] -> [String] -> [String]
freshVars xs ys =
       if not (xs `elems` ys)
       then xs
       else freshVar' 0
  where
    freshVar' n =
      let { xs' = [addPrimes x n| x <- xs] }
      in if xs' `elems` ys
         then freshVar' (n + 1)
         else xs'


createVars :: Int -> [String]
createVars n = ["x" ++ show i | i <- [1..n]]

addPrimes :: String -> Int -> String
addPrimes v n = v ++ take n (repeat '\'')

-- if two lists have elements in common then list of bools
listToBool :: [String] -> [String] -> [Bool]
listToBool xs ys = [x `elem` ys | x <- xs]

-- if two lists have elements in common
elems :: [String] -> [String] -> Bool
elems xs ys = not (null (filter (`elem` ys) xs)) -- or(listToBool xs ys)

-- creating from a seed u n new strings and a new seed
newStrs :: Int -> String -> ([String],String)
newStrs n u = let { us = take (n+1) (iterate fwd u) }
              in (init us,last us)

-- This is in fact an unsafe tail since an error won't be noticed
-- use tail instead
safetail :: [a] -> [a]
safetail [] = []
safetail (x:xs) = xs

findmatch :: Eq a => [a] -> [(a,b)] -> [(a,b)]
findmatch xss@(x:xs) yzs = 
   if xss == []
   then []
   else let {tri = [(x',y') | x' <- xss, y' <- yzs ]}
        in checkmatch tri
        
checkmatch :: Eq a => [(a,(a,b))] -> [(a,b)]
checkmatch [] = []
checkmatch ((x,(y,z)):xyz) = if x == y
                             then (y,z) : checkmatch xyz
                             else checkmatch xyz

-- Selecting segment in string:
-- If selectsegment s b e = s' /= "",
-- then s = s1 ++ b ++ s' ++ e ++ s2
-- where b is not a segment of s1 and s2 is not a segment of s'
selectsegment :: String -> String -> String -> String
selectsegment s b e = keepmatch (dropmatch s b) e

dropmatch, keepmatch :: String -> String -> String

-- if dropmatch s b = s' /= "",
-- then s = s1 ++ b ++ s' where b is not a segment of s1
dropmatch "" _ = ""
dropmatch s b = 
  case dropstring s b of
    {
      Just s' -> s' ;
      Nothing -> dropmatch (tail s) b
    }

-- if keepmatch s e = s' /= "", 
-- then s = s' ++ e ++ s2 where e is not a segment of s'
keepmatch s e = aux "" s
  where
   aux t s = 
     case dropstring s e of
       {
         Just _  -> reverse t ;
         Nothing ->  case s of
                       {
                         x:s' -> aux (x:t) s' ;
                         ""   -> ""
                       }
       }

-- dropstring s b = Just s' iff s = b ++ s'
dropstring :: String -> String -> Maybe String  
dropstring (x:s) (y:b) = if x == y then dropstring s b else Nothing
dropstring s _ = Just s

xlist :: Int -> String -> [String]
xlist n x = [x++(show i) | i <- [1..n]]


