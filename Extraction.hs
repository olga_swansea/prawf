-- Program : Prawf; version 2020.001
-- Module : Extraction (core)
-- Authors : U.Berger, O.Petrovska
-- Date created : 19 Jan 2020
-- Description : Defines program extraction procedure

{- Revision History  :
Date        Author      Ref    Description
20 01       OPUB                 added HSInd
23 01       OP                   added SInd
26 01       OP                   added normDeclF' to unfold possible predicates
-}

module Extraction where   -- core

-- haskell
import Data.List (nub)

-- aux
import Perhaps
import ListAux

-- core
import Program
import Proof
import Language

-- ==================================  
-- Functions 
 -- extract pe pe'
 -- epGInd epInd epHSInd epSInd
 -- epGCoi epCoi epHSCoi epSCoi

-- ==================================

extract :: Language -> Context -> [Decl] -> 
          [Assumpt] -> Proof -> Perhaps Program
extract l ctx decls assu proof = 
  case (pe l ctx decls assu proof) of
    {
     Success prog -> let { prog' = normProg prog }
                     in Success prog';
     Failure message -> fail ("Extraction.hs extract:\n" ++ message) 
    }

pe :: Language -> Context -> [Decl] -> 
          [Assumpt] -> Proof -> Perhaps Program
pe l ctx decls assu p = 
       pe' l ctx decls assu [] (transIFP l ctx decls p)

 
-- [String] stands for list of harrop predicate variables
pe' :: Language -> Context -> [Decl] -> 
          [Assumpt] -> [String] -> Proof' -> Perhaps Program
pe' l ctx decls assu hpvs p = 
  case (endFormula' l ctx decls assu p) of
   {Failure x -> Failure ("pe' endFormula':\n" ++ x);
    Success f ->
     if harropF hpvs decls f
     then return nilProg
     else let {ar = allArity f} in  -- needed in ind and coind  
         case p of 
           {   -- !! 

           AnProof label -> return (ProgVar label);
           AndIntr p1 p2 ->
             do
              {
               endf1  <- endFormula' l ctx decls assu p1;
               endf2  <- endFormula' l ctx decls assu p2;
               prog1  <- pe' l ctx decls assu hpvs p1;
               prog2  <- pe' l ctx decls assu hpvs p2;
               if harropF hpvs decls endf1
               then return prog2
               else if harropF hpvs decls endf2
                    then return prog1
                    else return (ProgCon Pair [prog1,prog2])
              };
           AndElil p1 ->
             do
               {
                endf <- endFormula' l ctx decls assu p1;
                prog <- pe' l ctx decls assu hpvs p1;
                let {a = "a_ell"; b = "b_ell";
                    endf' = normDeclF' decls endf};
                case normF endf' of
                   {
                    And f1 f2 ->
                    if harropF hpvs decls f2
                    then return prog
                    else return (ProgCase prog 
                                [(Pair, [a,b], ProgVar a)]);
                    g        ->  Failure ("pe' AndElil\n" ++ show g);
                   }
                };
           AndElir p1 ->
              do
                {
                 endf <- endFormula' l ctx decls assu p1;
                 prog <- pe' l ctx decls assu hpvs p1;
                 let {a = "a_elr"; b = "b_elr";
                     endf' = normDeclF' decls endf};
                 case normF endf' of
                   {
                    And f1 f2 ->
                       if harropF hpvs decls f1
                       then return prog
                       else return (ProgCase prog 
                               [(Pair, [a,b], ProgVar b)]);
                    g         -> Failure ("pe' AndElir:\n" ++ show g);
                    }
                 };                 
           ImpIntr p1 a@(label,g) ->
               do {
                   prog <- pe' l ctx decls (a:assu) hpvs p1; 
                   if harropF hpvs decls g
                   then return prog
                   else return (ProgAbst label prog);
                   };
           ImpElim p1 p2 ->
               do
                 {
                  f1    <- endFormula' l ctx decls assu p1;
                  prog1 <- pe' l ctx decls assu hpvs p1;
                  prog2 <- pe' l ctx decls assu hpvs p2;
                  let {f1' = normDeclF' decls f1};
                  case f1' of
                     {
                      Imp f11 _ ->
                         if harropF hpvs decls f11
                         then return prog1
                         else return (ProgApp prog1 prog2);
                      g           -> Failure ("pe' ImpElim:\n" ++ show g);
                       }
                   };
           OrIntrl p1 _ ->
               do {
                   prog <- pe' l ctx decls assu hpvs p1;
                   return (ProgCon Lt [prog]);
                  };
           OrIntrr p1 _ -> 
               do {
                   prog <- pe' l ctx decls assu hpvs p1;
                   return (ProgCon Rt [prog]);
                  };
           OrElim p1 p2 p3 ->
            do
              {
               prog1 <- pe' l ctx decls assu hpvs p1;
               prog2 <- pe' l ctx decls assu hpvs p2;
               prog3 <- pe' l ctx decls assu hpvs p3;
               endf  <- endFormula' l ctx decls assu p1;  
               let {a = "a_ore"; b = "b_ore";
                   endf' = normDeclF' decls endf};
               case endf' of
                 {Or g1 g2 -> 
                   if harropF hpvs decls g1
                   then if harropF hpvs decls g2
                        then return (ProgCase prog1 
                           [(Lt, [a], prog2), (Rt, [b], prog3)])
                        else return (ProgCase prog1 
                           [(Lt, [a], prog2), 
                            (Rt, [b], ProgApp prog3 (ProgVar b))])
                   else if harropF hpvs decls g2 
                        then return (ProgCase prog1 
                           [(Lt, [a], ProgApp prog2 (ProgVar a)),
                            (Rt, [b], prog3)])
                        else return (ProgCase prog1 
                           [(Lt, [a], ProgApp prog2 (ProgVar a)),
                           (Rt, [b], ProgApp prog3 (ProgVar b))]);
                  g -> Failure ("pe' OrElim:\n" ++ show g)
                  };
              };
           ExFQ p1 _ -> Success nilProg ;
           
           AllIntr p1 (x,s) -> 
                  pe' l (addCVars ctx [(x,s)]) decls assu hpvs p1;
           
           AllElim p1 _    -> 
                  pe' l ctx decls assu hpvs p1;
  
           ExIntr p1 (x,s) _ _ -> 
                  pe' l (addCVars ctx [(x,s)]) decls assu hpvs p1;
  
           ExElim p1 p2 ->
             do
               {
                f2    <- endFormula' l ctx decls assu p2;
                prog1 <- pe' l ctx decls assu hpvs p1;
                prog2 <- pe' l ctx decls assu hpvs p2;
                let {f2' = normDeclF' decls f2};
                case f2 of
                  {
                   All _ (Imp f21 _) -> 
                      if harropF hpvs decls f21
                      then return prog1
                      else return (ProgApp prog2 prog1);
                   g                 -> Failure ("pe' ExElim:\n" ++ show g);
                  }
               };
  
           LetP decl p1 -> -- need to reverse list of declarations here!
                pe' l ctx (decls ++ [decl]) assu hpvs p1;
           Ind pred p1 -> error " Ind not in Proof'";                       
           HSInd pred p1 -> error " HSInd not in Proof'";
           SInd pred p1 -> error " SInd not in Proof'";
           Coi pred p1 -> error " Coi not in Proof'";
           HSCoi pred p1 -> error " HSCoi not in Proof'";
           SCoi pred p1 -> error " SCoi not in Proof'";                      
           Ind' pred p1 x y p2 -> 
             case lookupPred decls pred of
              {
                Mu op -> epInd l ctx decls assu hpvs (p1,ar,x,y,p2) op;
                q  -> fail ("ep' Ind':\n" ++ show pred ++"\n" ++ show q) 
              };
           HSInd' pred p1 x y p2 ->
             case lookupPred decls pred of
              {
                Mu op -> epHSInd l ctx decls assu hpvs (p1,ar,x,y,p2) op;
                q  -> fail ("Extraction.hs ep' HSInd':\n" ++ show pred++"\n" ++ show q) 
              };
           SInd' pred p1 x y p2 ->
             case lookupPred decls pred of
              {
                Mu op -> epSInd l ctx decls assu hpvs (p1,ar,x,y,p2) op;
                q  -> fail ("Extraction.hs ep' SInd':\n" ++ show pred++"\n" ++ show q) 
              };
           Coi' pred p1 x y p2 ->
             case lookupPred decls pred of
              {
                Nu op -> epCoi l ctx decls assu hpvs (p1,ar,x,y,p2) op;
                q  -> fail ("Extraction.hs ep' Coi':\n" ++ show pred++"\n" ++ show q) 
              };
           HSCoi' pred p1 x y p2 ->
             case lookupPred decls pred of
              {
                Nu op -> epHSCoi l ctx decls assu hpvs (p1,ar,x,y,p2) op;
                q  -> fail ("Extraction.hs ep' HSCoi':\n" ++ show pred++"\n" ++ show q) 
              };
           SCoi' pred p1 x y p2 ->
             case lookupPred decls pred of
              {
                Nu op -> epSCoi l ctx decls assu hpvs (p1,ar,x,y,p2) op;
                q  -> fail ("Extraction.hs ep' SCoi':\n" ++ show pred++"\n" ++ show q) 
              };
           Clo _ -> return $ idProg;                        
           Cocl _ -> return $ idProg;
           EqRefl _ -> return $ nilProg;
           EqSym _ -> return $ nilProg;
           EqCong p1 ps pred -> pe' l ctx decls assu hpvs p1;
           Rep f p1 -> pe' l ctx decls assu hpvs p1;
           _ -> return $ idProg; 
         }
      }
           

-- Helper functions

epInd, epHSInd, epSInd :: Language -> Context -> [Decl] -> [Assumpt] ->
   [String] -> (Proof, Arity, String, String, Proof) -> Op -> Perhaps Program
epInd = epGInd "norm"
epHSInd = epGInd "hs"
epSInd = epGInd "strong"

epGInd :: String -> Language -> Context -> [Decl] -> [Assumpt] ->
   [String] -> (Proof, Arity, String, String, Proof) -> Op -> Perhaps Program
epGInd option l ctx decls assu hpvs (p1,ar,x,y,p2) op = 
  do {                            -- p1 : mu op sub P, p2 : mon op
       prog1 <- pe' l ctx decls assu hpvs p1; -- step
       prog2 <- pe' l (addCPVars ctx [(x,ar),(y,ar)]) decls assu hpvs p2; -- mon
       prog2' <- pe' l (addCPVars ctx [(x,ar),(y,ar)]) decls assu (x:hpvs) p2; 
       let {f = freshVar "f_mu" ((fvProg prog1) `union` (fvProg prog2))};
       if not $ harropO hpvs $ normDeclO decls op   -- we know that P is not Harrop
       then -- op non-Harrop, P non-harrop
         case option of
          {
            "norm" -> -- Ind, op non-harrop
              return $ ProgRec $ ProgAbst f 
                (comp prog1 (ProgApp prog2 (ProgVar f))) ;
            "hs" -> --HSInd, Phi Harrop
              return $ ProgRec $ ProgAbst f 
                (comp prog1 (funpair idProg (ProgApp prog2 (ProgVar f)))) ;
            "strong" ->  --SInd, Phi Harrop
               return $ ProgRec $ ProgAbst f 
                 (comp prog1 (ProgApp prog2 (funpair idProg (ProgVar f)))) 
          }
       else -- op Harrop, then norm, hs, strong all the same
         return $ ProgRec $ ProgAbst f 
           (ProgApp prog1 (ProgApp prog2' (ProgVar f)))
     }


epCoi, epHSCoi, epSCoi :: Language -> Context -> [Decl] -> [Assumpt] ->
   [String] -> (Proof, Arity, String, String, Proof) -> Op -> Perhaps Program
epCoi = epGCoi "norm"
epHSCoi = epGCoi "hs"
epSCoi = epGCoi "strong"

epGCoi :: String -> Language -> Context -> [Decl] -> [Assumpt] ->
   [String] -> (Proof, Arity, String, String, Proof) -> Op -> Perhaps Program
epGCoi option l ctx decls assu hpvs (p1,ar,x,y,p2) op = 
  do {                            -- p1 : P sub nu op, p2 : mon op
       prog1 <- pe' l ctx decls assu hpvs p1; -- costep
       prog2 <- pe' l (addCPVars ctx [(x,ar),(y,ar)]) decls assu hpvs p2; -- mon op 
       prog2' <- pe' l (addCPVars ctx [(x,ar),(y,ar)]) decls assu (x:hpvs) p2;
       let {f = freshVar "f_nu" ((fvProg prog1) `union` (fvProg prog2))};
       g <- endFormula' l ctx decls assu p1 ; -- g = P sub op P
       let { 
             g' = normDeclF decls g ;
             (_,g0) = splitquant g'  -- Language.hs: g = all xss g0 
           } ; 
       g1 <- premiseImp g0 ; -- Language.hs: g0 = g1 -> g2, i.e. P = \xss g1
       if not $ harropF hpvs decls g1 
       then case option of   -- P non-Harrop
              {
                "norm" -> 
                  return $ ProgRec $ ProgAbst f 
                    (comp (ProgApp prog2 (ProgVar f)) prog1) ;
                "hs" ->
                   return $ ProgRec $ ProgAbst f 
                     (comp (funsum (ProgApp prog2 (ProgVar f)) idProg) prog1) ;
                "strong" -> 
                   return $ ProgRec $ ProgAbst f 
                     (comp (ProgApp prog2 (funsum (ProgVar f) idProg)) prog1) 
              } 
       else case option of   -- P Harrop
              {
                "norm" -> 
                  return $ ProgRec $ ProgAbst f 
                    (ProgApp (ProgApp prog2' (ProgVar f)) prog1) ;
                "hs" -> 
                   return $ ProgRec $ ProgAbst f 
                     (ProgApp (funsum (ProgApp prog2' (ProgVar f)) idProg) prog1) ;
               "strong" ->
                  return $ ProgRec $ ProgAbst f 
                    (ProgApp 
                       (ProgApp prog2 (funsum (ProgAbst "dummy" (ProgVar f)) idProg))
                       prog1)
              }
     }     



