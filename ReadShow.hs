-- Program : Prawf; version 2020.001
-- Module : ReadShow (gen)
-- Authors : U.Berger, O.Petrovska
-- Date created : 17 Jan 2020
-- Description : Interprets user input strings - parsing and pretty-printing 
              -- for user interaction - and displays error messages and hints

{- Revision History  :
Date        Author      Ref    Description
-}

module ReadShow -- gen

  where

-- haskell
import Indentprint
import Data.List (intercalate)
import GHC.IO
import Control.Exception

-- aux
import Perhaps
import ListAux as LA

-- core
import Language
import Proof

-- gen
import Parser
import PlProof
import Step

-- ========================
 
-- Functions 
 -- str2Input str2Inputs 
 -- showState
 -- execmodetext 
 -- helptext fullhelp focusedhelp readmanual
 -- starttag endtag 
 -- quithelp newhelp manualhelp 
 -- showPlp showGlStack showProof showLang showType showFuns showPOs
 -- showCtx showSorts showConstants showCtxVars
 -- ifnotnull' ifnotnullctx
 -- showArity showCtxOPVars showCtxPVars
 -- showAssumpts showAssumpt 
 -- showDecls showTerm showTermB showTerms
 -- showFormula showF isinfix showinfix infixprednames infixfunname
 -- showFB showVarSort showP showOp showD
 -- showGoals showGoal' showGoal showPerhapsGoal 
 -- showThms showThm showThmR showThmE showThmI showPRs showPR
 -- showError displayError logo
 -- mReadFile mReadFileFlex 
 -- get1stline st2e st2e' remcomment
 
-- Contents
-- ========
-- Commands (input)
-- State printout
-- Execution mode 
-- Help
-- Showing
  -- partial proof
  -- goalstack
  -- (indented) proof
  -- language, context (type, functions, preds, ops, sorts, constants)
  -- Assumptions
  -- Declarations
  -- Terms
  -- Formula
  -- Theorems
  -- Proof records
  -- Errors
  -- Logo
-- Reading from files
-- Helper functions for tactics display and theorems


-- Commands
-- ==============================

str2Input :: String -> Input
str2Input s =
  case s of
   {
    "use"     -> RuleI AssuR;
    "usw"     -> RuleI UseWithR;
    "ust"     -> RuleI UseThmR;
    "ustw"    -> RuleI UseThmWithR;
    "andi"    -> RuleI AndiR;
    "andel"   -> RuleI AndelR;
    "ander"   -> RuleI AnderR;
    "andLeft" -> RuleI AndLeftR;
    "impi"    -> RuleI ImpiR;
    "impe"    -> RuleI ImpeR;
    "impLeft" -> RuleI ImpLeftR;
    "oril"    -> RuleI OrilR;
    "orir"    -> RuleI OrirR;
    "ore"     -> RuleI OreR;
    "ora"     -> RuleI OreAssuR;
    "orLeft"  -> RuleI OrLeftR;
    "efq"     -> RuleI EfqR;
    "raa"     -> RuleI RaaR;
    "alli"    -> RuleI AlliR;
    "alle"    -> RuleI AlleR;
    "falle"   -> RuleI AlleRF;
    "exi"     -> RuleI ExiR;
    "exe"     -> RuleI ExeR;
    "cut"     -> RuleI CutR;
    "leti"    -> RuleI LetR;
    "decl"    -> RuleI DeclR;
    "ind"     -> RuleI IndR;
    "hsind"   -> RuleI HSIndR; 
    "sind"    -> RuleI SIndR;     
    "coind"   -> RuleI CoiR;
    "hscoind" -> RuleI HSCoiR;
    "scoind"  -> RuleI SCoiR;
    "cl"      -> RuleI ClR;
    "cocl"    -> RuleI CoclR;
    "refl"    -> RuleI EqReflR;
    "sym"     -> RuleI EqSymR;
    "cong"    -> RuleI EqCongR;
    "congf"   -> RuleI EqCongfR;
    "congru"  -> RuleI EqCongruR;
    "norm"    -> RuleI NormR;
    "unfold"  -> RuleI UnfoldR;
    _         -> TextI s
   }


-- improved definition which doesn't require input strings to be prefix free
str2Inputs :: String -> [Input]  -- used in Step.hs
str2Inputs s =
  case words s of
    {
      (w:ws) -> 
         let { inp = str2Input w }
         in case inp of
              {
                RuleI _ -> inp : if null ws then [] else [TextI (concat ws)] ;
                _       -> [TextI s]
              } ;
      _ ->  [TextI s]
    }    
      
      
-- State printout
-- ==============================
showState :: State -> String
showState state =
  let {
        out = output state;
        ipstate = ips state;
        pp = stplproof state;
      }
  in "\n --------------------------- \n" ++
   showPerhapsGoal (currentGoal pp)  
   ++ "\n" ++ -- currentGoal in Proof 
 case out of
  {
   ErrorO err -> showError err;
   StartO     -> "Enter goal formula ";
   OkO        -> 
     case ipstate of
      {
       QuitS     -> "Bye\n";
       ExProgS   -> execmodetext;
       OkS       -> "Enter command";            
       StartS    -> "Enter goal formula"; 
       CompleteS -> 
          "Proof complete.\nEnter quit, savethm <file_name> <thm_name>, extract <name>, exec (to switch to execution mode), new, or ? for more commands";
       UseS      -> "Enter assumption variable";
       UseWithS  -> "Enter assumption variable";
       UseThmS   -> "Enter theorem name";
       ImpiS     -> "Enter assumption variable";
       ImpeS     -> "Enter missing formula X";
       ImpLeftS  -> "Enter missing label";
       AndelS    -> "Enter missing formula X";
       AnderS    -> "Enter missing formula X";
       AndLeftS  -> "Enter missing label";
       OreS      -> "Enter missing formula of the form  X or Y or else assumptionn label";
       OreAssuS  -> "Enter missing assumption label";
       OrLeftS   -> "Enter missing label";
       AlleS     -> "Enter the term you wish to generalise";
       AlleSF    -> "Enter the quantified formula of the form: all x A(x)";
       ExiS      -> "Enter a term that should substitute the variable";
       ExeS      -> "Enter missing formula of the form: ex x A(x)";
       CutS       -> "Enter missing formula X";
       DeclS     -> "Enter missing declaration";
       EqCongS   -> "Enter missing formula ";
       EqCongfS  -> "Enter missing formula ";
       EqCongruS -> "Enter missing predicate and equations ";
       NormS     -> "Enter missing formula ";
       UnfoldS   -> "Enter predicate or operator variable to unfold "
      } 
  }

-- Execution mode
-- ==============================
execmodetext :: String
execmodetext = showL ["You are now in the exec mode.", 
                "Type em to see available programs and instructions",
                 "Type prover to return to the prover mode.",""]

-- Help 
-- ==============================
helptext :: [String] -> IO String
helptext ws = 
  case ws of { [] -> return fullhelp ; (w:_) ->  focusedhelp w }

fullhelp :: String
fullhelp = showL ["\nType",
           "?p for information about proof commands",
           "?c for information about control commands",
           "? <command> for information about a particular command",
           "Full manual is available here: " ++ manuallink]
           

-- displays specific chapter from the manual
focusedhelp :: String -> IO String
focusedhelp key = 
  do {
       manual <- readFile "manual.txt" ;
       let { info = readmanual manual key };
       if null info 
       then return ("Sorry, no help available for " ++ key)
       else return info
     }

readmanual :: String -> String -> String
readmanual manual key = 
  selectsegment manual (starttag key) (endtag key)  -- ListAux

starttag, endtag :: String -> String
starttag key = "<" ++ key ++ ">"
endtag key = "</" ++ key ++ ">"

quithelp, newhelp, manualhelp, manuallink :: String 

quithelp = "\n quit:\n end the prover program "++
           "(you will loose the proof if you didn't submit it).\n"

newhelp =  "\n new:\n start a new proof.\n"   

manualhelp = "To access the User Manual go to " ++ manuallink

manuallink = "https://prawftree.wordpress.com/"

-- Showing
-- ==============================
-- partial proof
showPlp :: PlProof -> String
showPlp pp = " \n Partial proof : \n Goal stack: \n" ++ showGlStack (plpglstack pp) ++
            "\n Assumptions : \n" ++ showAssumpts (plpassumpts pp) ++
            "\n Declarations : \n" ++ showDecls (plpdecls pp) ++
            "\n" ++ showThms (plpthms pp) ++
            "\n Proof : \n" ++ showProof (plpproof pp)
        
-- goalstack        
showGlStack :: GlStack -> String
showGlStack [] = ""
showGlStack (g:gs) = showGoal g ++ showGlStack gs

-- displays indented proof
showProof :: Proof -> String
showProof p = indentproof 0 p

-- language
showLang :: Language -> String
showLang l = "LANGUAGE: \n Sorts: " ++ showSorts (sorts l) ++
             "\n Constants: " ++ showConstants (constants l) ++ 
             "\n Functions: " ++ showFuns (functions l) ++
             "\n Predicates: " ++ showPOs (predicates l) ++
             "\n Operators: " ++ showPOs (operators l)
            
showType :: Type -> String
showType (ar,sort) = showArity ar ++ "->" ++ sort

showFuns :: [(String,Type)] -> String
showFuns fs = grshowAssoc id showType fs
     
-- shows predicates or operators defined in the language     
showPOs :: [(String,Arity)] -> String
showPOs [] = []
showPOs ((str,ar):xs) = "(" ++ str ++ ", " ++ show (length ar) ++ ")" 
                           ++ showPOs xs
-- Context
showCtx :: Context -> String
showCtx c = ifnotnull showCtxVars (variables c) ++
            ifnotnull showCtxOPVars (ovars c) ++
            ifnotnull showCtxPVars (pvars c)

showSorts :: [Sort] -> String
showSorts = intercalate ", "

showConstants :: [(String,Sort)] -> String
showConstants = grshowAssoc id id
         
showCtxVars :: [(String,Sort)] -> String
showCtxVars xs = " variables: " ++ grshowAssoc id id xs

ifnotnull' :: ([a] -> String) -> [a] -> String -> String
ifnotnull' f xs str = if not (null xs) then (str ++ "\n") ++ (f xs) else ""   

ifnotnullctx :: Context -> String
ifnotnullctx c = if not (null (showCtx c)) then ("\nContext of the goal:") ++ (showCtx c) else ""  

showArity :: Arity -> String
showArity ss = "(" ++ intercalate "," ss ++ ")"

showCtxOPVars :: [(String,Arity)] -> String
showCtxOPVars os = " ovars: " ++ grshowAssoc id showArity os

showCtxPVars :: [(String,Arity)] -> String
showCtxPVars xs = " pvars: " ++ grshowAssoc id showArity xs

-- Assumptions
showAssumpts :: [Assumpt] -> String
showAssumpts gamma = intercalate "\n" $ (map showAssumpt gamma)

showAssumpt :: Assumpt -> String
showAssumpt (u,f) = " " ++ u ++ " : " ++ showFormula f

-- Delcarations
showDecls :: [Decl] -> String
showDecls [] = ""
showDecls (d:ds) = " " ++ (showD d "\n") ++ showDecls ds

-- Terms
showTerm :: Term -> String
showTerm t = case t of
   {
    Const c    -> c;
    Var x      -> x;
    Fun fun ts -> if (fun `elem` infixfunnames) && (length ts == 2)
                  then showTermB (ts!!0) ++ fun ++ showTermB (ts!!1)
                  else fun ++ " (" ++ (intercalate "," (map showTerm ts)) ++ ") "
   }
   
showTermB :: Term -> String
showTermB t@(Fun fun ts) = 
    if (fun `elem` infixfunnames)
    then "(" ++ showTerm t ++ ")"
    else showTerm t
showTermB t = showTerm t 
   
showTerms :: [Term] -> String
showTerms [] = []
showTerms (t:ts) = 
  "(" ++ showTerm t ++ concat ["," ++ showTerm s |  s <- ts] ++ ")"
  
-- Formula
showFormula :: Formula -> String
showFormula f = showF f ""

showF :: Formula -> String -> String
showF (Predic p ts) str = 
    if isinfix p
    then showTerm (ts!!0) ++ showinfix p ++ showTerm (ts!!1) ++ str
    else showP p (showTerms ts ++ str)
showF (And f1 f2) str = showFB f1 (" & " ++ showFB f2 str)
showF (Or f1 f2) str = showFB f1 (" v " ++ showFB f2 str)
showF (Imp f1 f2) str = showFB f1 (" -> " ++ showFB f2 str)
showF (Bot) str = "bot" ++ str
showF (All (x,s) f) str = "All (" ++ x ++ ":" ++ s ++ ") " ++ showFB f str
showF (Ex (x,s) f) str = "Ex (" ++ x ++ ":" ++ s ++ ") " ++ showFB f str
showF (LetF d f) str = "Let " ++ showD d (" in " ++ showFB f str)

isinfix :: Predicate -> Bool
isinfix p = case p of { PrConst a -> a `elem` infixprednames ; _ -> False }

showinfix :: Predicate -> String
showinfix (PrConst a) = a

infixprednames :: [String]
infixprednames = ["=","<","<="]

infixfunnames :: [String]
infixfunnames = ["+","-","*",":"]

showFB :: Formula -> String -> String
showFB f str = case f of
  { Bot -> showF f str;
    Predic _ _ -> showF f str;
    _          -> "(" ++ showF f (")" ++ str)
  }
  
showVarSort :: [(String,Sort)] -> String
showVarSort l = intercalate "," $ l'
    where l' = [(x ++ ":" ++ s) | (x,s) <- l]

-- take care of infix predicates!
showP :: Predicate -> String -> String
showP (PrVar x) str = x ++ str
showP (PrConst a) str = a ++ str
showP (Compr xss f) str = 
       "lambda (" ++ (showVarSort xss) ++ ") " ++ (showF f str) 
       --"{" ++ show xss ++ " | " ++ showF f ("}" ++ str) ++ show xss
showP (Mu o) str = "Mu(" ++ showOp o (")"++ str)  
showP (Nu o) str = "Nu(" ++ showOp o (")"++ str)
showP (OpApp o p) str = showOp o ("(" ++ showP p (")" ++ str))

showOp :: Op -> String -> String
showOp (OpA (pv,ar) p) str = "(\\" ++ pv ++ ":(" ++ (unwords ar) ++ ") " ++ showP p (")" ++ str)
showOp (OpV ov) str = ov ++ str

showD :: Decl -> String -> String
showD (PredDecl (pv,ar) p) str = pv ++ ":(" ++ (unwords ar) ++ ") = " ++ showP p str
showD (OpDecl (ov,ar) o) str = ov ++ ":(" ++ (unwords ar) ++ ") = " ++ showOp o str 

-- Goals
-- this is used to only see all the goals without detailed info
showGoals :: GlStack -> String
showGoals [] = "No goals to show"
showGoals xs = intercalate " \n\n" $ map showGoal' xs

showGoal' :: Goal -> String
showGoal' g = 
    ifnotnull' showAssumpts (gassumpts g) "Assumptions: " ++ 
    ifnotnullctx (gctx g) ++
    "\nGoal: \n |- " ++ showAssumpt (ggoal g) 

-- this is used to show the current goal
showGoal :: Goal -> String
showGoal g = 
    ifnotnull'  showDecls (gdecls g) "Declarations: " ++ 
    ifnotnull' showAssumpts (gassumpts g) "Assumptions: " ++ 
    ifnotnullctx (gctx g) ++
    "\nCurrent goal: \n |- " ++ showAssumpt (ggoal g) 

-- shows all goal information
showPerhapsGoal :: Perhaps Goal -> String
showPerhapsGoal (Success g) = showGoal g
showPerhapsGoal (Failure s) = s

-- Theorems
-- showing ThmIs 
showThms :: [ThmI] -> String
showThms thms = intercalate "\n" $ (map showThm thms)

showThm :: ThmI -> String
showThm (thmn,(f,_)) = " " ++ thmn  ++ " : " ++ showFormula f

-- showing ThmE
-- used for saving theorems into a file
showThmE :: ThmE -> String
showThmE (thmn,(f,prec)) = thmn ++ " . " ++ 
                  showFormula f ++ " . " ++ 
                  show prec
    
-- showing ThmI (experimental)
showThmI :: ThmI -> String
showThmI (thmn,(f,p)) = thmn ++ " . " ++ 
                  showFormula f ++ " . " ++ 
                  show p

-- showing proof records
showPRs :: [ProofRecord] -> String
showPRs precs = intercalate "\n" $ (map showPR precs)

showPR :: ProofRecord -> String
showPR prec = " " ++ showLang (prlang prec) ++ ", "
                   ++ showCtx (prctx prec) ++ ", "
                   ++ showAssumpts (praxi prec) ++ ", "
                   ++ showDecls (prdecl prec) ++ ", "
                   ++ showProof (prproof prec)

-- Errors
showError :: Err -> String
showError err =
  case err of
    IncorrectTermE     -> "Incorrect term, try again"
    IncorrectFormE     -> "Incorrect formula, try again"
    IncorrectPredE     -> "Incorrect predicate, try again"
    IncorrectDeclE     -> "Incorrect declaration, try again"
    NoTextExpE         -> "No text expected, try again"
    UnknownAssuE       -> "Assumption doesn't fit or doesn't exist, try again"
    RuleNotApplicableE -> "Rule not applicable, try again"
    ProofCompleteE     -> "Proof complete, nothing to do"
    TextExpE           -> "Text input expected, try again"
    RefinementE s      -> "Failed to refine: " ++ s    
    FileNotFoundE      -> "File doesn't exist"
    UseWithE s         -> "usw not applicable: " ++ s
    UseThmE            -> "ust not applicable"
    UseThmWithE        -> "ustw not applicable"
    CongE              -> "cong not applicable"
    CongfE             -> "congf not applicable"
    CongruE            -> "congru not applicable"
    

displayError :: Err -> IO ()
displayError err = putStr (showError err ++ "> ")
 
-- Logo
logo :: String
logo = 
    "======================================================== \n" ++
    "                      +-+-+-+-+-+ \n" ++
    "                      |P|r|a|w|f| \n" ++
    "                      +-+-+-+-+-+ \n" ++
    "======================================================== \n" ++
    "            Proving and extracting programs with         \n" ++
    "               Intuitionistic Fixed Point Logic          \n" ++
    "======================================================== \n"
    
-- Reading from files
-- ==================================
mReadFile :: String -> IO (Maybe String)
mReadFile fn = 
   do {
        x <- try (readFile fn) :: IO (Either IOException String);
        case x of
          {
            Left _  -> return Nothing ;
            Right s -> return (Just s)
          }
      }

-- mReadFileFlex [fn1,...] succeeds if one of the filenames fni exists
mReadFileFlex :: [String] -> IO (Maybe String)
mReadFileFlex fns = sequence (map mReadFile fns)  >>= return . findJust

-- Helper functions for tactics display and theorems
-- =================================================
-- gets the first line of a tactic file
get1stline :: String -> IO String
get1stline fn = 
  do { cont <- readFile ("tactics/" ++ fn ++ ".txt");
       case lines cont of
         {
          [] -> error "no content";
          l:_ -> return l
         }
      }

-- prints out a combined string
-- tacname : tacformula
st2e :: (String, IO String) -> IO ()
st2e (x,s) = do putStr (x ++ " : ")
                s >>= putStrLn

st2e' :: [(String, IO String)] -> IO ()
st2e' [] = putStr ""
st2e' ((x,s):xs) = do {st2e (x,s);
                     st2e' xs}

-- removes comments
remcomment :: String -> String
remcomment s = case s of 
                 { 
                   '-':'-':_ -> "" ; 
                   c:s'      -> c:remcomment s' ;
                   _         -> ""
                 }

-- reducing to lines starting with a non-space
reducelines :: [String] -> [String]
reducelines = map concat . group (\line-> head line /= ' ')                                               
