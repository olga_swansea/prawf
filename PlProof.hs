-- Program : Prawf; version 2020.001
-- Module : PlProof (gen)
-- Authors : U.Berger, O.Petrovska, H.Tsuiki
-- Date created : 19 Jan 2020
-- Description : Defines partial proof and refines proofs

{- Revision History  :
Date        Author      Ref    Description

-}

--Note : introduced left rules by YS (to add extraction 30.01.2020)

module PlProof  -- gen

  where

-- haskell
import Data.List as L

-- aux
import Perhaps
import ListAux as LA
import Syntax
import Parser 
import ParseLang

-- core
import Language
import Proof

-- ==================================
-- Definitions: 
  -- data Goal PlProof Command 
  -- type GlStack ThmE ThmI
  
-- Functions 
 -- goal_formula goal_label indexGoalSymbol
 -- newGoalSymbols newGoalSymbol newGoalSymbolsFromGoalSymbol
 -- startGoalSymbol currentGoal currentGoalCtx start mkgoal
 -- A set of functions: refine*BW 
    -- refineAssumptBW, refineUseWithBW, refineUseThmBW, refineUseThmWithBW,
    -- refineAndIntrBW, refineAndElilBW, refineAndElirBW, refineAndLeftBW
    -- refineImpIntrBW, refineImpElimBW, 
    -- refineOrIntrlBW, refineOrIntrrBW, refineOrElimBW, refineOrElimBWAssu, refineOrLeftBW
    -- refineExFQBW, refineReAABW, 
    -- refineAllIntrBW, refineAllElimBW, refineExIntrBW, refineExElimBW,
    -- refineCutBW, refineLetPBW, refineDeclPBW, 
    -- refineGIndBW, refineIndBW, refineHSIndBW, refineSIndBW
    -- refineGCoiBW, refineCoiBW, refineHSCoiBW, refineSCoiBW
    -- refineCloBW, refineCoclBW,
    -- refineEqReflBW, refineEqSymBW, refineEqCongBW, refineEqCongruBW,
    -- refineNormPBW, refineUnfoldBW
 -- useProof useit
 -- matchF matchFxys matchFs matchP matchTs matchT matchV
 -- replaces 
 -- andElini exElims
 -- thmE2thmI 
 -- parseProof parsePR parseThmE 


-- Contents
-- ========
  -- Definitions
  -- Basic functions 
  -- Step change
  -- Refinement
  -- Generating elimination proofs
  -- Matching
  -- Replacing proofs
  -- Iterated application
  -- Theorems
   -- Definitions
   -- Parsing theorems 
  
-- Definitions
-- ======================

data Goal = Goal { 
                   ggoal      :: Assumpt,
                   gassumpts  :: [Assumpt],
                   gctx       :: Context,
                   gdecls     :: [Decl]
                  }
            deriving (Show, Read)

type GlStack = [Goal]

--using plp instead of pl because plproof already used in State definition
data PlProof = PlProof {
                        plpglstack  :: GlStack,
                        plpassumpts :: [Assumpt],
                        plpthms     :: [ThmI],
                        plpdecls    :: [Decl],
                        plpproof    :: Proof
                       } 
            deriving Show
  
data Command = AssumptBW AnVar -- use rule/placeholder
               | UseWithBW GlSymbol Label
               | UseThmBW GlSymbol Label
               | UseThmWithBW GlSymbol Label
               | AndIntrBW GlSymbol GlSymbol
               | AndElilBW GlSymbol Formula
               | AndElirBW GlSymbol Formula
               | AndLeftBW GlSymbol Label Label Label
               | ImpIntrBW GlSymbol AnVar
               | ImpElimBW GlSymbol GlSymbol Formula
               | ImpLeftBW GlSymbol GlSymbol Label Label
               | OrIntrlBW GlSymbol
               | OrIntrrBW GlSymbol
               | OrElimBW GlSymbol GlSymbol GlSymbol Formula
               | OrElimBWAssu GlSymbol GlSymbol Label
               | OrLeftBW GlSymbol Label GlSymbol Label Label
               | ExFQBW GlSymbol
               | ReAABW GlSymbol
               | AllIntrBW GlSymbol 
               -- AllIntrBW ? from goal All x.P(x) obtain? P(x)
               | AllElimBW GlSymbol Formula Term 
               -- AllElimBW ? x P(x) t from goal P(t) obtain ? All x.P(x) 
               | ExIntrBW GlSymbol Term 
               -- ExIntrBW ? t from goal Ex x.P(x) obtain ? P(t)
               | ExElimBW GlSymbol GlSymbol Formula 
               -- ExElimBW ?1 ?2 x P(x)  from goal C obtain goals 
               -- ?1 Ex x.P(x) ?2 All x.(P(x) -> C)
               | CutBW GlSymbol GlSymbol Assumpt
               | LetPBW GlSymbol
               | DeclPBW GlSymbol Decl
               | IndBW GlSymbol
               | HSIndBW GlSymbol
               | SIndBW GlSymbol
               | CoiBW GlSymbol  
               | HSCoiBW GlSymbol  
               | SCoiBW GlSymbol     
               | CloBW 
               | CoclBW 
               | EqReflBW
               | EqSymBW GlSymbol
               | EqCongBW GlSymbol Formula
               | EqCongfBW GlSymbol Formula
               | EqCongruBW GlSymbol Predicate [(Term,Term)]
               | NormPBW Formula -- new goal formula
               | UnfoldBW String -- predicate or operator variable
               | Invalid 
             deriving (Show, Eq, Read)

-- Basic functions
-- ==============================
goal_formula :: Goal -> Formula
goal_formula g = assumpt_formula (ggoal g)

goal_label :: Goal -> Label
goal_label g = assumpt_label (ggoal g)

indexGoalSymbol :: GlSymbol -> Maybe Int
indexGoalSymbol g =
  case g of
    { 
      ('?':x) -> case readsPrec 1 x of 
                  { ((n,_):_) -> Just n ; _ -> Nothing } ;
      _     -> Nothing
    }

newGoalSymbols :: Int -> Int -> [GlSymbol]
newGoalSymbols n k = ["?" ++ show (n+i) | i <- [1..k]] 

newGoalSymbol :: Int -> GlSymbol
newGoalSymbol n = "?" ++ show (n+1)

newGoalSymbolsFromGoalSymbol :: GlSymbol -> Int -> [GlSymbol]
newGoalSymbolsFromGoalSymbol g k =
  case indexGoalSymbol g of
    {
      Just n -> newGoalSymbols n k ;
      _      -> fst (newStrs k (fwd g))
    }   

startGoalSymbol :: String
startGoalSymbol = head (newGoalSymbols (-1) 1)

currentGoal :: PlProof -> Perhaps Goal
currentGoal pp = let gs = plpglstack pp
                 in if null gs
                    then Failure "No Goal"
                    else Success (head gs)
                    
currentGoalCtx :: PlProof -> Perhaps Context
currentGoalCtx pp = 
     case (currentGoal pp) of 
       {
        Success g -> Success (gctx g);
        _ -> fail "No goal"
       }

start :: [Assumpt] -> [Decl] -> [ThmI] -> Label -> Formula -> PlProof
start assu decls thms gl f = 
            PlProof { 
                     plpglstack  = [(mkgoal gl f)],
                     plpassumpts = assu,
                     plpthms     = thms,
                     plpdecls    = decls,
                     plpproof    = Rep f (AnProof gl)
                    }
                    
-- creates a goal with empty assumpts, ctx and decls           
mkgoal :: Label -> Formula -> Goal
mkgoal gl f = Goal { ggoal  = (gl,f),
                     gassumpts = [],
                     gctx      = emptycontext,
                     gdecls    = []
                   }

-- Refinement
-- ==================================
--downtoup
refine :: Language -> Context -> PlProof -> Command -> Perhaps PlProof
refine l ctx pp cmd = 
  case cmd of
   {
    AssumptBW av -> refineAssumptBW l ctx pp av; 
    UseWithBW gl av -> refineUseWithBW l ctx pp gl av ;  
    UseThmBW gl av -> refineUseThmBW l ctx pp gl av ;     
    UseThmWithBW gl av -> refineUseThmWithBW l ctx pp gl av ;     
    AndIntrBW gl1 gl2 -> refineAndIntrBW l ctx pp gl1 gl2;
    AndElilBW gl f -> refineAndElilBW l ctx pp gl f;
    AndElirBW gl f -> refineAndElirBW l ctx  pp gl f;
    AndLeftBW gl u v w -> refineAndLeftBW l ctx pp gl u v w;
    ImpIntrBW gl av -> refineImpIntrBW l ctx pp gl av;
    ImpElimBW gl1 gl2 f -> refineImpElimBW l ctx pp gl1 gl2 f;
    ImpLeftBW gl1 gl2 u v -> refineImpLeftBW l ctx pp gl1 gl2 u v;
    OrIntrlBW gl -> refineOrIntrlBW l ctx pp gl;
    OrIntrrBW gl -> refineOrIntrrBW l ctx pp gl;
    OrElimBW gl1 gl2 gl3 f -> refineOrElimBW l ctx pp gl1 gl2 gl3 f;
    OrElimBWAssu gl2 gl3 av -> refineOrElimBWAssu l ctx pp gl2 gl3 av;
    OrLeftBW gl1 u gl2 v w -> refineOrLeftBW l ctx pp gl1 u gl2 v w;
    ExFQBW gl -> refineExFQBW l ctx pp gl;
    ReAABW gl -> refineReAABW l ctx pp gl;                                
    AllIntrBW gl -> refineAllIntrBW l ctx pp gl;
    AllElimBW gl f t -> refineAllElimBW l ctx pp gl f t;                                          
    ExIntrBW gl t -> refineExIntrBW l ctx pp gl t;
    ExElimBW gl1 gl2 f -> refineExElimBW l ctx pp gl1 gl2 f;
    CutBW gl1 gl2 avf -> refineCutBW l ctx pp gl1 gl2 avf;
    LetPBW gl -> refineLetPBW l ctx pp gl; 
    DeclPBW gl d-> refineDeclPBW l ctx pp gl d;
    IndBW gl -> refineIndBW l ctx pp gl;
    HSIndBW gl -> refineHSIndBW l ctx pp gl;
    SIndBW gl -> refineSIndBW l ctx pp gl;
    CoiBW gl -> refineCoiBW l ctx pp gl;
    HSCoiBW gl -> refineHSCoiBW l ctx pp gl;
    SCoiBW gl -> refineSCoiBW l ctx pp gl;      
    CloBW -> refineCloBW l ctx pp;
    CoclBW -> refineCoclBW l ctx pp;
    EqReflBW -> refineEqReflBW l ctx pp;
    EqSymBW gl -> refineEqSymBW l ctx pp gl;
    EqCongBW gl f -> refineEqCongBW equFor l ctx pp gl f;
    EqCongfBW gl f -> refineEqCongBW equFor0 l ctx pp gl f;
    EqCongruBW gl pred eqs 
       -> refineEqCongruBW l ctx pp gl pred eqs;
    NormPBW f -> refineNormPBW l ctx pp f;
    UnfoldBW pov -> refineUnfoldBW l ctx pp pov;
    Invalid -> fail ("invalid command: " ++ show cmd)
   }                   
              
-- refine sub-functions
refineAssumptBW :: Language -> Context -> PlProof -> AnVar -> Perhaps PlProof
refineAssumptBW l ctx pp av = 
  case currentGoal pp of
   { Success g -> 
      do {
          f1 <- look av ((gassumpts g) ++ (plpassumpts pp));
          if equalDeclFormula l ctx ((gdecls g) ++ (plpdecls pp)) f1 (goal_formula g) 
          then let 
               {
                p1 = replace (plpproof pp) (goal_label g) (AnProof av);
                gs = tail (plpglstack pp)
               }
               in return (pp {plpglstack = gs, plpproof = p1}) 
          else fail ("assumption doesn't fit: " ++ show av)
          };
      _ -> fail ("Not possible to refine, no goal.")
    }

refineUseWithBW :: Language -> Context -> PlProof -> GlSymbol -> AnVar -> 
                                                                Perhaps PlProof
refineUseWithBW l ctx pp gl av =
  case currentGoal pp of   
   { 
     Success g -> 
      do {
          f1 <- look av (gassumpts g ++ plpassumpts pp) ;
          let { Just n = indexGoalSymbol gl } ;  -- !!!
          case useProof n (av,f1) (goal_formula g) of
            {
              Just (d,ans) -> 
                let 
                    {
                      p1 = replace (plpproof pp) (goal_label g) d;
                      gs = tail (plpglstack pp) ;
                      gs' = [ g { ggoal = an } | an <- ans ]
                    }
                in return (pp {plpglstack = gs' ++ gs, plpproof = p1}) ; 
              _ -> fail ("PlProof usw refineUseWithBW useProof")
            } 
         } ;
     _ -> fail ("Not possible to refine, no goal.")
   }


refineUseThmBW :: Language -> Context -> PlProof -> GlSymbol -> AnVar -> Perhaps PlProof
refineUseThmBW l ctx pp gl av =
  case currentGoal pp of   
   { 
     Success g -> 
      do {
          (f,pt) <- look av (plpthms pp) ; 
          -- find matching f and proof term
          if equalDeclFormula l ctx (plpdecls pp) (goal_formula g) f 
          then let {
                      p1 = replace (plpproof pp) (goal_label g) pt;
                      gs = tail (plpglstack pp) ;
                    }
                in return (pp {plpglstack = gs, plpproof = p1});
          else fail "ust not applicable"
         } ;
     _ -> fail ("Not possible to refine, no goal.")
   }

   
refineUseThmWithBW :: Language -> Context -> PlProof -> GlSymbol -> AnVar -> Perhaps PlProof
refineUseThmWithBW l ctx pp gl av =
  case currentGoal pp of   
   { 
     Success g -> 
      do {
          (f,pt) <- look av (plpthms pp) ; 
          -- find matching f and proof term
          let { Just n = indexGoalSymbol gl } ;  
          case useProof n (av,f) (goal_formula g) of
            {
              Just (d,ans) -> 
                let 
                    {
                      d1 = replace d av pt ;
                      p1 = replace (plpproof pp) (goal_label g) d1 ;
                      gs = tail (plpglstack pp) ;
                      gs' = [ g { ggoal = an } | an <- ans ]
                    }
                in return (pp {plpglstack = gs' ++ gs, plpproof = p1}) ; 
              _ -> fail "ustw not applicable\n"
            } 
         } ;
     _ -> fail ("Not possible to refine, no goal.")
   }
     
refineAndIntrBW :: Language -> Context -> PlProof -> GlSymbol -> GlSymbol -> Perhaps PlProof
refineAndIntrBW l ctx pp gl1 gl2 = 
  case (currentGoal pp) of
  { 
    Success g ->  
      case (goal_formula g) of
       {
        And f1 f2 ->
          let {
               Success g0 = currentGoal pp; 
               g1 = g0 {ggoal = (gl1,f1)}; 
               g2 = g0 {ggoal = (gl2,f2)}; 
               p1 = replace (plpproof pp) (goal_label g)
                     (AndIntr (AnProof gl1) (AnProof gl2));
               gs = tail (plpglstack pp)
              }
          in return pp {plpglstack = g1:g2:gs, plpproof = p1}; 
        _ -> fail ("AndIntrBW, wrong command: " ++ show (ggoal g))
       };
      _ -> fail ("Not possible to refine, no goal.")
    }
    
refineAndElilBW :: Language -> Context -> PlProof -> GlSymbol -> Formula -> Perhaps PlProof
refineAndElilBW l ctx pp gl f =
  case (currentGoal pp) of 
   {
     Success g -> 
       let {
            f1 = And (goal_formula g) f;
            g1 = g {ggoal = (gl,f1)};
            p1 = replace (plpproof pp) (goal_label g) (AndElil (AnProof gl));
            gs = safetail (plpglstack pp)
           }
       in return (pp {plpglstack = g1:gs, plpproof = p1}); 
      _ -> fail ("Not possible to refine, no goal.")
    }
    
refineAndElirBW :: Language -> Context -> PlProof -> GlSymbol -> Formula -> Perhaps PlProof
refineAndElirBW l ctx pp gl f = 
  case (currentGoal pp) of
   {
     Success g ->
      let {
           f1 = And f (goal_formula g);
           g1 = g {ggoal = (gl,f1)};
           p1 = replace (plpproof pp) (goal_label g) (AndElir (AnProof gl));
           gs = safetail (plpglstack pp)
          }
      in return (pp {plpglstack = g1:gs, plpproof = p1});
     _ -> fail ("Not possible to refine, no goal.")
    }

refineAndLeftBW :: Language -> Context -> PlProof -> GlSymbol -> Label -> Label -> Label  -> Perhaps PlProof
refineAndLeftBW l ctx pp gl u v w  =
  case (plpglstack pp) of {
        [] -> Failure "refineAndLeftBW: Goalstack empty";
      (g:gs1) -> 
         do{
             let { c0 = gassumpts g;
                   gamma = plpassumpts pp;
                   c =  c0 ++ gamma };
             f <- look w c;
             case f of
                 { And f1 f2 ->
                       let {
                             h = goal_formula g;
                             p1 = replace (plpproof pp)(goal_label g) (AndLeft (AnProof gl) u v w);
                             c1 = (u,f1):(v,f2):c0;
                             g' = g {ggoal=(gl,h), gassumpts= c1}
                           }
                   in return (pp{plpglstack= g': gs1, plpproof= p1});
                   _ -> fail ("refineAndLeftBW: w does not exist or not a conjunction. ")
                  }
            }
       }
  
refineImpIntrBW :: Language -> Context -> PlProof -> GlSymbol -> AnVar -> Perhaps PlProof
refineImpIntrBW l ctx pp gl av = 
    case (currentGoal pp) of
     {
      Success g -> 
          case (goal_formula g) of
           {
            Imp f1 f2 ->
              let {
                   allassulabels = map fst (gassumpts g `LA.union` plpassumpts pp);
                   thmnames = map fst (plpthms pp);
                   av' = clashfix av (allassulabels `LA.union` thmnames); -- clashfix in ListAux.hs
                   c = (av',f1):(gassumpts g);
                   g1 = g {ggoal = (gl,f2), gassumpts = c};
                   p1 = replace   
                          (plpproof pp) 
                          (goal_label g) 
                          (ImpIntr (AnProof gl) (av', f1));
                   gs = safetail (plpglstack pp)
                  }
              in return (pp {plpglstack = g1:gs, plpproof = p1}); 
             _ -> fail ("ImpIntrBW, wrong command: " ++ show (ggoal g))
           };
     _ -> fail ("Not possible to refine, no goal.")
    }

refineImpElimBW :: Language -> Context -> PlProof -> GlSymbol -> GlSymbol -> Formula -> Perhaps PlProof
refineImpElimBW l ctx pp gl1 gl2 f = 
  case (currentGoal pp) of
    {
      Success g -> 
          let {
               f1 = Imp f (goal_formula g);
               g1 = g {ggoal = (gl1,f1)};
               g2 = g {ggoal = (gl2,f)};
               p1 = replace (plpproof pp) (goal_label g) (ImpElim (AnProof gl1) (AnProof gl2));
               gs = safetail (plpglstack pp)
              }
          in return (pp {plpglstack = g1:g2:gs, plpproof = p1});
       _ -> fail ("Not possible to refine, no goal.")
    }

refineImpLeftBW :: Language -> Context -> PlProof -> GlSymbol -> GlSymbol -> Label -> Label  -> Perhaps PlProof
refineImpLeftBW l ctx pp gl1 gl2 u w =
  case (plpglstack pp) of
    {
      [] -> Failure "refineImpLeftBW: Goalstack Empty";
      (g: gs1) ->
         do{
             let {c0 = gassumpts g;
                   gamma = plpassumpts pp;
                   c =  c0 ++ gamma};
             f <- look w c;
             case f of
               {
                 Imp f1 f2 ->
                     let {
                           h = goal_formula g;
                           c1 = c0;
                           c2 = (u, f2):c0;
                           g1 = g {ggoal=(gl1,f1), gassumpts= c1};
                           g2 = g {ggoal=(gl2,h), gassumpts= c2};
                           p1 = replace (plpproof pp)(goal_label g) (ImpLeft (AnProof gl1) (AnProof gl2) u w)
                           
                           }
                  in return (pp{plpglstack= g1: g2: gs1, plpproof= p1});
               _ -> fail "refineImpLeftBW: w does not exist or is not an implication."
                }
           }
    }
  
refineOrIntrlBW :: Language -> Context -> PlProof -> GlSymbol -> Perhaps PlProof
refineOrIntrlBW l ctx pp gl = 
  case (currentGoal pp) of
    {
      Success g -> 
         case (goal_formula g) of
           {
            Or f1 f2 ->
              let {
                   g1 = g {ggoal = (gl,f1)};
                   p1 = replace (plpproof pp) (goal_label g) (OrIntrl (AnProof gl) f2);
                   gs = safetail (plpglstack pp)
                  }
              in return (pp {plpglstack = g1:gs, plpproof = p1});
            _ -> fail ("OrIntrlBW, wrong command: " ++ show (ggoal g))
           };
      _ -> fail ("Not possible to refine, no goal.")
    }
   
refineOrIntrrBW :: Language -> Context -> PlProof -> GlSymbol -> Perhaps PlProof
refineOrIntrrBW l ctx pp gl =
    case (currentGoal pp) of
      {
        Success g ->       
          case (goal_formula g) of
           {
            Or f1 f2 ->
              let {
                   g1 = g {ggoal =(gl,f2)};
                   p1 = replace (plpproof pp) (goal_label g) (OrIntrr (AnProof gl) f1);
                   gs = safetail (plpglstack pp)
                  }
              in return (pp { plpglstack = g1:gs, plpproof = p1});
            _ -> fail ("OrIntrrBW, wrong command: " ++ show (ggoal g))
           };
        _ -> fail ("Not possible to refine, no goal.")
      }
   
refineOrElimBW :: Language -> Context -> PlProof -> GlSymbol -> GlSymbol -> GlSymbol -> Formula -> Perhaps PlProof
refineOrElimBW l ctx pp gl1 gl2 gl3 f = 
    case (currentGoal pp) of
      {
        Success g -> 
          case f of
           {
            Or f1 f2 ->
              let {
                   f3 = Imp f1 (goal_formula g);
                   f4 = Imp f2 (goal_formula g);
                   g1 = g {ggoal = (gl1,f)};
                   g2 = g {ggoal = (gl2,f3)};
                   g3 = g {ggoal = (gl3,f4)};
                   p1 = replace (plpproof pp) (goal_label g) (OrElim (AnProof gl1) (AnProof gl2) (AnProof gl3));
                   gs = safetail (plpglstack pp)
                  }
              in return (pp { plpglstack = g1:g2:g3:gs, plpproof = p1});
            _ -> fail ("OrElimBW, wrong command: " ++ show (ggoal g))
           };
        _ -> fail ("Not possible to refine, no goal.")
    }
   
refineOrElimBWAssu :: Language -> Context -> PlProof -> GlSymbol -> GlSymbol -> Label -> Perhaps PlProof
refineOrElimBWAssu l ctx pp gl2 gl3 av = 
    case (currentGoal pp) of
      {
        Success g -> 
          do {
               f <- look av ((gassumpts g) ++ (plpassumpts pp)) ;
               case f of
                 {
                   Or f1 f2 ->
                     let {
                           f3 = Imp f1 (goal_formula g);
                           f4 = Imp f2 (goal_formula g);
                           g2 = g {ggoal = (gl2,f3)};
                           g3 = g {ggoal = (gl3,f4)};
                           p1 = replace (plpproof pp) 
                                        (goal_label g) 
                                        (OrElim (AnProof av) 
                                                (AnProof gl2) 
                                                (AnProof gl3));
                           gs = safetail (plpglstack pp)
                         }
                     in return (pp { plpglstack = g2:g3:gs, 
                                     plpproof = p1});
                   _ -> fail "OrElimBWAssu1"
                 }
             } ;
        _ -> fail "OrElimBWAssu2"
      }

refineOrLeftBW :: Language -> Context -> PlProof -> GlSymbol -> Label -> GlSymbol -> Label -> Label  -> Perhaps PlProof
refineOrLeftBW l ctx pp gl1 u gl2 v w =
  case (plpglstack pp) of
    {
      [] -> Failure "refineOrLeftBW: Goalstack Empty";
      (g: gs1) ->  
         do{
             let { c0 = gassumpts g;
                   gamma = plpassumpts pp;
                   c =  c0 ++ gamma };
             f <- look w c;
             case f of
               {
                 Or f1 f2 ->
                    let {
                          h = goal_formula g;
                          c1 = (u, f1):c0;
                          c2 = (v, f2):c0;
                          g1 = g {ggoal=(gl1,h), gassumpts= c1};
                          g2 = g {ggoal=(gl2,h), gassumpts= c2};
                          p1 = replace (plpproof pp)(goal_label g) (OrLeft (AnProof gl1) u (AnProof gl2) v w)
                          
                        }
                    in return (pp{plpglstack= g1: g2: gs1, plpproof= p1});
                 _ -> fail "refineOrLeftBW: w does not exist ot is not a disjunction."
               }
           }
    }

refineExFQBW :: Language -> Context -> PlProof -> GlSymbol -> Perhaps PlProof
refineExFQBW l ctx pp gl = 
    case (currentGoal pp) of
      {
        Success g ->
          let {
               f1 = Bot;
               g1 = g {ggoal = (gl,f1)};
               p1 = replace (plpproof pp) (goal_label g) (ExFQ (AnProof gl) (goal_formula g));
               gs = safetail (plpglstack pp)
              }
          in return (pp { plpglstack = g1:gs, plpproof = p1});
        _ -> fail ("Not possible to refine, no goal.")
    }
  
refineReAABW :: Language -> Context -> PlProof -> GlSymbol -> Perhaps PlProof
refineReAABW l ctx pp gl = 
    case (currentGoal pp) of
      {
        Success g -> 
          let {
               f1 = Imp (Imp (goal_formula g) Bot) Bot;
               g1 = g {ggoal = (gl,f1)};
               p1 = replace (plpproof pp) (goal_label g) (ReAA (AnProof gl));
               gs = safetail (plpglstack pp)
              }
          in return (pp {plpglstack = g1:gs, plpproof = p1});
        _ -> fail ("Not possible to refine, no goal.")
    }
  
  -- get *v's into the c0 - CHECK
refineAllIntrBW :: Language -> Context -> PlProof -> GlSymbol -> Perhaps PlProof
refineAllIntrBW l ctx pp gl = 
    case (currentGoal pp) of
      {
        Success g -> 
          case (goal_formula g) of
           {
            All (v,s) f ->
              let {
                   axi = plpassumpts pp;
                   gassu = gassumpts g;
                   ds = plpdecls pp;
                   gds = gdecls g;                   
                   v' = freshVar v (LA.unions [(fvInDecls ds),(fvInDecls gds),(fvInAssumpts axi),(fvInAssumpts gassu),(fv (All (v,s) f))]);
                   f' = substituteFormula v (Var v') f;
                   c1 = addCVars (gctx g) [(v',s)]; 
                   g1 = g {ggoal = (gl,f'), gctx = c1};
                   p1 = replace (plpproof pp) (goal_label g) (AllIntr (AnProof gl) (v',s)); 
                   gs = safetail (plpglstack pp)
                  }
                in return (pp {plpglstack = g1:gs, plpproof = p1});
            _ -> fail ("AllIntrBW, wrong command: " ++ show (ggoal g))
           };
        _ -> fail ("Not possible to refine, no goal.")
    }
   
refineAllElimBW :: Language -> Context -> PlProof -> GlSymbol -> Formula -> Term -> Perhaps PlProof
refineAllElimBW l ctx pp gl f t = 
    case (currentGoal pp) of
      {
        Success g -> 
          case f of
           {
            All (v,s) f0 ->
              let f1 = substituteFormula v t f0 in
              if (equalDeclFormula l ctx ((gdecls g) ++ (plpdecls pp)) f1 (goal_formula g)) then 
                let {
                     c1 = addCVars (gctx g) [(v,s)]; 
                     g1 = g {ggoal = (gl,f), gctx = c1}; 
                     p1 = replace (plpproof pp) (goal_label g) (AllElim (AnProof gl) t);
                     gs = safetail (plpglstack pp)
                    }
                in return (pp {plpglstack = g1:gs, plpproof = p1})
              else fail ("AllElimBW, formulas don't fit: " ++ show (ggoal g) ++ " " ++ show f1);
            _ -> fail ("AllElimBW, universal formula expected")
           };
        _ -> fail ("Not possible to refine, no goal.")
    }
refineExIntrBW :: Language -> Context -> PlProof -> GlSymbol -> Term -> Perhaps PlProof
refineExIntrBW l ctx pp gl t = 
    case (currentGoal pp) of
      {
        Success g -> 
          case (goal_formula g) of
           {
            Ex (v,s) f ->
              let {
                   f1 = substituteFormula v t f;
                   g1 = g {ggoal = (gl,f1)};
                   p1 = replace (plpproof pp) (goal_label g) (ExIntr (AnProof gl) (v,s) f t);
                   gs = safetail (plpglstack pp)
                  }
              in return (pp { plpglstack = g1:gs, plpproof = p1});
            _ -> fail ("ExIntrBW, command not applicable to: " ++ show(goal_formula g))
           };
        _ -> fail ("Not possible to refine, no goal.")
      }

refineExElimBW :: Language -> Context -> PlProof -> GlSymbol -> GlSymbol -> Formula -> Perhaps PlProof
refineExElimBW l ctx pp gl1 gl2 f = 
    case (currentGoal pp) of
      {
        Success g ->
          case f of
           {
            Ex (v,s) f0 ->
              let {
                    gf = goal_formula g ;
                    v' = freshVar v (LA.unions [fvInAssumpts (gassumpts g), fv f, fv gf]) ;
                    f0' = substituteFormula v (Var v') f0 ;
                    g1 = g {ggoal = (gl1,Ex (v',s) f0')} ;
                    g2 = g {ggoal = (gl2,All (v',s) (Imp f0' gf))} ;
                    p1 = replace (plpproof pp) (goal_label g) (ExElim (AnProof gl1) (AnProof gl2)) ;
                    gs = tail (plpglstack pp)
                  }
              in return (pp { plpglstack = g1:g2:gs, plpproof = p1}) ;
            _ -> fail ("ExElimBW, existential formula expected")
           } ;
        _ -> fail ("Not possible to refine, no goal.")
      }

refineCutBW :: Language -> Context -> PlProof -> GlSymbol -> GlSymbol -> Assumpt -> Perhaps PlProof
refineCutBW l ctx pp gl1 gl2 avf@(u,f) =
  case (plpglstack pp) of
    {
      [] -> Failure "refineImpLeftBW: Goalstack Empty";
       (g: gs1) ->
         do{
             let {c0 = gassumpts g;
                  gamma = plpassumpts pp;
                  c =  c0 ++ gamma;
                  h = goal_formula g;
                  c1 = c0;
                  c2 = avf : c0;
                  g1 = g {ggoal=(gl1,f), gassumpts= c1};
                  g2 = g {ggoal=(gl2,h), gassumpts= c2};
                  p1 = replace (plpproof pp)(goal_label g) (Cut (AnProof gl1) (AnProof gl2) avf)
                  
                 };
             return (pp{plpglstack= g1: g2: gs1, plpproof= p1})
            
           }
    }


refineLetPBW :: Language -> Context -> PlProof -> GlSymbol -> Perhaps PlProof
refineLetPBW l ctx pp gl = 
    case currentGoal pp of
      {
        Success g -> 
          case goal_formula g of
            {
             LetF decl f ->
               let {
                    p1 = replace (plpproof pp) (goal_label g) (LetP decl (AnProof gl));
                    d1 = decl:(gdecls g); 
                    ctx' = ctxFromDecls [decl]; 
                    -- extracts ctx from decls
                    goalctx = gctx g;
                    goalctx' = mergeCtxs goalctx ctx'; -- GCTX change
                    g1 = g {ggoal = (gl,f), gdecls = d1, gctx = goalctx'};
                    gs = safetail (plpglstack pp)
                    }
               in return (pp { plpglstack = g1:gs, plpproof = p1});
             _ -> fail ("LetPBW, let formula is expected!") 
            };
        _ -> fail ("Not possible to refine, no goal.")
      }
      
refineDeclPBW :: Language -> Context -> PlProof -> GlSymbol -> Decl -> Perhaps PlProof
refineDeclPBW l ctx pp gl decl = 
    case currentGoal pp of
      {
        Success g -> 
          let 
             {
              p1 = replace (plpproof pp) (goal_label g) (DeclP decl (AnProof gl));
              d1 = decl:(gdecls g);
              ctx' = ctxFromDecls [decl]; -- extracts ctx from decls
              goalctx = gctx g;
              goalctx' = mergeCtxs ctx' goalctx ;
              g1 = g {gdecls = d1, gctx = goalctx'};
              gs = safetail (plpglstack pp)
             }
          in return (pp { plpglstack = g1:gs, plpproof = p1});
        _ -> fail ("DeclPBW, refinement failed") 
      }
      
-- ordinary, half-strong, strong induction      
refineIndBW, refineHSIndBW, refineSIndBW :: 
    Language -> Context -> PlProof -> GlSymbol -> Perhaps PlProof
refineIndBW = refineGIndBW "normal"
refineHSIndBW = refineGIndBW "hs"
refineSIndBW = refineGIndBW "strong"

refineGIndBW :: String -> Language -> Context -> PlProof -> 
                     GlSymbol -> Perhaps PlProof
refineGIndBW option l ctx pp gl = 
    case currentGoal pp of
      {
        Success g ->
          let {(xss,f) = splitquant (goal_formula g);
                xs = map fst xss;
                goalctx = gctx g;
                ctx' = addCVars (mergeCtxs goalctx ctx) xss}
            in case f of
                {
                 Imp f1@(Predic pred@(PrVar pn) ts) f2 ->
                    if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                    then case (lookupPred ((gdecls g) ++ (plpdecls pp)) pred) of
                       {
                        Mu o -> 
                         let {q = smartCompr xss f2;
                              f' = case option of
                                    {"normal" -> 
                                        quant xss 
                                        (Imp (Predic (OpApp o q) ts) f2);
                                     "hs" ->
                                        quant xss 
                                        (Imp (And f1 (Predic (OpApp o q) ts)) f2);
                                     "strong" ->
                                        quant xss 
                                        (Imp (Predic (OpApp o (smartCompr xss (And f1 f2))) ts) f2)
                                     };
                              p' = case option of
                                    {"normal" -> Ind pred (AnProof gl);
                                     "hs" -> HSInd pred (AnProof gl);
                                     "strong" -> SInd pred (AnProof gl)
                                    };
                              p1 = replace (plpproof pp) 
                                      (goal_label g) p';
                                                     
                              g1 = g {ggoal = (gl,f')};
                              gs = safetail (plpglstack pp)
                              }
                         in return (pp {plpglstack = g1:gs, plpproof = p1});
                         _ -> fail ("1: not an inductive proof")
                       }
                    else fail ("2: not an inductive proof"); -- fail message not printed!!
                 _ -> fail ("3: not an inductive proof"); 
               };
        _ -> fail ("Not possible to refine, no goal.")
      }

-- ordinary, half-strong, strong coinduction 
refineCoiBW, refineHSCoiBW, refineSCoiBW :: 
   Language -> Context -> PlProof -> GlSymbol -> Perhaps PlProof
refineCoiBW = refineGCoiBW "normal"
refineHSCoiBW = refineGCoiBW "hs"
refineSCoiBW = refineGCoiBW "strong"

refineGCoiBW :: String -> Language -> Context -> PlProof -> 
                    GlSymbol -> Perhaps PlProof
refineGCoiBW option l ctx pp gl = 
    case (currentGoal pp) of
      {
        Success g ->
          let {(xss,f) = splitquant (goal_formula g);
                xs = map fst xss;
                goalctx = gctx g;
                ctx' = addCVars (mergeCtxs goalctx ctx) xss}
            in case f of
                {
                 Imp f1 f2@(Predic pred@(PrVar pn) ts) ->
                    if map Var xs == ts && length xs == length (arityP l ctx' pred)
                    then case (lookupPred ((gdecls g) ++ (plpdecls pp)) pred) of
                         {
                          Nu o -> 
                            let {q = smartCompr xss f1;
                                 f' = case option of
                                       {"normal" ->
                                          quant xss 
                                          (Imp f1 (Predic (OpApp o q) ts));
                                        "hs" ->
                                          quant xss 
                                          (Imp f1 (Or (Predic (OpApp o q) ts) f2));
                                        "strong" ->
                                          quant xss 
                                          (Imp f1 (Predic (OpApp o (smartCompr  xss (Or f1 f2) )) ts))
                                        };
                                 p' = case option of
                                    {"normal" -> Coi pred (AnProof gl);
                                     "hs" -> HSCoi pred (AnProof gl);
                                     "strong" -> SCoi pred (AnProof gl)
                                    };
                                 p1 = replace (plpproof pp) (goal_label g) p';
                                 g1 = g {ggoal = (gl,f')};
                                 gs = safetail (plpglstack pp)
                                }
                            in return (pp {plpglstack = g1:gs, plpproof = p1});
                          _ -> fail ("1: not a coinductive goal")
                         }
                    else fail ("2: not a coinductive goal:\n" 
                               ++ show xs ++"\n" ++ show ts ++ "\n" ++ show (arityP l ctx' pred)) ;
                 _ -> fail ("3: not a coinductive goal") 
                };
        _ -> fail ("Not possible to refine, no goal.")
      }
      
refineCloBW :: Language -> Context -> PlProof -> Perhaps PlProof
refineCloBW l ctx pp = 
    case currentGoal pp of
      { 
        Success g -> 
          let { 
                (xss,f) = splitquant (goal_formula g);
                xs = map fst xss;
                goalctx = gctx g;
                ctx' = addCVars (mergeCtxs goalctx ctx) xss;
                aux f1 o pred ts = 
                  let {
                        p1 = replace (plpproof pp) (goal_label g) (Clo pred);
                        f' = Predic (OpApp o pred) ts;
                        gs = tail (plpglstack pp)
                      }
                  in if equalDeclFormula l ctx' ((gdecls g) ++ (plpdecls pp)) f1 f'
                     then return (pp {plpglstack = gs, plpproof = p1}) 
                     else fail ("1: not a closure proof")
              }
          in case f of
               {
                 Imp f1 (Predic pred@(PrVar pn) ts) ->
                   case (lookupPred (gdecls g ++ plpdecls pp) pred) of
                     {
                       Mu o -> if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                               then aux f1 o pred ts
                               else fail ("2: not a closure proof") ;
                       Nu o -> if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                               then aux f1 o pred ts
                               else fail ("3: not a closure proof") ;
                       _    -> fail ("4: not a closure proof") 
                     } ;
                 Imp f1 (Predic pred@(Mu o) ts) ->
                   if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                   then aux f1 o pred ts
                   else fail ("5: not a closure proof") ; 
                 Imp f1 (Predic pred@(Nu o) ts) ->
                   if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                   then aux f1 o pred ts
                   else fail ("6: not a closure proof") ;
                 _    -> fail ("7: not a closure proof")
               } ;
        Failure s -> fail ("8: empty goal stack. " ++ s)
      }

refineCoclBW :: Language -> Context -> PlProof -> Perhaps PlProof
refineCoclBW l ctx pp = 
    case currentGoal pp of
      { 
        Success g -> 
          let { 
                (xss,f) = splitquant (goal_formula g);
                xs = map fst xss;
                goalctx = gctx g;
                ctx' = addCVars (mergeCtxs goalctx ctx) xss;
                aux f1 o pred ts = 
                  let {
                        p1 = replace (plpproof pp) (goal_label g) (Cocl pred);
                        f' = Predic (OpApp o pred) ts;
                        gs = tail (plpglstack pp)
                      }
                  in if equalDeclFormula l ctx' ((gdecls g) ++ (plpdecls pp)) f1 f'
                     then return (pp {plpglstack = gs, plpproof = p1}) 
                     else fail ("1: not a coclosure proof")
              }
          in case f of
               {
                 Imp (Predic pred@(PrVar pn) ts) f1 ->
                   case (lookupPred (gdecls g ++ plpdecls pp) pred) of
                     {
                       Mu o -> if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                               then aux f1 o pred ts
                               else fail ("2: not a coclosure proof") ;
                       Nu o -> if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                               then aux f1 o pred ts
                               else fail ("3: not a coclosure proof") ;
                       _    -> fail ("4: not a coclosure proof") 
                     } ;
                 Imp (Predic pred@(Mu o) ts) f1 ->
                   if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                   then aux f1 o pred ts
                   else fail ("5: not a coclosure proof") ; 
                 Imp (Predic pred@(Nu o) ts) f1 ->
                   if map Var xs == ts && length xs == length (arityP l ctx' pred) 
                   then aux f1 o pred ts
                   else fail ("6: not a coclosure proof") ;
                 _    -> fail ("7: not a coclosure proof")
               } ;
        Failure s -> fail ("8: empty goal stack. " ++ s)
      }


                 
refineEqReflBW :: Language -> Context -> PlProof -> Perhaps PlProof
refineEqReflBW l ctx pp = 
  do {
       g <- currentGoal pp ;
       (t1,t2) <- equation (goal_formula g) ; -- no sort check needed
       if t1 == t2
       then let {
                  p1 = replace (plpproof pp) (goal_label g) (EqRefl t1);
                  gs = tail (plpglstack pp)
                }
             in return (pp { plpglstack = gs, plpproof = p1})
       else fail ("EqReflBW: terms are not equal" 
                                  ++ show t1 ++ "," ++ show t2)
     }           

refineEqSymBW :: Language -> Context -> PlProof -> 
                                GlSymbol -> Perhaps PlProof
refineEqSymBW l ctx pp gl =  
 do {
       g <- currentGoal pp ;
       (t1,t2) <- equation (goal_formula g) ;
       let {
            p1 = replace (plpproof pp) (goal_label g) (EqSym (AnProof gl));
            gs = tail (plpglstack pp) ;
            g1 = g { ggoal = (gl,mkEquation t2 t1) }
           } ;
       return(pp { plpglstack = g1:gs, plpproof = p1 })
     }

refineEqCongBW :: 
  (Language -> Context -> (Formula,Formula) -> 
     Perhaps (Formula,[((Term,Term),(String,Sort))])) -> 
  Language -> Context -> PlProof -> 
    GlSymbol -> Formula -> Perhaps PlProof
refineEqCongBW ef l ctx pp gl f = 
  do {
       g <- currentGoal pp ;
       goalctx <- return $ gctx g;
       decls <- Success ((plpdecls pp) `LA.union` (gdecls g));
       ctx' <- return $ mergeCtxs goalctx ctx;
       
       (h,rtus) <- ef l ctx' (normDeclF decls f, 
                              normDeclF decls (goal_formula g)) ; 
                        -- ef = equFor or ef = equFor0 in Language
       let { 
             hs = [mkEquation r t | (r,t) <- map fst rtus] ;
             us = map snd rtus ;
             gls = newGoalSymbolsFromGoalSymbol gl (length us) ;
             p0 = AnProof gl ;
             ps = map AnProof gls ;
             pred = Compr us h ;
             p1 = replace (plpproof pp) (goal_label g) (EqCong p0 ps pred) ;
             gs = tail (plpglstack pp) ;
             g1 = g { ggoal = (gl,f) } ;
             gs1 = [ g { ggoal = (glsi,hi) } | (glsi,hi) <- zip gls hs ]
           } ;
       return (pp { plpglstack = g1:gs1++gs, plpproof = p1})
     }     

refineEqCongruBW :: Language -> Context -> PlProof -> 
                    GlSymbol -> Predicate -> [(Term,Term)] -> 
                                                Perhaps PlProof
refineEqCongruBW l ctx pp gl pred eqs =
  do {
       g <- currentGoal pp ;
       goalctx <- return $ gctx g;
       decls <- Success ((plpdecls pp) `LA.union` (gdecls g));
       ctx' <- return $ mergeCtxs goalctx ctx;
       let { gf = goal_formula g; f2 = Predic pred (map snd eqs) } ;
       if equalDeclFormula l ctx' decls gf f2
       then let { 
                  f  = normF (Predic pred (map fst eqs)) ;
                  hs = [mkEquation r t | (r,t) <- eqs] ;
                  gls = newGoalSymbolsFromGoalSymbol gl (length eqs) ;
                  p0 = AnProof gl ;
                  ps = map AnProof gls ;
                  p1 = replace (plpproof pp) (goal_label g) (EqCong p0 ps pred) ;
                  gs = tail (plpglstack pp) ;
                  g1 = g { ggoal = (gl,f) } ;
                  gs1 = [ g { ggoal = (glsi,hi) } | (glsi,hi) <- zip gls hs ]
                }
            in return (pp { plpglstack = g1:gs1++gs, plpproof = p1})
       else fail ("refineEqCongruBW: " ++ show [gf,f2]) 
     }     

refineNormPBW :: Language -> Context -> PlProof -> 
                            Formula -> Perhaps PlProof
refineNormPBW l ctx pp f =                              
  do {
       g <- currentGoal pp ;
       decls <- Success ((plpdecls pp) `LA.union` (gdecls g));
       (v,cgf) <- Success (ggoal g); 
       goalctx <- return $ gctx g;
       ctx' <- return $ mergeCtxs goalctx ctx;
       if equalDeclFormula l ctx' decls cgf f 
       then let {
                 gs = safetail (plpglstack pp) ;
                 g1 = g {ggoal = (v, f)};
                 p1 = replace (plpproof pp) (goal_label g) (AnProof v);
                 } 
             in return (pp { plpglstack = g1:gs, plpproof = p1})
       else fail "refineNormPBW: Formulas don't match"
      }
      
refineUnfoldBW :: Language -> Context -> PlProof -> 
                     String -> Perhaps PlProof
refineUnfoldBW l ctx pp pov =                              
   do {
       g <- currentGoal pp ;
       decls <- Success ((plpdecls pp) `LA.union` (gdecls g));
       (v,cgf) <- Success (ggoal g); 
       goalctx <- return $ gctx g;
       ctx' <- return $ mergeCtxs goalctx ctx;
       if checkdecl pov decls
       then let { f = unfold pov decls cgf } 
            in if equalDeclFormula l ctx' decls cgf f 
               then let {
                         gs = safetail (plpglstack pp) ;
                         g1 = g {ggoal = (v, f)};
                         p1 = replace (plpproof pp) (goal_label g) (AnProof v);
                         } 
                    in return (pp { plpglstack = g1:gs, plpproof = p1})
               else fail ("PlProof.hs refineUnfoldPBW: fail, formulas are not equal\n" ++ show cgf ++ " \n" ++ show f);
       else fail "refineUnfoldPBW: fail, variable is not declared"
      }


-- Generating elimination proofs
-- ======================================

-- If useProof n an g = Just (d,ans), then an, ans |- d : g
-- using only and-, imp-, all-elimination 
-- (and ex-elims if there are existential variables) 
-- with 'main' assumption u:f and 'side' assumptions ans.
-- n is used to generate fresh goal symbols for ans.

{-
*Prover> let f = All ("x","R") (Imp (Predic (PrVar "N") [Fun "-" [Var "x",Const "1"]]) (Predic (PrVar "N") [Var "x"]))
*Prover> let g = Predic (PrVar "N") [Const "1"]
*Prover> useProof 3 ("u",f) g
Just (ImpElim (AllElim (AnProof "u") (Const "1")) (AnProof "?4"),[("?4",Predic (PrVar "N") [Fun "-" [Const "1",Const "1"]])])
*Prover> 
-}

useProof :: Int -> Assumpt -> Formula -> Maybe (Proof,[Assumpt])
useProof n an g =
  do {
       (d,theta,ans,ys) <- useit n [] an g ; 
       if null ans
       then return (d,[])
       else 
         if null ys
         then return (d,ans)
         else 
           let { 
                 vs = map fst ans ;
                 v = LA.freshVar "v" vs ;
                 w = newGoalSymbol n ;
                 h = foldr1 And (map snd ans) ;
                 m = length ans ;
                 es = [ andElini m i (AnProof v) | i <- [0..m-1]] ;
                 d' = replaces d vs es ;
                 an' = (w,foldr Ex h ys)
               }
           in return (exElims (AnProof w) ys (v,h)  d', [an'])
     }

-- If useit n xs (u,f) g = Just (d,theta,ans,ys), 
-- then (u:f, ans) theta |- d : g with dom theta subset xs
-- and ys are free only in ans theta. 
-- n is used to generate fresh goal symbols.

useit :: Int -> [String] -> Assumpt -> Formula -> 
            Maybe (Proof,[(String,Term)],[Assumpt],[(String,Sort)])
useit n xs (u,f) g =
   case matchF xs f g of
     {
       Just theta -> 
         Just (AnProof u,theta,[],[]) ; -- f theta =alpha= g
       Nothing ->
         case f of
           {
             All (x,s) f0 -> 
               let { ([x'],f0') = rename ([x],f0) (freev g) }
               in case useit n (x':xs) (u,f0') g of
                    {
                      Just (d,theta,ans,ys) ->
                        case LA.getLR theta x' of
                          {
                            Just t ->
                              let { theta1 = LA.deleteL theta [x'] }
                              in Just (replace d u (AllElim (AnProof u) t), 
                                       theta1, 
                                       [(vi,substF (mksubstT theta) fi) |
                                                             (vi,fi) <- ans], 
                                       ys) ;
                            Nothing -> 
                              Just (replace d u 
                                      (AllElim (AnProof u) (Var x')), 
                                    theta,ans,(x',s):ys) 
                          } ;
                      Nothing -> Nothing
                    } ;
             Imp f0 f1 -> 
               case useit (n+1) xs (u,f1) g of
                 {
                   Just (d,theta,ans,ys) -> 
                     let { v = newGoalSymbol n }
                     in Just (replace d u 
                                (ImpElim (AnProof u) (AnProof v)),
                              theta, (v,f0):ans, ys) ;
                   Nothing -> Nothing
                 } ;
             And f0 f1  -> 
               case useit n xs (u,f0) g of
                 {
                   Just (d,theta,ans,ys) ->    
                     Just (replace d u 
                             (AndElil (AnProof u)), theta, ans, ys);
                   Nothing ->
                     case useit n xs (u,f1) g of
                       {
                         Just (d,theta,ans,ys) ->    
                           Just (replace d u 
                                   (AndElir (AnProof u)), 
                                 theta, ans, ys) ;
                         Nothing -> Nothing
                       }
                 } ;
             _ -> Nothing
           }
     }


-- Matching
-- ==========================
-- If matchF xs f g = Just theta, then dom theta = xs and f theta = g

matchF :: [String] -> Formula -> Formula -> Maybe [(String,Term)]
matchF = matchFxys []

matchFxys :: [(String,String)] -> [String] -> Formula -> Formula -> Maybe [(String,Term)]
matchFxys xys xs f g = 
   case (f, g) of
    {
     (Predic p1 ts1, Predic p2 ts2)
                  -> do {
                         theta1 <- matchP xys xs p1 p2 ;
                         theta2 <- matchTs xys xs ts1 ts2 ;
                         unionAssoc theta1 theta2
                        };
     (And f1 f2, And g1 g2)     -> matchFs xys xs [f1,f2] [g1,g2] ;
     (Or f1 f2, Or g1 g2)       -> matchFs xys xs [f1,f2] [g1,g2] ;
     (Imp f1 f2, Imp g1 g2)     -> matchFs xys xs [f1,f2] [g1,g2] ;
     (Bot, Bot)                 -> Just [] ;
     (All (x,s) f1, All (y,t) g1) 
         -> if s == t 
            then matchFxys ((x, y):xys) xs f1 g1 
            else Nothing ;
     (Ex (x,s) f1, All (y,t) g1) 
         -> if s == t 
            then matchFxys ((x, y):xys) xs f1 g1 
            else Nothing ;
     _         -> Nothing  -- improve!
    }
  
matchFs :: [(String, String)] -> [String] -> [Formula] -> [Formula] -> Maybe [(String,Term)]
matchFs xys xs fs gs =
  if length fs == length gs
  then do {
            thetas <- sequence [matchFxys xys xs fi gi | (fi, gi) <- zip fs gs];
            unionAssocs thetas
          }
  else Nothing
    
matchP :: [(String, String)] -> [String] -> Predicate -> Predicate -> Maybe [(String,Term)]
matchP xys xs p1 p2 = if p1 == p2 then Just [] else Nothing  -- to improve in the future version!  (see Formula.hs) 

matchTs :: [(String, String)] -> [String] -> [Term] -> [Term] -> Maybe [(String,Term)]
matchTs xys xs ts1 ts2 =
  if length ts1 == length ts2
  then do {
            thetas <- sequence [matchT xys xs t1i t2i | (t1i, t2i) <- zip ts1 ts2];
            unionAssocs thetas
          }
  else Nothing

matchT :: [(String, String)] -> [String] -> Term -> Term -> Maybe [(String,Term)]
matchT xys xs t1 t2 =
  case (t1, t2) of
    {
      (Const c, Const d)   -> if c == d then Just [] else Nothing ;
      (Var x, t)           -> matchV xys xs x t;
      (Fun fun1 ts1, Fun fun2 ts2) -> 
                              if fun1 == fun2
                              then matchTs xys xs ts1 ts2
                              else Nothing;
       _                   -> Nothing
    }
      
matchV :: [(String, String)] -> [String] -> String -> Term -> Maybe [(String,Term)]
matchV xys xs x t = 
  case LA.getLR xys x of
    {
      Just y -> if t == Var y then Just [] else Nothing ;
      Nothing -> if x `elem` xs 
                 then Just [(x,t)] 
                 else if t == Var x then Just [] else Nothing
   } 

-- Replacing proofs
-- ================================
-- Iterated replacement in proofs

replaces :: Proof -> [Label] -> [Proof] -> Proof
replaces d vs es = 
  foldr (\ (v,e) d' -> replace d' v e) d (zip vs es)
  
-- Iterated application
-- ================================
-- Iterated conjunction elimination

-- If |- d : f0 & (f1 & ( .... f(n-1) & fn) and 0 <= i < n, then andElini n i d |- fi 

andElini :: Int -> Int -> Proof -> Proof
andElini n i d = 
  if n == 1
  then d
  else if i == 0
       then AndElil d
       else andElini (n-1) (i-1) (AndElir d)


-- Iterated existential elimination 

-- If |- d : ex ys h and (v:h) |- e : g where ys not free in g, 
-- then  |- exElims d ys (v,h) d : g

exElims :: Proof -> [(String,Sort)] -> Assumpt -> Proof -> Proof
exElims d [] (v,h) e  = replace e v d
exElims d ((y,s):ys) (v,h) e = 
  let {
        w = "www" ++ show (length ys); -- something new! (need function, also for replace !!!)
        d' = exElims (AnProof w) ys (v,h) e
      }
  in ExElim d (AllIntr (ImpIntr d' (w, foldr Ex h ys)) (y,s))
  
-- Theorems
-- ========================================

-- theorem record
data ThmR = ThmRec {
                    tn :: String, -- theorem name
                    tf :: Formula,
                    tp :: Proof
                   }
          deriving (Show, Read)               
-- theorem standalone (external)
-- for recording theorems as standalone objects
type ThmE = (Label, (Formula, ProofRecord))

-- theorem in a proof (internal)
type ThmI = (Label, (Formula, Proof))

-- converting ThmE into ThmI
thmE2thmI :: [ThmE] -> [ThmI]
thmE2thmI thmes = 
  [(thmn,(f,(prproof prec))) | (thmn,(f,prec)) <- thmes ]

-- converting ThmR into ThmI 
-- (experimental, records only to be used in the next version)
thmR2thmI :: [ThmR] -> [ThmI]
thmR2thmI thmrs = [tothmI t | t <- thmrs]
   where tothmI t = ((tn t),((tf t),(tp t)))
                                                             
-- preloading and parsing theorems from a file
parseThm :: Parser ThmR
parseThm = P reads :: Parser ThmR
-- used for theorems parsing (not as records)
parseProof :: Parser Proof
parseProof = P reads :: Parser Proof

parsePR :: Parser ProofRecord
parsePR = P reads :: Parser ProofRecord

-- reads :: Read a => ReadS a
-- type ReadS a = String -> [(a, String)]

parseThmE :: Language -> Context -> Parser ThmE
parseThmE l c = do {
                  label <- lab;
                  tstring "." ;
                  f <-  parseFormula l c;
                  tstring "." ;
                  prec <- parsePR ;
                  return (label, (f, prec)) 
                 }

-- new way of working with theorems via records
-- quicker loading as theorems are saved with the batch
-- are expected to match language are context
parseThmR :: Parser ThmR
parseThmR = do {
                thmrec <- parseThm ;
                return thmrec
                }                                               
