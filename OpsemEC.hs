-- Program : Prawf; version 2020.001
-- Module : OpsemEC (core)
-- Authors : U.Berger, O.Petrovska
-- Date created : 17 Jan 2020
-- Description : Operational semantics with Extended closures

{- Revision History  :
Date        Author      Ref    Description
-}

module OpsemEC 

 where

import Program
import Perhaps
import Proof
import Language
import ListAux
import Data.List (nub)

-- ==================================

-- Definitions
 -- type: Env
 -- data: Closure EC ClosureTerm
 
-- Functions 
 -- small small' smallit smallit'
 -- ctbot ctp 
 -- norm norm' demo' demo stepbystep
 -- stepchange demo''

data Closure = Clos Program Env deriving Show

type Env = [(String,Closure)]  

-- Extended closures
data EC = ECCL Closure | ECCA EC [Clause] Env | ECAP EC Closure  
           deriving Show

-- small steps
small :: EC -> EC
small ec = 
  case ec of
    {
      ECCL cl@(Clos prog eta) -> 
        case prog of
          {
            ProgVar x ->        -- (ii) 
              case look x eta of
                {
                  Success u -> ECCL u ;
                  Failure s -> 
                     error ("small Progvar: " ++ s ++ show prog) 
                } ;
            ProgCase prog0 clauses ->  -- (iii a)
              ECCA (ECCL (Clos prog0 eta)) clauses eta ;
            ProgApp prog1 prog2 ->  -- (iv a)
              ECAP (ECCL (Clos prog1 eta)) (Clos prog2 eta) ;
            ProgRec prog0 ->   -- (v)
              ECAP (ECCL (Clos prog0 eta)) cl ;
            _ -> ec
          } ;
      ECCA ec0 clauses eta ->
        case ec0 of
          {
            ECCL (Clos (ProgCon c progs) eta0) ->  -- (iii b)
              case look3 c clauses of
                {
                  Success (xs,prog') ->
                    if length xs == length progs
                    then ECCL (Clos 
                                prog' 
                                (zip xs [Clos p eta0 | p <- progs] 
                                 ++ eta))
                    else error ("small ProgCase length: " 
                                ++ show ec) ;
                  Failure s -> 
                    error ("small ProgCase cons: " ++ show ec) 
                } ; 
            _ -> ECCA (small ec0) clauses eta   -- (iii c)
          } ;
      ECAP ec0 cl ->
        case ec0 of
          {
            ECCL (Clos (ProgAbst x prog0) eta) ->  -- (iv b)
              ECCL (Clos prog0 ((x,cl):eta)) ;
            _ -> ECAP (small ec0) cl                -- (iv c)
          } ;
    }
         
data ClosureTerm = Cl EC | Con Constructor [ClosureTerm]
                    deriving Show

small' :: ClosureTerm -> ClosureTerm
small' (Cl ec) =
   case ec of
     {
       ECCL (Clos (ProgCon c progs) eta) ->
         Con c [Cl (ECCL (Clos p eta)) | p <- progs] ;    
       _ -> Cl (small ec)
     }
small' (Con c cts) = Con c (map small' cts)      

-- n steps       
smallit :: Int -> ClosureTerm -> ClosureTerm
smallit n ct = if n <= 0 then ct else smallit (n-1) (small' ct)

-- all steps up to n
smallit' :: Int -> ClosureTerm -> [ClosureTerm]
smallit' n ct = if n <= 0 then [] else ct : smallit' (n-1) (small' ct)

ctbot :: ClosureTerm -> D
ctbot (Cl _)      = DBot   -- bot would loop!
ctbot (Con c cts) = conApp c (map ctbot cts)

ctp :: Program -> ClosureTerm
ctp p = Cl (ECCL (Clos p []))

-- n steps, result in D
norm :: Int -> Program -> D
norm n p = ctbot (smallit n (ctp p))

-- all steps up to n, results in D
norm' :: Int -> Program -> [D]
norm' n p = map ctbot (smallit' n (ctp p))

-- pretty print version of norm'
demo' :: Int -> Program -> IO ()
demo' n p = 
  putStrLn 
    (concat 
      [show i ++ "  " ++ show d ++ "\n" | 
         (i,d) <- zip [0..n] (norm' (n+1) p)])
         
-- shows state after a certain number of steps         
demo :: Int -> Program -> IO ()
demo n p = 
  putStrLn 
    (head $ reverse
      [show i ++ "  " ++ show d ++ "\n" | 
         (i,d) <- zip [0..n] (norm' (n+1) p)])
         
-- allows viewing execution step by step      
stepbystep :: Int -> Program -> IO ()
stepbystep n p = 
  do {input <- getLine;
      if input == ""
      then do { demo n p;
                stepbystep (n+1) p;
               }
      else return ();
     }
    

-- for debugging -- not used
-- prints out closure terms instead of D         
stepchange :: Int -> Program -> [ClosureTerm]
stepchange n p = smallit' n (ctp p)

demo'' :: Int -> Program -> IO ()
demo'' n p =
  putStrLn 
    (concat 
      [show i ++ "  " ++ show d ++ "\n" | 
         (i,d) <- zip [0..n] (stepchange (n+1) p)])
