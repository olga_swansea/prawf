-- Program : Prawf; version 2020.001
-- Module : ParseLang (gen)
-- Authors : U.Berger, O.Petrovska
-- Date created : 17 Jan 2020
-- Description : parsing for language

{- Revision History  :
Date        Author      Ref    Description
-}

module ParseLang -- gen
  where

-- haskell
import Control.Monad (foldM)
import Data.List (intercalate)
import qualified Data.Map.Strict as Map

-- aux
import Parser
import ListAux
import Perhaps

-- core
import Language

-- ==================================
-- Functions 
 -- parseSort parseArity parseType parseConst parseFun 
 -- parsePconst parseVar parsePvar parseOvar  
 -- parseNewFun parseNewVar parseNewPvar parseNewOvar
 -- parseQuantifier
 -- parseAbstraction0 parseAbstraction1 
 -- parseSingleAbstraction0 parseSingleAbstraction1 
 -- parseFix 
 -- parseTerm funinfix binfunterm 
 -- parseFormula loginfix parseEquation 
 -- parsePredicate parsePredicateA 
 -- parseOperator parseOperatorA parseDeclaration parseDecl'
 -- parseAF parseD parseDs

-- Contents
-- ========
-- Examples
-- Strings
-- Parsing
-- Showing (moved to ReadShow)


-- Examples
-- ========

{-
*ParseLang> runP0 (parseTerm lReal cx) "abs(x)+1"
Fun "+" [Fun "abs" [Var "x"],Const "1"]
*ParseLang> runP0 (parseFormula lReal cx) "let Phi:(R) = lambda X:(R) lambda x:R x=0 or X(x-1) in let N:(R) = mu Phi in all x:R all y:R (x=0 -> y=0 -> x+y=0)"
LetF (OpDecl ("Phi",["R"]) (OpA ("X",["R"]) (Compr [("x","R")] (Or (Predic (PrConst "=") [Var "x",Const "0"]) (Predic (PrVar "X") [Fun "-" [Var "x",Const "1"]]))))) (LetF (PredDecl ("N",["R"]) (Mu (OpV "Phi"))) (All ("x","R") (All ("y","R") (Imp (Predic (PrConst "=") [Var "x",Const "0"]) (Imp (Predic (PrConst "=") [Var "y",Const "0"]) (Predic (PrConst "=") [Fun "+" [Var "x",Var "y"],Const "0"]))))))
*ParseLang> putStrLn (showFormula (runP0 (parseFormula lReal cx) "let (Phi,(R)) = lambda (X,(R)) lambda (x,R) x=0 or X(x-1) in let (N,(R)) = mu Phi in all (x,R) all (y,R) (N(x) -> N(y) -> N(x+y))"))
Let (Phi,(R))  = lambda (X,(R)) lambda ((x,R) )x = 0 v X(x - 1) in 
(Let (N,(R))  = Mu(Phi) in 
(All (x,R) (All (y,R) (N(x) -> (N(y) -> N(x + y))))))

let (Phi,(R)) = lambda (X,(R)) lambda (x,R) x=0 or X(x-1) in let (N,(R)) = mu Phi in all (x,R) all (y,R) (N(x) -> N(y) -> N(x+y))

-}


-- Strings
-- =======

botstrings, notstrings, andstrings, orstrings, impstrings, allstrings, exstrings, letstrings, multstrings, plusstrings :: [String]
botstrings = ["bot", "\\bot", "Bot", "F", "_|_"] 
notstrings = ["not", "\\neg", "-"] 
andstrings = ["&", "and", "\\land"]
orstrings  = ["or", "v", "|", "\\lor"]
impstrings = ["->", "\\to"]
allstrings = ["all", "All", "For all", "for all", "\\forall"]
exstrings = ["ex", "Ex", "Exists", "\\exists"]
predstrings = ["pred", "Pred"]
letstrings = ["let", "Let"] 
skolemstrings = ["skolem", "Skolem"] 
multstrings = ["*","/"]
plusstrings = ["+","-"]
abststrings = ["\\","lambda","Lambda"]
mustrings = ["mu","Mu"]
nustrings = ["nu","Nu"]

relinfixstrings = ["=", "<", ">", "<=", ">=", "<<"]


botstr, notstr, andstr, orstr, impstr, allstr, exstr, predstr, letstr, multstr, plusstr :: String
botstr = head botstrings
notstr = head notstrings
andstr = head andstrings
orstr  = head orstrings
impstr = head impstrings
allstr = head allstrings
exstr = head exstrings
predstr = head predstrings
letstr = head letstrings
skolemstr = head skolemstrings
multstr = head multstrings
plusstr = head plusstrings
abststring = head abststrings


-- Parsing
-- =======

-- Sorts, Arities, types with sorts declared in language

parseSort :: Language -> Parser Sort
parseSort l = tstrings (sorts l)

parseArity :: Language -> Parser Arity
parseArity l = listing (parseSort l)

parseType :: Language -> Parser Type
parseType l = parsePair (parseArity l) (parseSort l)
                                    -- parsePair in Parser


-- Variables, function symbols, constants, 
-- declared in language resp. context

parseConst :: Language -> Parser (String,Sort)
parseConst l = tstringassocs (constants l)

parseFun :: Language -> Parser (String,Type)
parseFun l = tstringassocs (functions l)

parsePconst :: Language -> Parser (String,Arity)
parsePconst l = tstringassocs (predicates l)

parseVar :: Context -> Parser (String,Sort)
parseVar c = tstringassocs (variables c)

parsePvar :: Context -> Parser (String,Arity)
parsePvar c = tstringassocs (pvars c)

parseOvar :: Context -> Parser (String,Arity)
parseOvar c = tstringassocs (ovars c)


-- New variables, function symbols, 

-- maybe use (f:type) format for funs too?? i.e. use parseColonPair?
parseNewFun :: Language -> Parser (String,Type)
parseNewFun l = parsePair tidentifier (parseType l)

parseNewVar :: Language -> Parser (String,Sort)
parseNewVar l = parsePair tidentifier (parseSort l)

parseNewPvar :: Language -> Parser (String,Arity)
parseNewPvar l = parsePair tidentifier (parseArity l)

parseNewOvar :: Language -> Parser (String,Arity)
parseNewOvar l = parsePair tidentifier (parseArity l)


-- Quantifiers

parseQuantifier :: Parser ((String,Sort) -> Formula -> Formula)
parseQuantifier = (tstrings allstrings >> return All) `pluss`
                  (tstrings exstrings >> return Ex)


-- Abstraction

parseAbstraction0 :: Language -> Parser [(String,Sort)]
parseAbstraction0 l = 
    tstrings abststrings >> listing (parseNewVar l)

parseAbstraction1 :: Language -> Parser [(String,Arity)]
parseAbstraction1 l = 
    tstrings abststrings >> listing (parseNewPvar l)

parseSingleAbstraction0 :: Language -> Parser (String,Sort)
parseSingleAbstraction0 l = 
    tstrings abststrings >> parseNewVar l

parseSingleAbstraction1 :: Language -> Parser (String,Arity)
parseSingleAbstraction1 l = 
    tstrings abststrings >> parseNewPvar l


-- Fixed points

parseFix :: Parser (Op -> Predicate)
parseFix= (tstrings mustrings >> return Mu) `pluss`
          (tstrings nustrings >> return Nu)

-- Terms

-- sorts are ignored at this moment!
parseTerm :: Language -> Context -> Parser Term
parseTerm l c = 
  p1bn (1,2)                       -- see Parser 
       (plusss [constant, 
                variable, 
                prefixbn funprefix (parseTerm l c)]) 
       funinfix 
  where                 
    constant = do {
                     (const,s) <- parseConst l ;
                     return (Const const)
                   }
    variable  = do {
                     (x,s) <- parseVar c ;
                     return (Var x)
                   }
--  funprefix :: Parser (Int,[Term] -> Term)
    funprefix = 
      do {
           (f,ty) <- parseFun l ;
           return (length (fst ty), Fun f)
         }
 
funinfix :: [(Parser (Term -> Term -> Term) , Int, Orientation)]
funinfix = 
   [
    (tstrings multstrings >>= 
      \fun-> return (binfunterm fun),2,R), 
    (tstrings plusstrings >>=         -- tstrings in Parser 
      \fun-> return (binfunterm fun),1,R)
   ]

binfunterm :: String -> Term -> Term -> Term
binfunterm fun t1 t2 = Fun fun [t1,t2]


-- Formulas

parseFormula :: Language -> Context -> Parser Formula
parseFormula l c = 
  p1bn (1,2)                       
       (plusss [infixrel, predf, botf, negf, quantf, letf])
       loginfix  
 where
  infixrel = do {
                  t1 <- parseTerm l c ;
                  r <- tstrings relinfixstrings ;
                  t2 <- parseTerm l c ;
                  return (Predic (PrConst r) [t1,t2])
                } 
  predf = do {
              (p,a) <- parsePredicateA l c ;
              if null a 
              then return (Predic p []) 
              else do {
                        ts <- inbrackets 
                                 (listingn (length a) (parseTerm l c)) ;
                        return (Predic p ts)
                      }
             }
  botf = tstrings botstrings >> return Bot
  negf = do {
              tstrings notstrings ;
              f <- parseFormula l c ;
              return (Imp f Bot)
            }
  quantf = do {
                q <- parseQuantifier ;
                xs <- parseNewVar l;
                f <- parseFormula l (addCVars c [xs]) ;
                return (q xs f)
              }
  letf = do { 
              tstrings letstrings ;
              (d,l',c') <- parseDeclaration l c ;
              tstring "in" ;
              f <- parseFormula l' c' ;
              return (LetF d f)
            }

loginfix :: [(Parser (Formula -> Formula -> Formula), 
             Int, Orientation)]
loginfix = 
  [
    (tstrings andstrings >> return And,2,R), 
    (tstrings orstrings >> return Or,2,R), 
    (tstrings impstrings >> return Imp,1,R)
  ] 
   
-- Equations

parseEquation :: Language -> Context -> Parser (Term,Term)
parseEquation l ctx =
  do { 
       f <- parseFormula l ctx ;
       case f of
         {
           Predic (PrConst "=") [r,t] -> return (t,r) ;
           _ -> fail ("ParseLang parseEquation: Not an equation: "
                      ++ show f)
         }
     }

-- Predicates

parsePredicate :: Language -> Context -> Parser Predicate
parsePredicate l c = parsePredicateA l c >>= return.fst

parsePredicateA :: Language -> Context -> Parser (Predicate, Arity)
parsePredicateA l c = plusss [ pv, pc, compr, fix, oapp, bpp ]
  where
    pv = do { (x,a) <- parsePvar c; return (PrVar x,a) }
    pc = do { (cnst,a) <- parsePconst l; return (PrConst cnst,a) }
    compr = do { 
                 xss <- parseAbstraction0 l ; 
                 f <- parseFormula l (addCVars c xss) ;
                 return (Compr xss f,map snd xss)
               } 
    fix = do { 
               fix <- parseFix ; 
               (o,a) <- parseOperatorA l c ;
               return (fix o,a)
             } 
    oapp = do { 
                (o,a) <- parseOperatorA l c ;
                p <- parsePredicate l c ;
                return (OpApp o p, a)
              }
    bpp = inbrackets (parsePredicateA l c)

-- Operators

parseOperator :: Language -> Context -> Parser Op
parseOperator l c = parseOperatorA l c >>= return.fst

parseOperatorA :: Language -> Context -> Parser (Op,Arity)
parseOperatorA l c = plusss [oabst, ov, bpo ]
  where
   oabst = do {
                (x,a) <- parseSingleAbstraction1 l ;
                p <- parsePredicate l (addCPVars c [(x,a)]) ;
                return (OpA (x,a) p,a)
              }
   ov = do { 
             (phi,a) <- parseOvar c ;
             return (OpV phi,a)
           }
   bpo = inbrackets (parseOperatorA l c)


-- Declarations

parseDeclaration :: 
          Language -> Context -> Parser (Decl,Language,Context)
parseDeclaration l c = plusss [ fundecl, prdecl, opdecl ]
  where
   fundecl = do { 
                  gty <- parseNewFun l ;
                  tstring "=" ;
                  tstrings skolemstrings ; 
                  p <- parsePredicate l c ;
                  return (FunDecl gty p, addLFuns l [gty], c)
                } 
   prdecl  = do { 
                  xa <- parseNewPvar l ;
                  tstring "=" ;
                  p <- parsePredicate l c ;
                  return (PredDecl xa p, l, addCPVars c [xa])
                }
   opdecl = do { 
                  phia <- parseNewOvar l ;
                  tstring "=" ;
                  o <- parseOperator l c ;
                  return (OpDecl phia o, l, addCOVars c [phia])
                }

-- Only used for parsing declaration in decl rule, 
-- hence no impact on global ctx and language
parseDecl' :: Language -> Context -> Parser Decl
parseDecl' l c = plusss [ fundecl, prdecl, opdecl ]
  where
   fundecl = do { 
                  gty <- parseNewFun l ;
                  tstring "=" ;
                  tstrings skolemstrings ; 
                  p <- parsePredicate l c ;
                  return (FunDecl gty p)
                } 
   prdecl  = do { 
                  xa <- parseNewPvar l ;
                  tstring "=" ;
                  p <- parsePredicate l c ;
                  return (PredDecl xa p)
                }
   opdecl = do { 
                  phia <- parseNewOvar l ;
                  tstring "=" ;
                  o <- parseOperator l c ;
                  return (OpDecl phia o)
                }
                
-- preloading and parsing assumptions from a file
parseAF :: Language -> Context -> Parser (String, Formula)
parseAF l c = do {
                  label <- lab;
                  tstring "." ;
                  f <-  parseFormula l c;
                  return (label,f)
                 }

-- for preloading declarations                 
parseD :: ([Decl], Language, Context) -> String -> ([Decl],Language,Context)
parseD (ds, l, c) str = 
    let (d, l', c') = runP0 (parseDeclaration l c) str
    in (d:ds, l', c')

parseDs :: Language -> Context -> [String] -> ([Decl], Language, Context)
parseDs l c [] = --foldl parseD ([], l, c) strs
    ([],l,c)
parseDs l c (s:strs) = 
    let {
        (d,l',c') = runP0 (parseDeclaration l c) s ;
        (ds,l'',c'') = parseDs l' c' strs ;
       }
    in (d:ds,l'',c'')


