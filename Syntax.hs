-- Program : Prawf; version 2020.001
-- Module : Syntax (aux)
-- Authors : U.Berger
-- Date created : 19 Jan 2020
-- Description : Syntax

{- Revision History  :
Date        Author      Ref    Description
-}
module Syntax where  -- aux

import ListAux  -- aux

-- ==================================
-- Definitions: 
  -- class Syntax
  -- data Rubbish
  
-- Functions 
 -- rename replaceStr 

-- ===================================
    

class Syntax a where
  allv :: a -> [String]      -- all variables
  freev :: a -> [String]     -- free variables
  replacev :: [(String,String)] -> a -> a

-- replacev (zip xs ys) t is the result of simultaneously replacing in t
-- all free occurrences of xs by ys without avoiding variable capture.

-- rename (xs,t) ys = (xs',t') where xs' and ys are disjoint 
-- and lambda xs t =alpha= lambda xs' t'. 
-- Moreover, (xs',t') is as close to (xs,t) as possible. 
-- I.p. if xs and ys are disjoint, then (xs',t') = (xs,t). 

rename :: Syntax a => ([String],a) -> [String] -> ([String],a)
rename ([],t) ys = ([],t)
rename (x:xs,t) ys = 
  let { 
        x' = freshVar x (remove x (allv t) ++ ys) ; 
--        x' = freshVar x (allv t ++ ys) ; 
        t' = replacev [(x, x')] t ;
        (xs',t'') = rename (xs,t') ys
      }
  in (x':xs',t'')


-- alpha-equality could (should?) be added to the class Syntax

-- Testing
-- ========================

data Rubbish = Rubbish [String] deriving Show

instance Syntax Rubbish where
  allv (Rubbish xs) = xs
  freev (Rubbish xs) = xs 
  replacev xys (Rubbish xs) = Rubbish (replaceStr xys xs)

replaceStr :: [(String,String)] -> [String] -> [String]
replaceStr xys xs = [case getLR xys x of {Just y -> y;_ -> x}|x <- xs]

{-
*Syntax> rename (["x","y"],Rubbish ["x","x'"]) ["y","x''"]
(["x","y'"],Rubbish ["x","x'"])
*Syntax> rename (["x","y"],Rubbish ["x","x'"]) ["x","x''"]
(["x'''","y"],Rubbish ["x'''","x'"])
*Syntax> rename (["x","y"],Rubbish ["x","z"]) ["x","x''"]
(["x'","y"],Rubbish ["x'","z"])
*Syntax> rename (["x","y"],Rubbish ["x","y"]) ["x","x''"]
(["x'","y"],Rubbish ["x'","y"])
-}