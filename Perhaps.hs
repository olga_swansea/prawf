-- Program : Prawf; version 2020.001
-- Module : Perhaps (aux)
-- Authors : U.Berger, O.Petrovska
-- Date created : 17 Jan 2020
-- Description : Defines the Perhaps data type for communicating a result 
           -- of a computation or an error message in case of failure 

{- Revision History  :
Date        Author      Ref    Description
-}

module Perhaps  -- aux

 where

-- haskell
import Control.Applicative

-- ==============================
-- Definitions
 -- data: Perhaps
 
-- Instances
 -- Functor Perhaps / Applicative Perhaps / Monad Perhaps
 
-- Functions 
 -- look look3 find' exec execloop 
 -- (TO CHANGE) toPerhaps toMaybe
 -- findJust mapply

data Perhaps a = Success a | Failure String
               deriving (Show, Eq)

instance Functor Perhaps where
  fmap f (Success a) = Success (f a)
  fmap f (Failure s) = Failure s

instance Applicative Perhaps where
  pure a = Success a

  Success f <*> a = fmap f a
  Failure s <*> _ = Failure s

  Success a *> b = b
  Failure s *> _ = Failure s
  
instance Monad Perhaps where
   px >>= f = case px of 
               {
                Success x -> f x;
                Failure s -> Failure s
               }
   return x = Success x
   fail s   = Failure s

look :: (Eq a, Show a) => a -> [(a,b)] -> Perhaps b
look x []               = Failure ("look not found: " ++ show x)
look x ((x0,y0):xys)    = if x == x0
                          then Success y0
                          else look x xys

look3 :: (Eq a, Show a) => a -> [(a,b,c)] -> Perhaps (b,c)
look3 x []                   = Failure ("look3 not found:  " ++ show x)
look3 x ((x0,y0,z0):xyzs)    = if x == x0
                               then Success (y0,z0)
                               else look3 x xyzs

-- variant of find for Perhaps 
-- quick hack, to redo                         
find' :: (Eq a, Eq b, Show a, Show b) => (a,b) -> [(a,b,c)] -> Perhaps (a,b,c)
find' (x,y) []   = Failure ("not found: " ++ show x ++ " " ++ show y)
find' (x,y) ((x0,y0,z0):xyzs) = if x == x0 && y == y0
                                then Success (x0,y0,z0)
                                else find' (x,y) xyzs
                 
-- !!                  
toPerhaps :: Maybe a -> Perhaps a
toPerhaps (Just x) = (Success x)
toPerhaps (Nothing) = fail("HERE IT FAILS to Perhaps ==================! faill")

toMaybe :: Perhaps a -> Maybe a                  
toMaybe (Success x) = (Just x)
toMaybe _ = Nothing

-- Maybe
-- =====

-- findJust [mx1,...] finds the first mxi of the form Just _ if any
findJust :: [Maybe a] -> Maybe a
findJust (mx : mxs) = case mx of { Just _ -> mx; _ -> findJust mxs }
findJust [] = Nothing            

-- Monadic apply
-- =============

mapply :: Monad m => (a -> b -> m c) -> m a -> m b -> m c
mapply f mx my = do { x <- mx; y <- my; f x y }
