-- Program : Prawf; version 2020.001
-- Module : Indentprint (aux)
-- Authors : O.Petrovska
-- Date created : 18 Jan 2020
-- Description : pretty print of programs and proofs

{- Revision History  :
Date        Author      Ref    Description

-}

module Indentprint
  where
  
import Program 
import Proof
import Language
import Perhaps

-- -------------------------------------------
-- Functions
-- indentedProg indentpr indentprs addspaces
-- indentcls indentcl indentedProof indentproof
-- indentf indentpred indento checkifextracted
-- -------------------------------------------
-- default indent
level = 3 

-- indented program view ---------------------
indentedProg :: Program -> IO()
indentedProg prog = putStr (indentpr 0 prog)

indentpr :: Int -> Program -> String
indentpr n prog = case prog of
    {
      ProgVar a -> 
        addspaces n ("ProgVar " ++ a ++ "\n");
      ProgCon c progs -> 
        let n' = n+level 
        in addspaces n ("Conpr " ++ (show c) ++ "\n" 
                               ++ (indentprs n' progs) ++ "\n");
      ProgCase prog1 csps -> 
        let n' = n+level 
        in addspaces n ("Casepr \n" ++ (indentpr n' prog1) 
                                  ++ (indentcls n' csps) ++ "\n");
      ProgApp prog1 prog2 -> 
        let n' = n+(2*level) 
        in addspaces n ("Apppr \n" ++ (indentpr n' prog1) 
                                 ++ (indentpr n' prog2));
      ProgAbst a prog1 -> 
        let n' = n+level 
        in addspaces n ("Abstpr " ++ a ++ "\n" ++ (indentpr n' prog1)); 
      ProgRec  prog1 -> 
        let n' = n+level 
        in addspaces n ("Recpr \n" ++ (indentpr n' prog1)) 
    }
    
indentprs :: Int -> [Program] -> String
indentprs n [] = ""
indentprs n (p:ps) = (indentpr n p) ++ "\n" ++ (indentprs n ps)
    
addspaces :: Int -> String -> String
addspaces n s = (replicate n ' ') ++ s

indentcls n csps = concat (map (indentcl n) csps) -- addspaces n (show csps)

indentcl n cl@(c,s,p) = 
     addspaces n ("*{" ++ show c ++ " " ++ show s ++ "\n" 
                     ++ (indentpr (n + level) p) ++ "}")

-- indented proof view ---------------------------
-- may need to be modified
indentedProof :: Proof -> IO()
indentedProof p = putStr (indentproof 0 p)

indentproof :: Int -> Proof -> String
indentproof n p = case p of
   {
    AnProof av -> 
       addspaces n ("AnProof " ++ show av ++ "\n");
    AndIntr p1 p2 -> 
       let n' = n+level 
       in addspaces n ("And+ \n" ++ (indentproof n' p1) 
                                 ++ (indentproof n' p2));
    AndElil p1 -> 
       let n' = n+level 
       in addspaces n ("And-l \n" ++ (indentproof n' p1));
    AndElir p1 -> 
       let n' = n+level 
       in addspaces n ("And-r \n" ++ (indentproof n' p1));
    ImpIntr p1 avf -> 
       let n' = n+level 
       in addspaces n ("Imp+ " ++ (show avf) ++ ": \n" 
                               ++ (indentproof n' p1));
    ImpElim p1 p2 -> 
       let n' = n+level 
       in addspaces n ("Imp- \n" ++ (indentproof n' p1) ++ "\n" 
                                 ++ (indentproof n' p2));
    OrIntrl p1 f -> 
       let n' = n+level 
       in addspaces n ("Or+l \n" ++ (indentf n' f) 
                               ++ (indentproof n' p1));
    OrIntrr p1 f -> 
       let n' = n+level 
       in addspaces n ("Or+r \n" ++ (indentf n' f) 
                               ++ (indentproof n' p1));
    OrElim p1 p2 p3 -> 
       let n' = n+level 
       in addspaces n ("Or- \n" ++ (indentproof n' p1) ++ "\n" 
                                ++ (indentproof n' p2) ++ "\n" 
                                ++ (indentproof n' p3));
    ExFQ p1 f -> 
       let n' = n+level 
       in addspaces n ("efq \n" ++ (indentf n' f) 
                                ++ (indentproof n' p1));
    ReAA p1 -> 
       let n' = n+level 
       in addspaces n ("raa \n" ++ (indentproof n' p1));
    AllIntr p1 x -> 
       let n' = n+level 
       in addspaces n ("All+ " ++ (show x) ++ " \n" 
                               ++ (indentproof n' p1));
    AllElim p1 t -> 
       let n' = n+level 
       in addspaces n ("All- " ++ (show t) ++ " \n" 
                               ++ (indentproof n' p1));
    ExIntr p1 x f t -> 
       let n' = n+level 
       in addspaces n ("Ex+ [" ++ (show x) ++ "/" 
                               ++ (show t) ++  "] in \n" 
                               ++ (indentf n' f) 
                               ++ (indentproof n' p1));
    ExElim p1 p2 -> 
      let n' = n+level 
      in addspaces n ("Ex- \n" ++ (indentproof n' p1) 
                               ++ (indentproof n' p2));
    LetP d p1 -> 
      let n' = n+level 
      in addspaces n ("let decl: " ++ (show d) ++ "\n" 
                                   ++ (indentproof n' p1));
    DeclP d pl -> 
      let n' = n+level 
      in addspaces n ("declp: " ++ (show d) ++ "\n" 
                                ++ (indentproof n' pl));
    Ind pred p1 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("Ind \n" ++ (indentpred n' pred) ++ " \n" 
                             ++ (indentproof n'' p1));
    Coi pred p1 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("Coi \n" ++ (indentpred n' pred) ++ " \n" 
                             ++ (indentproof n'' p1));
    HSInd pred p1 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("Ind \n" ++ (indentpred n' pred) ++ " \n" 
                             ++ (indentproof n'' p1));
    HSCoi pred p1 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("HSCoi \n" ++ (indentpred n' pred) ++ " \n" 
                             ++ (indentproof n'' p1));
    SCoi pred p1 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("SCoi \n" ++ (indentpred n' pred) ++ " \n" 
                             ++ (indentproof n'' p1));    
    Ind' pred p1 x y p2 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("Ind' \n" ++ (indentpred n' pred) ++ " \n" 
                             ++ (indentproof n'' p1));
    Coi' pred p1 x y p2 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("Coi' \n" ++ (indentpred n' pred) ++ " \n" 
                             ++ (indentproof n'' p1));
    HSInd' pred p1 x y p2 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("HSInd' \n" ++ (indentpred n' pred) ++ " \n" 
                                   ++ (indentproof n'' p1));                 
    HSCoi' pred p1 x y p2 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("HSCoi' \n" ++ (indentpred n' pred) ++ " \n" 
                             ++ (indentproof n'' p1));                     
    SCoi' pred p1 x y p2 -> 
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("SCoi' \n" ++ (indentpred n' pred) ++ " \n"                                                                 
                             ++ (indentproof n'' p1));
    Clo pred -> 
      let n' = n+level 
      in addspaces n ("Clo \n" 
                             ++ (indentpred n' pred) ++"\n"); 
    Cocl pred -> 
      let n' = n+level 
      in addspaces n ("Cocl \n" 
                              ++ (indentpred n' pred) ++ "\n");
    EqRefl t -> 
      addspaces n ("Refl " ++ (show t) ++ "\n");
    EqSym p1 -> 
      let n' = n+level 
      in addspaces n ("Sym  \n" ++ (indentproof n' p1));
    EqCong p1 ps pred -> 
      let n' = n+level; n'' = n'+ level; n''' = n'' + level 
      in addspaces n ("Cong \n" 
                        ++ (indentproof n' p1) ++ "\n" 
                        ++ (concat (map (indentproof n''') ps))
                        ++ (indentpred n' pred) ++ "\n" );
    Rep f p1 ->
      let n' = n+level; n'' = n'+ level  
      in addspaces n ("Rep \n" ++ (indentf n' f) ++ " \n" 
                             ++ (indentproof n'' p1));

   }

--indentf n f = addspaces n (show f) ++ "\n"
indentf :: Int -> Formula -> String
indentf n f = 
  case f of
    {
      Predic p ts -> 
        let n' = n+level; 
        in addspaces n ("Predic " ++ "\n" 
                                  ++ (indentpred n' p) ++ "\n"
                                  ++ (addspaces n' (show ts)) ++ "\n"); 
      And f g -> 
        let n' = n+level 
        in addspaces n ("And " ++ "\n" 
                               ++ (indentf n' f)
                               ++ (indentf n' g));     
      Or f g -> 
        let n' = n+level 
        in addspaces n ("Or " ++ "\n" 
                              ++ (indentf n' f)
                              ++ (indentf n' g));
      Imp f g -> 
        let n' = n+level 
        in addspaces n ("Imp " ++ "\n" 
                               ++ (indentf n' f)
                               ++ (indentf n' g));
      Bot -> addspaces n ("Bot") ;
      All (x,s) f -> 
        let n' = n+level 
        in addspaces n ("All " ++ (show (x,s)) ++ "\n" 
                               ++ (indentf n' f));
      Ex (x,s) f -> 
        let n' = n+level 
        in addspaces n ("Ex " ++ (show (x,s)) ++ "\n" 
                               ++ (indentf n' f));
      LetF decl f -> 
        let n' = n+level 
        in addspaces n ("LetF " ++ (show decl) ++ "\n" 
                               ++ (indentf n' f));
    }

--quick solution, need to be improved for bigger proofs    
indentpred :: Int -> Predicate -> String
indentpred n p =
  case p of
    {
     Compr xss f -> 
        let n' = n+level 
        in addspaces n ("Compr " ++ (show xss) ++ "\n" 
                                 ++ (indentf n' f));
     Mu o -> 
        let n' = n+level 
        in addspaces n ("Mu* " ++ "\n" ++ (indento n' o));
     Nu o -> 
        let n' = n+level 
        in addspaces n ("Nu " ++ "\n" ++ (indento n' o));
     OpApp o p' ->
        let n' = n+level 
        in addspaces n ("OpApp " ++ "\n" 
                                 ++ (addspaces n' (show o))
                                 ++ (indentpred n' p));                                 
     _ -> addspaces n (show p);
    }

indento :: Int -> Op -> String
indento n o = 
  case o of
    {
     OpA (s,a) p -> 
        let n' = n+level 
        in addspaces n ("Op " ++ show (s,a) ++ "\n" 
                              ++ (indentpred n' p));
     _ -> addspaces n (show o)
    }
    
-- prints out extracted program
checkifextracted :: String -> Perhaps Program -> IO ()
checkifextracted s prog = 
      case prog of 
          { Success prog -> 
                putStr ("\n================================ \n" ++ 
                        "Extracted program (" ++ s ++ "):\n" ++ 
                        "================================ \n" ++ 
                          (indentpr 0 prog));
             _ -> putStr "Proof failed to extract. ";
           }