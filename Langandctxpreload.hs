-- Program : Prawf; version 2020.001
-- Module : Langandctxpreload (gen)
-- Authors : O.Petrovska
-- Date created : 17 Jan 2020
-- Description : Used for parsing language and context and checks consistency

{- Revision History  :
Date        Author      Ref    Description

-}

module Langandctxpreload

  where

-- haskell
import GHC.IO
import System.Process

-- aux
import Perhaps
import ListAux as LA

-- core
import Language
import Proof
import Extraction

-- gen
import ParseLang
import Parser
import Step
import ReadShow

-- Functions 
 -- planguage pcontext 
 -- psrts psrt pconsts pfuns pfun 
 -- ptype parity 
 -- ppreds ppando pops
 -- pcvargroup pcvars 
 -- splitVS splitOn dropDelim
 -- consistencycheck consistencycheck'
 -- 
 
-- Contents:
-- ============

-- Parsing Language and Context
-- Consistency check

-- ============== Parsing Language and Context ===============================           
planguage :: Parser Language
planguage = do
    pss <- optlist psrts
    pcs <- optlist pconsts
    pfs <- optlist pfuns
    pps <- optlist ppreds
    pos <- optlist pops
    let check = consistencycheck pss pcs pfs pps pos 
    if check 
    then do let lang = Lang {sorts = pss,
                       constants = pcs,
                       functions = pfs,
                       predicates = pps,
                       operators = pos}
            return lang
    else fail "Language inconsistent! "
             
              
-- ["R"] [("0","R")] [("-",(["R","R"],"R"))] [("P",["R","R"])] [("Phi",["R"])]
    
pcontext :: Parser Context
pcontext = do
   pcvs  <- optlist pcvars
   pcpvs <- optlist ppreds
   pcovs <- optlist pops
   let ctx = Ctxt {variables = pcvs,
                   pvars     = pcpvs,
                   ovars     = pcovs
                  }
   return ctx
               
psrts :: Parser [Sort]
psrts = do 
    tstring "<sorts>"
    ss <- psrt `sepby1` (token $ char ';')
    tstring "<end sorts>"
    return ss

psrt :: Parser Sort
psrt = do 
    s <- upper
    ss <- many letter
    return $ s : ss
    
pconsts :: Parser [(String, Sort)]
pconsts = do
    tstring "<constants>"
    cs <- pcvargroup `sepby1` (token $ char ';')
    tstring "<end constants>"
    let scs = map splitVS cs
    return $ concat scs
    
pfuns :: Parser [(String,Type)]
pfuns = do 
    tstring "<functions>"
    fs <- pfun `sepby1` (token $ char ';')
    tstring "<end functions>"
    return fs

pfun :: Parser (String,Type)
pfun = do
    fn <- token name
    tstring ":"
    ty <- ptype
    return (fn,ty)
    
ptype :: Parser Type
ptype = do
    ar <- token parity
    tstring "->"
    s <- token psrt
    return (ar,s)
    
parity :: Parser [Sort]
parity = do
    tstring "("
    ss <- psrt `sepby0` (token $ char ',')
    tstring ")"
    return ss

-- used for both language and context parsing    
ppreds :: Parser [(String,Arity)]
ppreds = do 
    tstring "<predicates>"
    ps <- ppando `sepby1` (token $ char ';')
    tstring "<end predicates>"
    return ps

-- for parsing predicates and operators
-- used for both language and context parsing
ppando :: Parser (String,Arity)
ppando = do
    po <- token name
    tstring ":"
    ar <- token parity
    return (po,ar)

-- used for both language and context parsing    
pops :: Parser [(String,Arity)]
pops = do 
    tstring "<operators>"
    os <- ppando `sepby1` (token $ char ';')
    tstring "<end operators>"
    return os

-- used for context parsing
-- variables can be written x,y,z : R in the context file
-- and will be parsed as [(x,R),(y:R),(z,R)]
-- predicate and operator variables in the context file
-- are written in the same format as
-- predicates and operators in the language file
pcvargroup :: Parser (String, Sort)
pcvargroup = do
    vs <- token name
    tstring ":"
    s <- token psrt
    return (vs,s)
    
pcvars :: Parser [(String, Sort)]
pcvars = do
    tstring "<vars>"
    vs <- pcvargroup `sepby1` (token $ char ';')
    tstring "<end vars>"
    let svs = map splitVS vs
    return $ concat svs
    
-- gets ("x,y,z","R") as input
splitVS :: (String, Sort) ->  [(String, Sort)]
splitVS (str,s) = 
  let strs = splitOn ',' str
      n = length strs
      ss = zip strs [s  | _ <- [1..n] ]
  in ss     
  
splitOn :: (Eq a) => a -> [a] -> [[a]]
splitOn _ [] = []
splitOn d ys = 
    f:(splitOn d (dropDelim d rem))
  where (f, rem) = break (== d) ys

dropDelim :: Eq a => a ->  [a] -> [a]
dropDelim _ [] = []
dropDelim d (x:xs) = if x == d then xs else x:xs


-- ============== Consistency check =========================

-- basic consistency check - to be improved in future versionsS
consistencycheck :: [Sort] -> [(String,Sort)] -> [(String,Type)] -> [(String,Arity)]
                     -> [(String,Arity)] -> Bool
consistencycheck [] _ _ _ _     = False
consistencycheck ss cs fs ps os = 
   let {css = map fst cs;
        fss = map fst fs;
        pss = map fst ps;
        oss = map fst os;
        qelem = (css,fss,pss,oss)
       }
   in consistencycheck' qelem ss cs fs ps os                    


consistencycheck' :: ([String],[String],[String],[String]) 
    -> [Sort] -> [(String,Sort)] -> [(String,Type)] -> [(String,Arity)]
                     -> [(String,Arity)] -> Bool
consistencycheck' qelem ss cs fs ps os = 
  case qelem of
    {
     ([],[],[],[]) -> True;
     ([],[],[],oss) ->
           let { oss' = concat $ map snd os }
           in (not (oss `elems` ss)) && (oss' `elems` ss);
     ([],[],pss,[]) -> 
           let { pss' = concat $ map snd ps }
           in (not (pss `elems` ss)) && (pss' `elems` ss);
     ([],fss,[],[]) ->
           let {
                fsty = map snd fs; -- [([s0,s0'],s)([s11,s11'],s1)] 
                fssrt = map snd fsty; -- [s,s1] 
                fsar = map fst fsty; -- [[s0,s0'],[s11,s11']]
                fss' = fssrt ++ (concat fsar)
               }
           in (not (fss `elems` ss)) && (fss' `elems` ss);
     ([],[],pss,oss) -> 
           let { 
                strconf = (pss `elems` oss) ||
                          ((pss ++ oss) `elems` ss);
                pss' = concat $ map snd ps;
                oss' = concat $ map snd os;
                sortcheck = pss' `elems` ss && 
                            oss' `elems` ss
               }
           in (not strconf) && sortcheck;
     ([],fss,[],oss) ->
           let { 
                strconf = (fss `elems` oss) ||
                          ((fss ++ oss) `elems` ss);
                fsty = map snd fs; -- [([s0,s0'],s)([s11,s11'],s1)] 
                fssrt = map snd fsty; -- [s,s1] 
                fsar = map fst fsty; -- [[s0,s0'],[s11,s11']]
                fss' = fssrt ++ (concat fsar); 
                oss' = concat $ map snd os;
                sortcheck = fss' `elems` ss && 
                            oss' `elems` ss
               }
           in (not strconf) && sortcheck;
     ([],fss,pss,[]) -> 
           let { 
                strconf = (fss `elems` pss) ||
                          ((fss ++ pss) `elems` ss);
                fsty = map snd fs; -- [([s0,s0'],s)([s11,s11'],s1)] 
                fssrt = map snd fsty; -- [s,s1] 
                fsar = map fst fsty; -- [[s0,s0'],[s11,s11']]
                fss' = fssrt ++ (concat fsar); 
                pss' = concat $ map snd ps;
                sortcheck = fss' `elems` ss && 
                            pss' `elems` ss
               }
           in (not strconf) && sortcheck;
      ([],fss,pss,oss) -> 
           let { 
                strconf = (fss `elems` pss) || (fss `elems` oss) ||
                          (pss `elems` oss) ||
                          ((fss ++ pss ++ oss) `elems` ss);
                fsty = map snd fs; -- [([s0,s0'],s)([s11,s11'],s1)] 
                fssrt = map snd fsty; -- [s,s1] 
                fsar = map fst fsty; -- [[s0,s0'],[s11,s11']]
                fss' = fssrt ++ (concat fsar); 
                pss' = concat $ map snd ps;
                oss' = concat $ map snd os;
                sortcheck = fss' `elems` ss && 
                            pss' `elems` ss && 
                            oss' `elems` ss
               }
           in (not strconf) && sortcheck;
     (css,[],[],[]) -> let {css' = map snd cs}
           in (not (css `elems` ss)) && (css' `elems` ss);
     (css,[],[],oss) ->  
           let { 
                strconf = (css `elems` oss) ||
                          ((css ++ oss) `elems` ss);
                css' = map snd cs;
                oss' = concat $ map snd os;
                sortcheck = css' `elems` ss && 
                            oss' `elems` ss
               }
           in (not strconf) && sortcheck;

     (css,[],pss,[]) ->
           let { 
                strconf = (css `elems` pss) ||
                          ((css ++ pss) `elems` ss);
                css' = map snd cs;
                pss' = concat $ map snd ps;
                sortcheck = css' `elems` ss && 
                            pss' `elems` ss
               }
           in (not strconf) && sortcheck;
     (css,[],pss,oss) -> 
           let {  
                strconf = (css `elems` pss) || (css `elems` oss) ||
                          (pss `elems` oss) ||
                          ((css ++ pss ++ oss) `elems` ss);
                css' = map snd cs;
                pss' = concat $ map snd ps;
                oss' = concat $ map snd os;
                sortcheck = css' `elems` ss && 
                            pss' `elems` ss && 
                            oss' `elems` ss
               }
           in (not strconf) && sortcheck;
     (css,fss,[],[]) -> 
           let { 
                strconf = (css `elems` fss) ||
                          ((css ++ fss) `elems` ss);
                css' = map snd cs;
                fsty = map snd fs; -- [([s0,s0'],s)([s11,s11'],s1)] 
                fssrt = map snd fsty; -- [s,s1] 
                fsar = map fst fsty; -- [[s0,s0'],[s11,s11']]
                fss' = fssrt ++ (concat fsar); 
                sortcheck = css' `elems` ss && 
                            fss' `elems` ss
               }
           in (not strconf) && sortcheck;
     (css,fss,[],oss) ->
           let { 
                strconf = (css `elems` fss) || (css `elems` oss) ||
                          (fss `elems` oss) ||
                          ((css ++ fss ++ oss) `elems` ss);
                css' = map snd cs;
                fsty = map snd fs; -- [([s0,s0'],s)([s11,s11'],s1)] 
                fssrt = map snd fsty; -- [s,s1] 
                fsar = map fst fsty; -- [[s0,s0'],[s11,s11']]
                fss' = fssrt ++ (concat fsar); 
                oss' = concat $ map snd os;
                sortcheck = css' `elems` ss && 
                            fss' `elems` ss && 
                            oss' `elems` ss
               }
           in (not strconf) && sortcheck;
     (css,fss,pss,[]) -> 
           let {      
                strconf = (css `elems` fss) || (css `elems` pss) ||
                          (fss `elems` pss) ||
                          ((css ++ fss ++ pss) `elems` ss);
                css' = map snd cs;
                fsty = map snd fs; -- [([s0,s0'],s)([s11,s11'],s1)] 
                fssrt = map snd fsty; -- [s,s1] 
                fsar = map fst fsty; -- [[s0,s0'],[s11,s11']]
                fss' = fssrt ++ (concat fsar); 
                pss' = concat $ map snd ps;
                sortcheck = css' `elems` ss && 
                            fss' `elems` ss && 
                            pss' `elems` ss
               }
           in (not strconf) && sortcheck;
     (css,fss,pss,oss) ->
           let { 
                strconf = (css `elems` fss) || (css `elems` pss) || (css `elems` oss) ||
                          (fss `elems` pss) || (fss `elems` oss) ||
                          (pss `elems` oss) ||
                          ((css ++ fss ++ pss ++ oss) `elems` ss);
                css' = map snd cs;
                fsty = map snd fs; -- [([s0,s0'],s)([s11,s11'],s1)] 
                fssrt = map snd fsty; -- [s,s1] 
                fsar = map fst fsty; -- [[s0,s0'],[s11,s11']]
                fss' = fssrt ++ (concat fsar); 
                pss' = concat $ map snd ps;
                oss' = concat $ map snd os;
                sortcheck = css' `elems` ss && 
                            fss' `elems` ss && 
                            pss' `elems` ss && 
                            oss' `elems` ss
               }
           in (not strconf) && sortcheck
    }

