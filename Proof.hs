-- Program : Prawf; version 2020.001
-- Module : Proof (core)
-- Authors : U.Berger, O.Petrovska
-- Date created : 17 Jan 2020
-- Description : Defines data type of a proof, replaces proofs 
          -- and generates an end formula based on the Assumpts and a proof, 
          -- performs appropriate correctness checks
          -- parses proof records (theorems)

{- Revision History  :
Date        Author      Ref    Description
24 01        OP                 updated endFormula for And, Or, All

-}

--Note : introduced left rules by YS (to add extraction 30.01.2020)

module Proof  -- core

  where

-- haskell
import Data.List as L

-- aux
import Perhaps
import ListAux as LA

-- core
import Language

-- ==================================
-- Definitions
 -- type: Label Assumpt AnVar GLSymbol
      --  Proof' (extended Proof definition for IFP')
 -- data: Proof ProofRecord

-- Functions 
 -- endFormula endFormula'
 -- endFAndIntr endFAndElil endFAndElir
 -- endFImpIntr endFImpElim
 -- endFOrIntrl endFOrIntrr endFOrElim 
 -- endFExFQ endFReAA
 -- endFAllIntr endFAllElim endFExIntr endFExElim
 -- endFLetP endFLetP' endFDeclP
 -- endFGInd endFInd endFHSInd endFSInd  
 -- endFGCoi endFCoi endFHSCoi endFSCoi endFClo endFCocl
 -- endFEqRefl endFEqSym endFEqCong endFRep 
 -- transIFP fixpred opformon
 -- monP' monP monF transincl trivincl
 -- substProof
 -- allintros allelims freshVsTs findassulabel 
 -- freeassumptlabels assumpt_formula assumpt_label
 -- checkVarCondition fvInAssumpts letsProof replace
 
 
-- Contents
-- ========

-- Preliminary remarks
-- Type declarations and basic functions
-- Correctness and end formula of a proof (endFormula)
-- Monotonicity; IFP to IFP'
-- Substitution of proofs  
-- Helper functions

-- ==================================
{- 
Gamma, u: A |- g2: B
-----------------------
      Gamma |- g1: A -> B
      
"Gamma" is Assumpts
"u: A" is Assumpt (u, A)
"u" is Label
"A", "B", "A -> B" is Formula
"g1", "g2" is GlSymbol
-}


-- 0. Preliminary remarks
-- ======================
{-
Every proof rule has the form

Gamma1 |- A1 ... Gamman |- An
--------------------------------- Rule(a)
   Gamma |- A

Where a is a vector of parameters (which may be empty) and

Gamma1 |- A1, ... Gamman |- An, Gamma |- A, a

must satisfy a number of conditions that defines the rule.
The conditions on Gamma1, ..., Gamman, Gamma should only depend on
the parameters p but not on the formulas A1,...., An, A.


Examples: in manual.txt

The strategy for endFormula is as follows:

Given: Rule(a), Gamma, proofs p1, ..., pn 

1. Find Gamma1, ..., Gamman as small as possible
so that the condition on Gamma1, ..., Gamman, Gamma, a is satisfied
and FAV(pi) subset Gammai (i = 1,...,n)

2. Use a recursive call of endFormula to find A1, ..., An
such that Gamma1 |- p1:A1 ... Gamman |- pn:An

3. Find A s.t 

Gamma1 |- A1 ... Gamman |- An
--------------------------------- Rule(a)
   Gamma |- A

is a valid instance of the rule.



The strategy for refine is:

Given:  Rule(a), Gamma, A.

1. Find Gamma1, ..., Gamman as large as possible
so that the condition on Gamma1, ..., Gamman, Gamma, a is satisfied.

2. Find A1, ..., An such that

Gamma1 |- A1 ... Gamman |- An
--------------------------------- Rule(a)
   Gamma |- A

is a valid instance of the rule.


Tasks: 

1.Check that endFormula and refine follow the strategies above.

-}


-- Type declarations and basic functions
-- ========================================

-- Definitions

type Label   = String
type Assumpt = (Label, Formula)
type AnVar = Label
type GlSymbol = Label


data Proof = 
    AnProof Label -- AnProof u : A  if u:A is a free assumption
  | AndIntr Proof Proof  -- AndIntr(p:A)(q:B) : A&B
  | AndElil Proof        -- AndElil(p:A&B) : A
  | AndElir Proof        -- AndElir(p:A&B) : B
  | AndLeft Proof Label Label Label 
  | ImpIntr Proof Assumpt -- ImpIntr(p:B)(u:A) : A->B
  | ImpElim Proof Proof   -- ImpElim(p:A->B)(q:A) : B
  | ImpLeft Proof Proof Label Label
  | OrIntrl Proof Formula -- OrIntrl(p:A)B : A or B
  | OrIntrr Proof Formula -- OrIntrr(p:A)B : B or A
  | OrElim Proof Proof Proof -- OrElim(p:A or B)(q:A->C)(r:B->C) : C
  | OrLeft Proof Label Proof Label Label
  | ExFQ Proof Formula -- ExFQ(p:bot)A : A
  | ReAA Proof -- ReAA(p:not not A) : A
  | AllIntr Proof (String,Sort) -- AllIntr(p:A)(x:s) : all(x:s)A
  | AllElim Proof Term -- AllElim(p:all(x:s)A)t : A[t/x]
  | ExIntr Proof (String,Sort) Formula Term -- ExIntr(p:A(t))(x,s)A(x)t : ex(x,s)A(x)
  | ExElim Proof Proof -- ExElim(p:ex(x,s)A)(q: All(y,s)(A->C)) : C  (x not free in C)
  | Cut Proof Proof Assumpt
  | LetP Decl Proof -- to add decl from the let formula into the context
  | DeclP Decl Proof -- to introduce new declarations
  | Ind Predicate Proof -- Ind(mu Phi)(p:Phi P sub P) : mu Phi sub P
  | HSInd Predicate Proof -- HSInd(mu Phi)(p:mu Phi \cap Phi P sub P) : mu Phi sub P
  | SInd Predicate Proof                                                      
  | Coi Predicate Proof -- Coi(nu Phi)(p:P sub Phi P) : P sub nu Phi  
  | HSCoi Predicate Proof 
  | SCoi Predicate Proof
  | Ind' Predicate Proof String String Proof 
-- Ind'(mu Phi)(p:Phi P sub P) X Y (q: X sub Y -> Phi X sub Phi Y) : 
--                                            mu Phi sub P (X, Y not free in Phi)
  | HSInd' Predicate Proof String String Proof 
-- HSInd'(mu Phi)(p:mu Phi \cap Phi P sub P) X Y (q: X sub Y -> Phi X sub Phi Y) : 
--                                            mu Phi sub P (X, Y not free in Phi)
  | SInd' Predicate Proof String String Proof                                               
  | Coi' Predicate Proof String String Proof 
-- Coi'(mu Phi)(p:P sub Phi P) X Y (q: X sub Y -> Phi X sub Phi Y) : 
--                                            P sub nu Phi (X, Y not free in Phi)
  | HSCoi' Predicate Proof String String Proof 
  | SCoi' Predicate Proof String String Proof 
  | Clo Predicate -- Clo(* Phi) : Phi(* Phi) sub * Phi  (* = mu, nu)
  | Cocl Predicate -- Cocl(* Phi) : * Phi sub Phi(* Phi)  (* = mu, nu)
  | EqRefl Term   -- EqRefl t : t = t
  | EqSym Proof   -- EqSym(p:s=t) : t=s
  | EqCong Proof [Proof] Predicate -- EqCong(d:P(rs)(ps:rs=ts)P : P(ts)
  | Rep Formula Proof -- Rep A(d:A) : A
     deriving (Show, Eq, Read)

-- extended Proof definition for IFP'
-- Ind', HSInd', SInd' and Coi', HSCoi', SCoi' are supposed to be used only in Proof'
type Proof' = Proof 

-- used in saving and loading theorems
data ProofRecord = PrRec {
                           prlang  :: Language,
                           prctx   :: Context,
                           praxi   :: [Assumpt],
                           prdecl  :: [Decl],
                           prproof :: Proof
                         }
               deriving (Show, Read)
               
assumpt_formula :: Assumpt -> Formula
assumpt_formula = snd

assumpt_label :: Assumpt -> Label
assumpt_label = fst

-- replace (literally, no avoidance of variable capture)
replace :: Proof -> Label -> Proof -> Proof
replace p u q =
 if not (u `elem` freeassumptlabels p)
 then p
 else 
  case p of
   {
    AnProof av -> q ;  -- we know av = u 
    AndIntr p1 p2 -> AndIntr (replace p1 u q) (replace p2 u q);
    AndElil p1 -> AndElil (replace p1 u q);
    AndElir p1 -> AndElir (replace p1 u q);
    AndLeft p1 u' v w -> AndLeft (replace p1 u q) u' v w;
    ImpIntr p1 (a,f) -> ImpIntr (replace p1 u q) (a,f);
    ImpElim p1 p2 -> ImpElim (replace p1 u q) (replace p2 u q);
    ImpLeft p1 p2 u' v -> ImpLeft (replace p1 u q) (replace p2 u q) u' v;
    OrIntrl p1 f -> OrIntrl (replace p1 u q) f;
    OrIntrr p1 f -> OrIntrr (replace p1 u q) f;
    OrElim p1 p2 p3 -> OrElim (replace p1 u q) (replace p2 u q) (replace p3 u q);
    OrLeft p1 u' p2 v w -> OrLeft (replace p1 u q) u' (replace p2 u q) v w;
    ExFQ p1 f -> ExFQ (replace p1 u q) f;
    ReAA p1 -> ReAA (replace p1 u q);
    AllIntr p1 x -> AllIntr (replace p1 u q) x;
    AllElim p1 t -> AllElim (replace p1 u q) t;
    ExIntr p1 x f t -> ExIntr (replace p1 u q) x f t;
    ExElim p1 p2 -> ExElim (replace p1 u q) (replace p2 u q);
    Cut p1 p2 (a,f) -> Cut (replace p1 u q) (replace p2 u q) (a,f);
    LetP d p1 -> LetP d (replace p1 u q);
    DeclP d p1 -> DeclP d (replace p1 u q);
    Ind pred p1 -> Ind pred (replace p1 u q);
    HSInd pred p1 -> HSInd pred (replace p1 u q);
    SInd pred p1 -> SInd pred (replace p1 u q);
    Coi pred p1 -> Coi pred (replace p1 u q);
    HSCoi pred p1 -> HSCoi pred (replace p1 u q);
    SCoi pred p1 -> SCoi pred (replace p1 u q);
    Ind' pred p1 x y p2 -> Ind' pred (replace p1 u q) x y (replace p2 u q);
    HSInd' pred p1 x y p2 -> HSInd' pred (replace p1 u q) x y (replace p2 u q); 
    SInd' pred p1 x y p2 -> SInd' pred (replace p1 u q) x y (replace p2 u q); 
                                             
    Coi' pred p1 x y p2 -> Coi' pred (replace p1 u q) x y (replace p2 u q);
                                                 
    HSCoi' pred p1 x y p2 -> HSCoi' pred (replace p1 u q) x y (replace p2 u q); 
                                               
    SCoi' pred p1 x y p2 -> SCoi' pred (replace p1 u q) x y (replace p2 u q);    
    Clo pred -> Clo pred; 
    Cocl pred -> Cocl pred;
    EqRefl t -> EqRefl t;
    EqSym p1 -> EqSym (replace p1 u q); 
    EqCong p1 ps pred -> 
       EqCong (replace p1 u q) [replace pi u q | pi <- ps] pred ; 
    Rep f p1 -> Rep f (replace p1 u q)
   }

-- 2. Correctness and end formula of a proof
-- =========================================
             
endFormula :: Language -> Context -> [Assumpt] -> 
                  Proof -> Perhaps Formula 
                                 -- to update everywhere add decls
endFormula l ctx assumpts p = 
  if correctL l
  then if correctC l ctx
       then if and (correctFs l ctx (map snd assumpts))
            then endFormula' l ctx [] assumpts p
            else fail("Assumption formulas are not correct. ")    
       else fail("Context is not correct with respect to the language. ")
  else fail("Language is not correct. ")

endFormula' :: Language -> Context -> [Decl] -> [Assumpt] -> 
                  Proof -> Perhaps Formula 
endFormula' l ctx decls assumpts p = 
  case p of
   {
    AnProof av -> look av assumpts;
    AndIntr p1 p2 -> endFAndIntr l ctx decls assumpts p1 p2;
    AndElil p1 -> endFAndElil l ctx decls assumpts p1;
    AndElir p1 -> endFAndElir l ctx decls assumpts p1;
    AndLeft p u v w -> endFAndLeft l ctx decls assumpts p u v w;
    ImpIntr p1 avf -> endFImpIntr l ctx decls assumpts p1 avf;
    ImpElim p1 p2 -> endFImpElim l ctx decls assumpts p1 p2;
    ImpLeft p q u v -> endFImpLeft l ctx decls assumpts p q u v;
    OrIntrl p1 f -> endFOrIntrl l ctx decls assumpts p1 f;
    OrIntrr p1 f -> endFOrIntrr l ctx decls assumpts p1 f;
    OrElim p1 p2 p3 -> endFOrElim l ctx decls assumpts p1 p2 p3;
    OrLeft p u q v w -> endFOrLeft l ctx decls assumpts p u q v w;
    ExFQ p1 f -> endFExFQ l ctx decls assumpts p1 f;
    ReAA p1 -> endFReAA l ctx decls assumpts p1;
    AllIntr p1 (x,s) -> endFAllIntr l ctx decls assumpts p1 (x,s);
    AllElim p1 t -> endFAllElim l ctx decls assumpts p1 t;
    ExIntr p1 (x,s) f t -> endFExIntr l ctx decls assumpts p1 (x,s) f t;
    ExElim p1 p2 -> endFExElim l ctx decls assumpts p1 p2;
    Cut p q avf -> endFCut l ctx decls assumpts p q avf;
    LetP decl p1 -> endFLetP l ctx decls assumpts decl p1;
    DeclP decl p1 -> endFDeclP l ctx decls assumpts decl p1;
    Ind pred p1 -> endFInd l ctx decls assumpts pred p1;
    HSInd pred p1 -> endFHSInd l ctx decls assumpts pred p1;
    SInd pred p1 -> endFSInd l ctx decls assumpts pred p1;
    Coi pred p1 -> endFCoi l ctx decls assumpts pred p1;
    HSCoi pred p1 -> endFHSCoi l ctx decls assumpts pred p1;
    SCoi pred p1 -> endFSCoi l ctx decls assumpts pred p1;
    Ind' pred p1 x y p2 -> endFInd l ctx decls assumpts pred p1;-- incomplete 
    HSInd' pred p1 x y p2 -> endFHSInd l ctx decls assumpts pred p1;-- incomplete 
    SInd' pred p1 x y p2 -> endFSInd l ctx decls assumpts pred p1;-- incomplete
    Coi' pred p1 x y p2 -> endFCoi l ctx decls assumpts pred p1; -- incomplete 
    HSCoi' pred p1 x y p2 -> endFHSCoi l ctx decls assumpts pred p1;-- incomplete 
    SCoi' pred p1 x y p2 -> endFSCoi l ctx decls assumpts pred p1;-- incomplete 
    Clo pred -> endFClo l ctx decls assumpts pred; 
    Cocl pred -> endFCocl l ctx decls assumpts pred;
    EqRefl t -> endFEqRefl t;
    EqSym p1 -> endFEqSym  l ctx decls assumpts p1;
    EqCong p1 ps pred -> endFEqCong l ctx decls assumpts p1 ps pred ;
    Rep f p1 -> endFRep l ctx decls assumpts f p1
   }

-- endFormula sub-functions
endFAndIntr :: Language -> Context -> [Decl] -> 
                 [Assumpt] -> Proof -> Proof -> Perhaps Formula
endFAndIntr l ctx decls assumpts p1 p2 =
  do {
      f1 <- endFormula' l ctx decls assumpts p1;
      f2 <- endFormula' l ctx decls assumpts p2;
      return (And f1 f2)
     }

endFAndElil :: Language -> Context -> [Decl] -> [Assumpt] -> Proof -> Perhaps Formula 
endFAndElil l ctx decls assumpts p1 =
  do {
      f <- endFormula' l ctx decls assumpts p1;
      let {f' = normDeclF decls f};
      case f' of
       {
        And f1 f2 -> return f1;
        _ -> fail ("AndElil, formula doesn't fit: " ++ show f)
       }
      }

endFAndLeft :: Language-> Context -> [Decl] -> [Assumpt] -> Proof -> Label -> Label -> Label -> Perhaps Formula
endFAndLeft l ctx decls assumpts p u v w =
  do{
     h <- look w assumpts;
     case h of
       {
         And f g -> endFormula' l ctx decls ((u,f) : (v,g) : assumpts) p;
         _ -> fail "endAndLeft: Assumption w does not fit."
       }
    }
      
endFAndElir :: Language -> Context -> [Decl] -> [Assumpt] -> Proof -> Perhaps Formula 
endFAndElir l ctx decls assumpts p1 =
  do {
      f <- endFormula' l ctx decls assumpts p1;
      case f of
       {
        And f1 f2 -> return f2;
        _ -> fail ("AndElir, formula doesn't fit: " ++ show f)
       }
      }
      
endFImpIntr :: Language -> Context -> [Decl] -> 
                 [Assumpt] -> Proof -> Assumpt -> Perhaps Formula
endFImpIntr l ctx decls assumpts p1 avf =
      if correctF l ctx (snd avf)
      then do {
               f <- endFormula' l ctx decls (avf:assumpts) p1;
               return (Imp (snd avf) f)
              }
      else fail ("ImpIntr, avf formula " ++ show avf ++ "is not correct!")
     
     
endFImpElim :: Language -> Context -> [Decl] -> 
                 [Assumpt] -> Proof -> Proof -> Perhaps Formula
endFImpElim l ctx decls assumpts p1 p2 =
  do {
      f1 <- endFormula' l ctx decls assumpts p1;
      f2 <- endFormula' l ctx decls assumpts p2;
      let {f1' = normDeclF decls f1};                               
      case f1' of
       {
        Imp f11 f12 ->
          if equalDeclFormula l ctx decls f11 f2  
          then return f12
          else fail ("ImpElim, formulae don't fit: " ++ show f1 ++ ",\n " ++ show f2);
        _ -> fail ("ImpElim, formula doesn't fit: " ++ show f1)
       }
      }

endFImpLeft :: Language -> Context -> [Decl] -> [Assumpt] -> Proof -> Proof -> Label -> Label -> Perhaps Formula
endFImpLeft l ctx decls assumpts p q u w =
  do{
     h <- look w assumpts;
     case h of
       {
         Imp f g -> do {
                        f' <- endFormula' l ctx decls assumpts p;
                        if equalDeclFormula l ctx decls f' f
                        then endFormula' l ctx decls ((u,g) : assumpts) q
                        else fail "endImpLeft: f' and f are not equal."
                       };
         _ -> fail "endImpLeft: Assumption w does not fit."
        }
    }

endFOrIntrl :: Language -> Context -> [Decl] -> 
                 [Assumpt] -> Proof -> Formula -> Perhaps Formula
endFOrIntrl l ctx decls assumpts p1 f =
  do {
      f1 <- endFormula' l ctx decls assumpts p1;
      return (Or f1 f)
     }
     
endFOrIntrr :: Language -> Context -> [Decl] -> 
                 [Assumpt] -> Proof -> Formula -> Perhaps Formula
endFOrIntrr l ctx decls assumpts p1 f =
  do {
      f1 <- endFormula' l ctx decls assumpts p1;
      return (Or f f1)
     }

endFOrElim :: Language -> Context -> [Decl] -> 
                 [Assumpt] -> Proof -> Proof -> Proof -> Perhaps Formula
endFOrElim l ctx decls assumpts p1 p2 p3 =
  do {
      f1 <- endFormula' l ctx decls assumpts p1;
      f2 <- endFormula' l ctx decls assumpts p2;
      f3 <- endFormula' l ctx decls assumpts p3;
      let {f1' = normDeclF decls f1};
      case (f1',f2,f3) of
       {
        (Or f11 f12, Imp f21 f22, Imp f31 f32) ->  
          if (equalDeclFormula l ctx decls f11 f21)&&
             (equalDeclFormula l ctx decls f12 f31)&&
             (equalDeclFormula l ctx decls f22 f32) 
          then return f32
          else fail ("1. OrElim, formulae don't fit " ++ 
                     show f1' ++ ", "++ show f2 ++ ", " ++ show f3);
        _ -> fail ("2. OrElim, formulae don't fit " ++ 
                   show f1' ++ ", "++ show f2 ++ ", " ++ show f3)
       }
      }

endFOrLeft :: Language -> Context -> [Decl] -> [Assumpt] -> Proof -> Label -> Proof -> Label -> Label -> Perhaps Formula
endFOrLeft l ctx decls assumpts p u q v w =
  do{
     h <- look w assumpts;
     case h of
       {
         Or f g -> do{
                      c <- endFormula' l ctx decls ((u,f) : assumpts) p;
                      d <- endFormula' l ctx decls ((v,g) : assumpts) q;
                      if equalDeclFormula l ctx decls c d
                      then return c
                      else fail "endOrLeft: c and d are different."
                     };
         _ -> fail "endOrLeft: Assumption w does not fit."
       }
    }

endFExFQ :: Language -> Context -> [Decl] -> 
               [Assumpt] -> Proof -> Formula -> Perhaps Formula
endFExFQ l ctx decls assumpts p1 f =
  do {
      f1 <- endFormula' l ctx decls assumpts p1;
      case f1 of
       {
        Bot -> return f;
        _   -> fail ("ExFQ, formula doesn't fit: "++ show f1)
       }
      }
      
endFReAA :: Language -> Context -> [Decl] -> [Assumpt] -> Proof -> Perhaps Formula
endFReAA l ctx decls assumpts p1 =
  do {
      f <- endFormula' l ctx decls assumpts p1;
      let {f' = normDeclF decls f};
      case f' of
       {
        Imp (Imp f Bot) Bot -> return f;
        _  -> fail ("ReAA, formula doesn't fit: "++ show f')
       }
      }
      
endFAllIntr :: Language -> Context -> [Decl] -> 
                  [Assumpt] -> Proof -> (String,Sort) -> Perhaps Formula
endFAllIntr l ctx decls assumpts p1 (x,s) =
   -- x must not occur free in any free assumption used at this point!
      if isSortL l s
      then do {
               f <- endFormula' l (addCVars ctx [(x,s)]) decls assumpts p1;
               let { 
                     assumpts' = 
                       [(u,g) | 
                          (u,g) <- assumpts, 
                          u `elem` freeassumptlabels p1]
                   } ;
               if not (checkVarCondition x assumpts')
               then fail ("AllIntr, variable condition violated: ")
               else return (All (x,s) f)
              }               
      else fail ("No sort " ++ s ++ " in the language.")

endFAllElim :: Language -> Context -> [Decl] -> 
                 [Assumpt] -> Proof -> Term -> Perhaps Formula
endFAllElim l ctx decls assumpts p1 t =
 do {
     s' <- toPerhaps (sortT l ctx t);
     if isSortL l s'
     then do {
              f <- endFormula' l ctx decls assumpts p1;
              let {f' = normDeclF decls f};
              case f' of
              {
               All (x,s) g -> if s == s'
                              then return (substituteFormula x t g)
                              else fail ("AllElim, sorts don't match.");
               _ -> fail ("AllElim, formula doesn't fit: " ++ show f')
              }
             }
     else fail ("No sort for the given term found in the language.")
     }
     
endFExIntr :: Language -> Context -> [Decl] -> 
                [Assumpt] -> Proof -> (String,Sort) -> Formula -> Term -> Perhaps Formula
endFExIntr l ctx decls assumpts p1 (x,s) f t =
  if isSortL l s && toPerhaps (sortT l ctx t) == Success s
  then do {
           f1 <- endFormula' l ctx decls assumpts p1;
           let f' = substituteFormula x t f in
           if equalDeclFormula l ctx decls f' f1 
           then return (Ex (x,s) f)  
           else fail ("ExInr, formula doesn't fit: " ++ show f1)
          }
  else fail ("ExIntr, sorts don't match or no sort in the language.")
     
endFExElim :: Language -> Context -> [Decl] -> 
                [Assumpt] -> Proof -> Proof -> Perhaps Formula
endFExElim l ctx decls assumpts p1 p2 =
  do { -- ExElim (p: ex x A(x))  (q: All y (A(y) -> C))  x not in C
       -- x must not occur free in C!
      f1 <- endFormula' l ctx decls assumpts p1;
      f2 <- endFormula' l ctx decls assumpts p2;
      let {f1' = normDeclF decls f1;
          f2' = normDeclF decls f2};
      case (f1', f2') of 
       {
        (Ex (x,s) fa, All (y,s') (Imp fa' fc)) -> 
          if equalDeclFormula l ctx decls (All (x,s) fa) (All (y,s') fa') && not (free y fc) 
          then return fc
          else fail ("Formulas don't match: " 
                     ++ x ++ " is not free in ");
        _ -> fail ("ExElim, formulas don't fit: " ++ (show f1') ++ "\n " ++ (show f2'))
       }
      }

endFCut :: Language -> Context -> [Decl] -> [Assumpt] -> Proof -> Proof -> Assumpt -> Perhaps Formula
endFCut l ctx decls assumpts p q avf@(u,f) =
  do {
       c <- endFormula' l ctx decls assumpts p;
       if equalDeclFormula l ctx decls c f
       then endFormula' l ctx decls (avf : assumpts) q
       else fail "endCut: Left premise does not fit cut formula."
      }

endFLetP :: Language -> Context -> [Decl] -> [Assumpt] -> Decl -> Proof -> Perhaps Formula
endFLetP l ctx decls assumpts dec p = 
  if correctD l ctx dec 
  then if declvarinctx dec ctx
       then endFLetP' l ctx (dec:decls) assumpts dec p
       else let ctx' = extendCtx ctx dec
            in endFLetP' l ctx' (dec:decls) assumpts dec p
  else fail ("Let : Declaration is not correct " ++ show dec)

endFLetP' :: Language -> Context -> [Decl] -> [Assumpt] -> Decl -> Proof -> Perhaps Formula
endFLetP' l ctx decls assumpts dec p =
       do {f <- endFormula' l ctx decls assumpts p; 
           return (LetF dec f)
          }
  
endFDeclP :: Language -> Context -> [Decl] -> [Assumpt] -> Decl -> Proof -> Perhaps Formula
endFDeclP l ctx decls assumpts dec p = 
  if correctD l ctx dec--ls dec
  then do{
          decls' <- Success [d | d <- decls, d /= dec];
          f <- endFormula' l ctx decls' assumpts p; 
          return f
         }
  else fail ("Decl : Declaration is not correct")
         
endFInd, endFHSInd, endFSInd :: Language -> Context -> [Decl] -> 
              [Assumpt] -> Predicate -> Proof -> Perhaps Formula
endFInd = endFGInd "norm"
endFHSInd = endFGInd "hs"
endFSInd = endFGInd "strong"

endFGInd :: String -> Language -> Context -> [Decl] -> 
              [Assumpt] -> Predicate -> Proof -> Perhaps Formula
endFGInd option l ctx decls assumpts pred p = 
 case lookupPred decls pred of
  {
   Mu o -> 
      do {f <- endFormula' l ctx decls assumpts p; 
          let (xss,g) = splitquant f  
          in case g of
              { Imp g1 g2 ->
                  let { 
                       pred' = Compr xss g2 ; --P = \x.g2
                       ctx' = addCVars ctx xss;
                       xss' = map Var (map fst xss);
                       h = Predic pred xss' -- mu o x
                      }
                  in case option of 
                      { "norm" -> if equalDeclFormula l ctx' decls -- o P = g1
                                    (Predic (OpApp o pred') xss') 
                                    g1 
                                  then incl l ctx pred pred' -- mu o subset P
                                  -- incl in Language.hs
                                  else fail "1: induction rule cannot be applied. ";
                        "hs"   -> case g1 of 
                                   {
                                    And g11 g12 -> -- g11 = mu o x ; g12 = o P x
                                      if equalDeclFormula l ctx' decls 
                                        (Predic (OpApp o pred') xss') 
                                         g12 --decls -- CHECK
                                         &&  
                                         equalDeclFormula l ctx' decls 
                                         h g11
                                      then incl l ctx pred pred' -- incl in Language.hs
                                      else fail "1: half-strong induction rule cannot be applied. " ;
                                    _ -> fail "2: half-strong ind not applicable"
                                    };
                        "stong" -> if equalDeclFormula l ctx' decls 
                                    (Predic (OpApp o (Compr xss (And h g2))) xss') 
                                     g1 
                                   then incl l ctx pred pred' -- incl in Language.hs
                                   else fail "1: induction rule cannot be applied. ";
                        };
                   _ -> fail "3: induction rule cannot be applied."
                 }
              };
   _ -> fail "4: induction rule cannot be applied."
  }


endFCoi, endFHSCoi, endFSCoi :: Language -> Context -> [Decl] -> 
              [Assumpt] -> Predicate -> Proof -> Perhaps Formula
endFCoi = endFGCoi "norm"
endFHSCoi = endFGCoi "hs"
endFSCoi = endFGCoi "strong"

endFGCoi :: String -> Language -> Context -> [Decl] -> 
             [Assumpt] -> Predicate -> Proof -> Perhaps Formula
endFGCoi option l ctx decls assumpts pred p = case (lookupPred decls pred) of
  {
   Nu o -> 
     do {f <- endFormula' l ctx decls assumpts p; -- all xs (Phi P xs -> P xs)
         let (xss,g) = splitquant f
         in case g of
             { Imp g1 g2 -> 
                let { 
                     pred' = Compr xss g1 ;
                     ctx' = addCVars ctx xss;
                     xss' = map Var (map fst xss);
                     h = Predic pred xss'
                     }
                in case option of 
                    { "norm" -> if equalDeclFormula l ctx' decls 
                                  (Predic (OpApp o pred') xss') 
                                  g2
                                then incl l ctx pred' pred
                                else fail ("coinduction rule cannot be applied. ");
                      "hs"   -> case g2 of 
                                  {
                                   Or g11 g12 ->
                                     if equalDeclFormula l ctx' decls 
                                        (Predic (OpApp o pred') xss') 
                                         g11
                                         && 
                                         equalDeclFormula l ctx' decls 
                                          h g12
                                     then incl l ctx pred' pred
                                     else fail ("1: half-strong coinduction rule cannot be applied. ");
                                     _ -> fail "2: half-strong coinduction not applicable"
                                   };
                       "strong" -> if equalDeclFormula l ctx' decls 
                                      (Predic (OpApp o (Compr xss (Or g1 h))) xss') 
                                       g2
                                   then incl l ctx pred' pred
                                   else fail ("strong coinduction rule cannot be applied. ");
                       _ -> fail ("3: coinduction rule cannot be applied.")
                     }
                }};
   _ -> fail ("4: coinduction rule cannot be applied.")
  }


endFClo :: Language -> Context -> [Decl] -> [Assumpt] -> Predicate -> Perhaps Formula
endFClo l ctx decls assumpts pred = case (lookupPred decls pred) of
  {
   Mu o -> incl l ctx (OpApp o pred) pred;
   Nu o -> incl l ctx (OpApp o pred) pred;
    _ -> fail ("closure rule cannot be applied, predicate is not a fixed point")
   }
  

endFCocl :: Language -> Context -> [Decl] -> [Assumpt] -> Predicate -> Perhaps Formula
endFCocl l ctx decls assumpts pred = case (lookupPred decls pred) of
  {
   Nu o -> incl l ctx pred (OpApp o pred);
   Mu o -> incl l ctx pred (OpApp o pred);
    _ -> fail ("closure rule cannot be applied, predicate is not a fixed point") 
   }
endFEqRefl :: Term -> Perhaps Formula
endFEqRefl t = Success (Predic (PrConst "=") [t, t])
   
endFEqSym :: Language -> Context -> [Decl] -> [Assumpt] -> 
                                       Proof -> Perhaps Formula
endFEqSym l ctx decls assumpts p = 
  do {
       f <- endFormula' l ctx decls assumpts p ;
       (r,t) <- equation f ;         -- equation in Language.hs
       return (mkEquation t r)       -- mkEequation in Language.hs
     }

endFEqCong :: Language -> Context -> [Decl] -> [Assumpt] -> Proof -> 
                  [Proof] -> Predicate -> Perhaps Formula
endFEqCong l ctx decls assumpts p ps pred = 
  let { 
        eF = endFormula' l ctx decls assumpts ; 
        aP = toPerhaps . arityP l ctx ; -- avoid toPerhaps (in Perhaps.hs)
        sT = toPerhaps . sortT l ctx ;  -- by using always Perhaps
        eqDF = equalDeclFormula l ctx decls 
      } 
  in do {
          f <- eF p ;  
          fs <- sequence (map eF ps) ;
          rts <- sequence (map equation fs) ;  
          let { rs = map fst rts ; ts = map snd rts } ;
          a <- aP pred ;
          ar <- sequence (map sT rs) ;
          at <- sequence (map sT ts) ;
          if ar == a && at == a && eqDF f (predApp pred rs)
          then return (predApp pred ts)
          else fail ("endFEqCong1: " ++ 
                     showL ["ar = ", show ar, 
                            "a = ", show a, "at = ", show at,
                            "f = ", show f, 
                            "f' =", show (predApp pred rs),
                            "g =", show (predApp pred ts)])
        }   

endFRep :: Language -> Context -> [Decl] -> [Assumpt] ->  
                           Formula -> Proof -> Perhaps Formula
endFRep l ctx decls assumpts f p =
  do { 
       g <- endFormula' l ctx decls assumpts p ;
       if equalDeclFormula l ctx decls f g
       then return f
       else fail ("Proof.hs endFRep:\n" ++ 
                   show f ++ "\n" ++ show g)
     }  
  

-- Monotonicity; IFP to IFP'
-- =====================================
-- translates IFP proofs into IFP' proofs, i.e. ind and coind with monotonicity
transIFP:: Language -> Context -> [Decl] -> Proof -> Proof'
transIFP l ctx decl p = case p of
  {
    AnProof av -> AnProof av;
    AndIntr p1 p2 -> AndIntr (transIFP l ctx decl p1) (transIFP l ctx decl p2);
    AndElil p1 -> AndElil (transIFP l ctx decl p1);
    AndElir p1 -> AndElir (transIFP l ctx decl p1);
    AndLeft p1 u v w -> AndLeft (transIFP l ctx decl p1) u v w;
    ImpIntr p1 avf -> ImpIntr (transIFP l ctx decl p1) avf;
    ImpElim p1 p2 -> ImpElim (transIFP l ctx decl p1) (transIFP l ctx decl p2);
    ImpLeft p1 p2 u v -> ImpLeft (transIFP l ctx decl p1) (transIFP l ctx decl p2) u v;
    OrIntrl p1 f -> OrIntrl (transIFP l ctx decl p1) f;
    OrIntrr p1 f -> OrIntrr (transIFP l ctx decl p1) f;
    OrElim p1 p2 p3 -> OrElim 
                         (transIFP l ctx decl p1) 
                         (transIFP l ctx decl p2) 
                         (transIFP l ctx decl p3);
    OrLeft p1 u p2 v w -> OrLeft (transIFP l ctx decl p1) u (transIFP l ctx decl p2) v w;
    ExFQ p1 f -> ExFQ (transIFP l ctx decl p1) f;
    ReAA p1 -> ReAA (transIFP l ctx decl p1);
    AllIntr p1 x -> AllIntr (transIFP l (addCVars ctx [x]) decl p1) x;
    AllElim p1 t -> AllElim (transIFP l ctx decl p1) t;
    ExIntr p1 x f t -> ExIntr (transIFP l ctx decl p1) x f t;
    ExElim p1 p2 -> ExElim 
                      (transIFP l ctx decl p1) 
                      (transIFP l ctx decl p2);
    Cut p1 p2 avf -> Cut (transIFP l ctx decl p1) (transIFP l ctx decl p2) avf;
    LetP d p1 -> LetP d (transIFP l ctx decl p1);
    DeclP d p1 -> DeclP d (transIFP l ctx decl p1);
    Ind pred p1 -> 
      let { (ctx',(x,y),p2) = opformon l ctx decl pred } 
      in Ind' pred (transIFP l ctx decl p1) x y p2;
    HSInd pred p1 -> 
      let { (ctx',(x,y),p2) = opformon l ctx decl pred } 
      in HSInd' pred (transIFP l ctx decl p1) x y p2;
    SInd pred p1 -> 
      let { (ctx',(x,y),p2) = opformon l ctx decl pred } 
      in SInd' pred (transIFP l ctx decl p1) x y p2;                    
    Coi pred p1 -> 
      let { (ctx',(x,y),p2) = opformon l ctx decl pred }
      in Coi' pred (transIFP l ctx decl p1) x y p2;
    HSCoi pred p1 -> 
      let { (ctx',(x,y),p2) = opformon l ctx decl pred } 
      in HSCoi' pred (transIFP l ctx decl p1) x y p2;
    SCoi pred p1 -> 
      let { (ctx',(x,y),p2) = opformon l ctx decl pred } 
      in SCoi' pred (transIFP l ctx decl p1) x y p2;                                                
    Clo pred -> Clo pred; 
    Cocl pred -> Cocl pred;
    EqRefl t -> EqRefl t;
    EqSym p1 -> EqSym (transIFP l ctx decl p1); 
    EqCong p1 ps pred -> 
       EqCong (transIFP l ctx decl p1) (map (transIFP l ctx decl) ps) pred ; 
    Rep f p1 -> Rep f (transIFP l ctx decl p1)      
  }

-- fixpred decls pred assumes that pred is of the form @ (OpA (x,ar) pred'), 
-- where @ is Mu or Nu, and computes ((x,ar),pred')
fixpred :: [Decl] -> Predicate -> ((String,Arity),Predicate)
fixpred decls pred =
  let {
        op = case lookupPred decls pred of  -- lookupPred in Language.hs
              {
                Mu o -> o ;
                Nu o -> o ;
                _ -> error "Proof.fixpred: not a fixed point predicate"
              } 
      }
  in case lookupOperator decls op of -- lookupOperator in Language.hs
      {
        OpA (x,ar) pred' -> ((x,ar), pred') ;
        _ -> error "Proof.fixpred: not a predicate abstraction"
      }

-- Mon Phi = X sub Y -> Phi(X) sub Phi(Y)
-- all xs (X xs -> Y xs) -> all xs (Phi X xs -> Phi Y xs)
-- previously called mon
-- opformon l ctx decls pred assumes pred is of the form @ OpA (x:ar) pred'
-- where @ is Mu or Nu, and computes a proof of x sub y -> x pred' sub pred'[y/x] 
-- with fresh y 
opformon :: Language -> Context -> [Decl] -> Predicate -> ( Context,(String, String), Proof')
opformon l ctx decls pred =
  let {
        ((x,ar),pred') = fixpred decls pred ;
        y = freshVar "Y" (x:allVarsP pred') ;
        ctx' = addCPVars ctx [(x,ar),(y,ar)] ;
        Success xsuby = incl l ctx' (PrVar x) (PrVar y) ; 
        u = "xsuby"; -- may be a problem if we call again ! Check!
        p = ImpIntr (monP' l ctx' decls pred' ar [((x,y),(u, xsuby))] x y) (u, xsuby)
      }   
  in (ctx',(x,y),p)

-- monP' l ctx decls pred ar x y assumes pred is s.p. in x:ar and y:ar is fresh,
-- and computes a proof of pred sub pred[y/x] from the assumption ("u",x sub y)
monP' :: Language -> Context -> [Decl] -> Predicate -> Arity ->
   [((String,String),Assumpt)] -> String -> String -> Proof'
monP' l ctx decls pred ar xyassu x y = 
  let {
        xs = createVars (length ar); 
        ys = fvpred pred;
        zs = freshVars xs ys;
        vxs = map Var zs 
      }
  in allintros 
          (zip xs ar)
          (ImpIntr 
             (monP l ctx decls pred xyassu x y vxs (AnProof "monPv")) -- OK
             ("monPv", predApp pred vxs))     

 
-- monP l ctx decls pred x y ts p assumes that pred is s.p. in x, 
-- and computes a proof of pred[y/x] ts from the assumption 
-- "u" : x sub y and a proof p : pred ts where it assumed that 
-- the sorts of ts coincide with the arity of pred
monP :: Language -> Context -> [Decl] -> 
          Predicate -> [((String,String), Assumpt)] ->
          String -> String -> [Term] -> 
          Proof' -> Proof' -- u: label for x sub y
monP l ctx decls pred xyassu x y ts p =
  if not (x `elem` (fpvpred pred))
  then p
  else 
    case pred of 
      {
        PrVar pv -> 
           let {Success u = findassulabel (x,y) xyassu} in
          ((AnProof u) `allelims` ts) `ImpElim` p ;  
        PrConst _ -> error "monP, constant"; 
                     -- impossible since x free in pred 
        Compr _ _ -> monF l ctx decls (predApp pred ts) xyassu x y p; 
        Mu o  -> 
          case lookupOperator decls o of
           {OpA (z,ar) pred' -> 
              let {(z',zs,rs) = freshVsTs z (x:y:(allVarsP pred')) 
                                  (length ar) (fvpred pred');
                   sub3 = mksubstP [(x,PrVar y)];
                   pred3 = substP sub3 pred; -- P
                   sub1 = mksubstP [(z, pred3)];
                   pred1 = substP sub1 pred'; -- Q[P/Z]
                   sub2 = addP sub1 [(x,PrVar y)];
                   pred2 = substP sub2 pred'; -- Q[P/Z][X/Y]
                   p0 = monP' l ctx decls pred' ar xyassu x y; -- Q subs Q[Y/X]
                   sub4 = mksubstP [(z,pred3)];
                   p1 = substProof sub4 p0;
                   p2 = Clo pred3;
                   p' = transincl ar pred1 pred2 pred3 p1 p2;
                   ctx' = (addCPVars ctx [(z,ar),(z',ar)]);
                   u = freshVar "u" (map fst(map snd xyassu));
                   Success zsubz' =
                        incl l ctx' (PrVar z) (PrVar z') ; 
                   xyassu' = ((z,z'),(u,zsubz')):xyassu} 
               in ImpElim 
                    (allelims 
                      (Ind' pred p' z z'
                        (monP' l ctx' decls pred' ar xyassu' z z')) 
                      ts)
                    p; 
             _ -> error "monP, Mu o, o is a variable"};
        Nu o  -> let {o' = substO (mksubstP [(x,PrVar y)]) o} in
          case lookupOperator decls o' of
            {OpA (z,ar) pred' -> 
              let {(z',zs,rs) = freshVsTs z (x:y:(allVarsP pred')) 
                                  (length ar) (fvpred pred');
                   sub3 = mksubstP [(y,PrVar x)];
                   pred3 = substP sub3 pred; -- P
                   sub1 = mksubstP [(z, pred3)];
                   pred1 = substP sub1 pred'; -- Q[P/Z]
                   sub2 = addP sub1 [(y,PrVar x)];
                   pred2 = substP sub2 pred'; -- Q[P/Z][X/Y]
                   p0 = monP' l ctx decls pred' ar xyassu y x; -- Q subs Q[Y/X]
                   sub4 = mksubstP [(z,pred3)];
                   p1 = substProof sub4 p0;
                   p2 = Cocl pred3;
                   p' = transincl ar pred1 pred2 pred3 p1 p2;
                   ctx' = (addCPVars ctx [(z,ar),(z',ar)]);
                   u = freshVar "u" (map fst(map snd xyassu));
                   Success zsubz' =
                        incl l ctx' (PrVar z) (PrVar z') ; 
                   xyassu' = ((z,z'),(u,zsubz')):xyassu} 
               in ImpElim 
                    (allelims 
                      (Coi' pred p' z z'
                        (monP' l ctx' decls pred' ar xyassu' z z')) 
                      ts)
                    p; 
           _ -> error "monP, Nu o, o is a variable"};
        OpApp o pred' -> 
          let {o' = lookupOperator decls o;
               pred'' = operApp o' pred'} in 
          case pred'' of {
               OpApp _ _ -> error "monP, opapp";
               _ -> monP l ctx decls pred'' xyassu x y ts p}
     }
  
-- monF l ctx decls f x y p assumes that f is s.p. in x, 
-- and computes a proof of f[y/x] from "u" : x sub y and p : f
monF :: Language -> Context -> [Decl] -> 
          Formula -> [((String,String),Assumpt)] -> String -> String -> Proof' -> Proof'
monF l ctx decls f xyassu x y p =
 if not (x `elem` (fpvform f))
 then p
 else case f of 
  {
    Predic pred ts -> monP l ctx decls pred xyassu x y ts p;
    And g1 g2 -> 
      AndIntr
        (monF l ctx decls g1 xyassu x y (AndElil p)) 
        (monF l ctx decls g2 xyassu x y (AndElir p)) ;
    Or g1 g2 -> 
     let {g1' = substF (mksubstP [(x,PrVar y)]) g1;
          g2' = substF (mksubstP [(x,PrVar y)]) g2;
          v1 = freshVar "v1" (freeassumptlabels p);
          v2 = freshVar "v2" (freeassumptlabels p)} in
      (OrElim
         p
         (ImpIntr 
            (OrIntrl (monF l ctx decls g1 xyassu x y (AnProof v1)) g2')
            (v1,g1))
         (ImpIntr 
            (OrIntrr (monF l ctx decls g2 xyassu x y (AnProof v2)) g1')
            (v2,g2))) ;
    Imp g1 g2 -> 
      let { w = freshVar "w" (freeassumptlabels p)} in
       ImpIntr
         (monF l ctx decls g2 xyassu x y (ImpElim p (AnProof w)))
         (w,g1) ;  -- g1[y/x] = g1 since g1 does not contain x 
    Bot -> error "monF: Bot does not contain x" ;
    All z@(z0,s) g1 -> 
      AllIntr
        (monF l (addCVars ctx [z]) decls g1 xyassu x y (AllElim p (Var z0))) 
        z ;
    Ex z@(z0,s) g1 -> --v = g: ex x Ax; g1 Ax
      let { g1' = substF (mksubstP [(x,PrVar y)]) g1;
           w = freshVar "w" (freeassumptlabels p)} in
         ExElim 
           p 
           (AllIntr
              (ImpIntr 
                 (ExIntr  
                     (monF l ctx decls g1 xyassu x y (AnProof w))
                     z g1' (Var z0))
                  (w, g1))
               z);
    LetF d g1 -> monF l ctx (d:decls) g1 xyassu x y p;
  }    
  
-- transitivity of inclusion
-- we assume that all predicates have the same arity ar
-- p1 proves pred1 subs pred2 
-- p2 proves pred2 subs pred3
-- returns proof of pred1 subs pred3
transincl :: Arity -> Predicate -> Predicate -> Predicate ->
     Proof -> Proof -> Proof
transincl ar pred1 pred2 pred3 p1 p2 = 
  let {fvp1 = allVarsP pred1;
       fvp2 = allVarsP pred2;
       fvp3 = allVarsP pred3;
       xs = createVars (length ar); 
       ys = unions [fvp1, fvp2, fvp3];
       zs = freshVars xs ys;
       zsar = zip zs ar;
       u = freshVar "uti" (freeassumptlabels p1 `L.union` 
                           freeassumptlabels p2);
       ts = map Var zs} 
  in allintros 
       zsar
       (ImpIntr 
          (ImpElim
             (allelims p2 ts)
             (ImpElim 
                (allelims p1 ts)
                (AnProof u)))
          (u, predApp pred1 ts))
   
-- trivial inclusion.
trivincl :: Language -> Context -> Predicate -> Proof'
trivincl l ctx pred = case pred of 
  {
   Compr xs f -> allintros xs (ImpIntr (AnProof "u") ("u", f)); -- ??
   _ -> let { fvp = fvpred pred;
              arp = case arityP l ctx pred of {Just ar -> ar; _ -> error "trivinc"};
              xs = freshVars (xlist (length arp) "x") fvp;
              f = Predic pred (map Var xs)} 
        in allintros (zip xs arp) (ImpIntr (AnProof "u") ("u",f))
                    
   }

-- for lemma 16: If p:A from ssumptions Gamma, 
-- then p[P/X]:A[P/X] from assumptions Gamma[P/X].

-- Substitution of proofs
-- =====================================
substProof :: Substitution -> Proof' -> Proof'
substProof theta p =
 case p of -- adding predicate to other substitutions
  {
    AnProof av -> AnProof av; 
    AndIntr p1 p2 -> AndIntr (substProof theta p1) (substProof theta p2);
    AndElil p1 -> AndElil (substProof theta p1);
    AndElir p1 -> AndElir (substProof theta p1);
    AndLeft p1 u v w -> AndLeft (substProof theta p1) u v w;
    ImpIntr p1 avf -> let {a = fst avf; f = snd avf} 
                      in ImpIntr (substProof theta p1) (a,substF theta f); 
    ImpElim p1 p2 -> ImpElim (substProof theta p1) (substProof theta p2);
    ImpLeft p1 p2 u v -> ImpLeft (substProof theta p1) (substProof theta p2) u v;
    OrIntrl p1 f -> OrIntrl (substProof theta p1) (substF theta f);
    OrIntrr p1 f -> OrIntrr (substProof theta p1) (substF theta f);
    OrElim p1 p2 p3 -> OrElim 
                         (substProof theta p1) 
                         (substProof theta p2) 
                         (substProof theta p3);
    OrLeft p1 u p2 v w -> OrLeft (substProof theta p1) u (substProof theta p2) v w;
    ExFQ p1 f -> ExFQ (substProof theta p1) (substF theta f);
    ReAA p1 -> ReAA (substProof theta p1);
    AllIntr p1 (x,s) -> 
         let {(x',theta') = freshSubst (x,theta)}
         in AllIntr (substProof theta' p1) (x',s); 
    AllElim p1 t -> AllElim (substProof theta p1) t;
    ExIntr p1 (x,s) f t -> 
         let {(x',theta') = freshSubst (x,theta);
               f' = substF theta' f;
               t' = substT theta t} 
         in ExIntr (substProof theta p1) (x',s) f' t';
    ExElim p1 p2 -> ExElim (substProof theta p1) (substProof theta p2);
    Cut p1 p2 avf -> let {a = fst avf; f = snd avf} 
                     in Cut (substProof theta p1) (substProof theta p2) (a,substF theta f);
    LetP d p1 -> LetP (substD theta d) (substProof theta p1);
    DeclP d p1 -> DeclP (substD theta d) (substProof theta p1); 
    Ind' pred p1 x y p2 -> 
       Ind' (substP theta pred) (substProof theta p1) x y (substProof theta p2); 
                                            
                                             
    HSInd' pred p1 x y p2 -> 
       HSInd' (substP theta pred) (substProof theta p1) x y (substProof theta p2); 
                                                   -- x y may clash with theta! 
    SInd' pred p1 x y p2 -> 
       SInd' (substP theta pred) (substProof theta p1) x y (substProof theta p2);
    Coi' pred p1 x y p2 -> 
       Coi' (substP theta pred) (substProof theta p1) x y (substProof theta p2);
                                           
                                                   -- x y may clash with theta! 
    HSCoi' pred p1 x y p2 -> 
       HSCoi' (substP theta pred) (substProof theta p1) x y (substProof theta p2);
    SCoi' pred p1 x y p2 -> 
       SCoi' (substP theta pred) (substProof theta p1) x y (substProof theta p2);
    Ind pred p1 -> 
       Ind (substP theta pred) (substProof theta p1) ;  
    HSInd pred p1 -> 
       HSInd (substP theta pred) (substProof theta p1) ;  
    SInd pred p1 -> 
       SInd (substP theta pred) (substProof theta p1) ;
    Coi pred p1 -> 
       Coi (substP theta pred) (substProof theta p1) ;  
    HSCoi pred p1 -> 
       HSCoi (substP theta pred) (substProof theta p1) ;
    SCoi pred p1 -> 
       SCoi (substP theta pred) (substProof theta p1) ;
    Clo pred -> Clo (substP theta pred); 
    Cocl pred -> Cocl (substP theta pred);
    EqRefl t -> EqRefl (substT theta t);
    EqSym p1 -> EqSym (substProof theta p1); 
    EqCong p1 ps pred -> 
       EqCong (substProof theta p1) (map (substProof theta) ps) (substP theta pred) ; 
    Rep f p1 -> Rep (substF theta f) (substProof theta p1)      
  }              

           
-- Helper functions
-- adding or removing quantification in bulk
allintros :: [(String,Sort)] -> Proof' -> Proof'
allintros [] p = p
allintros (x:xs) p = AllIntr (allintros xs p) x

allelims :: Proof' -> [Term] -> Proof'
allelims p [] = p
allelims p (t:ts) = AllElim (allelims p ts) t
  
-- helper for monP to get fresh variables and terms
-- freshVsTs z (x:y:(allVarsP pred')) (length ar) (fvpred pred)
-- z' = freshVar z (x:y:(allVarsP pred));
-- xs = createVars (length ar); 
-- ys = fvpred pred;
-- zs = freshVars xs ys;
-- rs = map Var zs;
freshVsTs :: String -> [String] -> 
            Int -> [String] -> (String,[String],[Term])
freshVsTs x xs i ys = 
  let {x' = freshVar x xs;
       xs' = createVars i; 
       ys' = freshVars xs' ys;
       rs = map Var ys'}
  in (x',ys',rs)
  
findassulabel :: (String, String) -> [((String,String), Assumpt)] ->
                  Perhaps String
findassulabel xy xyassu = 
   case getLR xyassu xy of 
    {Just (u, f) -> Success u;
    _ -> Failure ("findassulabel, assumption not found") }
    
-- free assumption labels in a proof
freeassumptlabels :: Proof -> [Label]
freeassumptlabels p = aux [] p  where
  aux us p =  -- us is the list of bound assumption labels
    case p of
      {
        AnProof u -> [ u | not (u `elem` us)] ;
        AndIntr p1 p2 -> aux us p1 `LA.union` aux us p2 ;
        AndElil p1 -> aux us p1 ;
        AndElir p1 -> aux us p1 ;
        AndLeft p1 u v w -> u:v:w: aux us p1;
        ImpIntr p1 (u,_) -> aux (u : us) p1 ;
        ImpElim p1 p2 -> aux us p1 `LA.union` aux us p2 ;
        ImpLeft p1 p2 u v -> u:v: aux us p1 `LA.union` aux us p2;
        OrIntrl p1 _ -> aux us p1 ;
        OrIntrr p1 _ -> aux us p1 ;
        OrElim p1 p2 p3 -> LA.unions (map (aux us) [p1,p2,p3]) ;
        OrLeft p1 u p2 v w -> u:v:w: aux us p1 `LA.union` aux us p2;
        ExFQ p1 _ -> aux us p1 ;
        ReAA p1 -> aux us p1 ;
        AllIntr p1 _ -> aux us p1 ;
        AllElim p1 _  -> aux us p1 ;
        ExIntr p1 _ _ _ -> aux us p1 ;
        ExElim p1 p2 -> aux us p1 `LA.union` aux us p2 ;
        Cut p1 p2 _ -> aux us p1 `LA.union` aux us p2;
        LetP _ p1 -> aux us p1 ;
        DeclP _ p1 -> aux us p1 ;
        Ind _ p1 -> aux us p1 ;
        HSInd _ p1 -> aux us p1 ;
        SInd _ p1 -> aux us p1 ;
        Ind' _ p1 _ _ p2 -> aux us p1 `LA.union` aux us p2;
        HSInd' _ p1 _ _ p2 -> aux us p1 `LA.union` aux us p2;
        SInd' _ p1 _ _ p2 -> aux us p1 `LA.union` aux us p2;
        Coi _ p1 -> aux us p1 ;
        HSCoi _ p1 -> aux us p1 ;    
        SCoi _ p1 -> aux us p1 ;
        Coi' _ p1 _ _ p2 -> aux us p1 `LA.union` aux us p2;
        HSCoi' _ p1 _ _ p2 -> aux us p1 `LA.union` aux us p2;
        SCoi' _ p1 _ _ p2 -> aux us p1 `LA.union` aux us p2;
        Clo _ -> [] ;                               
        Cocl _ -> [] ;
        EqRefl _ -> [] ;
        EqSym p1 -> aux us p1 ; 
        EqCong p1 ps _ -> LA.unions (map (aux us) (p1:ps)) ;
        Rep _ p1 ->  aux us p1
      }

-- checks if a variable v is free in any of the assumptions
checkVarCondition :: String -> [Assumpt] -> Bool
checkVarCondition v assumpts =
  not $ any (free v) (map snd $ filter (\(l, _) -> head l /= '?') assumpts)

-- returns a list of all free variables in the list of assumptions
fvInAssumpts :: [Assumpt] -> [String]
fvInAssumpts assumpts =
  let {
       assumpts' = filter (\(l, _) -> head l /= '?') assumpts
      }
  in LA.unions (map (fv.snd) assumpts')
  
-- adds declarations via LetP
letsProof :: [Decl] -> Proof -> Proof
letsProof decls p = foldr LetP p decls
