-- Program : Prawf; version 2020.001
-- Module : Parser (core)
-- Authors : U.Berger, O.Petrovska
-- Date created : 17 Jan 2020
-- Description : A library of monadic parser combinators

{- Revision History  :
Date        Author      Ref    Description
-}

module Parser

 where

import qualified Control.Applicative as Appl
import Control.Monad
import Perhaps

-- ==========================
-- Definitions
 -- data: Parser Orientation
 
-- Instances
 -- Functor Parser / Appl.Applicative Parser / Monad Parser
 
-- Functions 
 -- runP runP0 runPerhaps result bind bind_ 
 -- zero plus item sat char bindp 
 -- string digit lower upper letter alphanum
 -- many many1 mymaybe times lab word neword
 -- ident identifier nat first pluss plusss
 -- space dot parse token plusst
 -- strings tstrings tstring
 -- optional parsePair
 -- stringassoc stringassocs tstringassocs
 -- tidentifier parseStrings spacesOnly
 -- chainr1 chainp
 -- bracket inbrackets
 -- sepby sepbyn
 -- nelisting listing listingn 
 -- lambdaabstr setcompr abstraction
 -- prefix prefixb prefixbn prefix1
 -- infixN infixL infixR infix
 -- p1 p1b p1bn
 -- copy (+++) shift replacefront1 replacefront repl
 -- lift0 lift1 lift2 liftl lift1l modenv
 -- sepby0 sepby1 optlist
 -- anybutcolon anybutcomma name
 

data Parser a = P (String -> [(a,String)]) 

runP :: Parser a -> String -> [(a,String)]
runP (P f) = f 

runP0 :: Parser a -> String -> a
runP0 (P f) inp = 
  let { res = f inp } 
  in case [x | (x,"") <- res] of 
        { 
          (x:_) ->  x ; 
           _    ->  error ("runP0: " ++ inp ++ "\n" ++ show (map snd res))
        }

runPerhaps :: Parser a -> String -> Perhaps a
runPerhaps (P f) inp =
  let { res = f inp } 
  in case [x | (x,"") <- res] of 
        { 
          (x:_) ->  Success x ; 
           _    ->  Failure ("runPerhaps: " ++ show (map snd res))
        }

result :: a -> Parser a
result v = P (\inp -> [(v,inp)])

bind :: Parser a -> (a -> Parser b) -> Parser b
p `bind` f = 
  P (\inp -> concat [runP (f v) inp' | (v,inp') <- runP p inp])

bind_ :: Parser a -> Parser b -> Parser b
p `bind_` q = p `bind` \_ -> q

zero :: Parser a
zero = P (\inp -> [])

plus :: Parser a -> Parser a -> Parser a
p `plus` q = P (\inp -> (runP p inp ++ runP q inp))

item :: Parser Char
item = P (\inp -> case inp of 
                  {
                   [] -> []; 
                   (x:xs) -> [(x,xs)]
                  })
               
sat :: (Char -> Bool) -> Parser Char
sat test = item `bind` \x -> if test x then result x else zero

char :: Char -> Parser Char
char x = sat (x ==)

instance Functor Parser where
  fmap f (P fa) = P ((map (\(a, s) -> (f a, s))) . fa)
  
instance Appl.Applicative Parser where
     pure = result
     (<*>) = ap
     m *> k = bind m (\_ -> k)
     
instance Monad Parser where
     return = result
     (>>=)  = bind

-- Delivering pairs and lists

bindp :: Monad m => m a -> m b -> m (a,b)
bindp mx my = do { x <- mx ; y <- my ; return (x,y) }

-- some further basic parsers 

string :: String -> Parser String
string ""     = return ""
string (x:xs) = do {char x; string xs; return (x:xs)}

digit, lower, upper, letter, alphanum :: Parser Char
digit = sat (\c ->  '0' <= c && c <= '9')
lower = sat (\c ->  'a' <= c && c <= 'z')
upper = sat (\c ->  'A' <= c && c <= 'Z')
letter = lower `plus` upper
alphanum = letter `plus` digit

-- iteration

many :: Parser a -> Parser [a]
many p = do {x<- p; xs <- many p; return (x:xs)} `plus` 
         do {string ""; return []}  -- ?? should be same as return []

many1 :: Parser a -> Parser [a]
many1 p = do {x<- p; xs<- many p; return (x:xs)}

mymaybe :: Parser a -> Parser (Maybe a)
mymaybe p = do { x  <- p; return (Just x) } `plus` return Nothing

-- iteration, a fixed number of times

times :: Int -> Parser a -> Parser [a]
times n p = if n <= 0 
            then return []
            else do {x <- p ; xs <- times (n-1) p ; return (x:xs)}

-- words, identifiers, numbers 
 
lab :: Parser String
lab = token $ many alphanum 

word :: Parser String
word = many letter

neword :: Parser String
neword = many1 letter

ident :: Parser String
ident = do {x<- lower; xs <- many alphanum; return (x:xs)}

identifier :: Parser String
identifier = do {
                  xs <- neword; 
                  us  <- many (char '_'); 
                  ys <- many alphanum; 
                  zs <- many (char '\'');
                  let {
                        ys' = if null us || null ys 
                              then ys 
                              else ("_{" ++ ys ++ "}")
                      } ;
                  return (xs ++ ys' ++ zs)
                }

nat :: Parser Int
nat = do {xs <- many1 digit; return (read xs)}

-- only first result (improves efficiency)

first :: Parser a -> Parser a
first p = P (\inp-> case runP p inp of 
                     {
                      [] -> []; 
                      (x:xs) -> [x]
                     })

pluss :: Parser a -> Parser a -> Parser a
p `pluss` q = first (p `plus` q)

-- running a list of parsers in parallel

plusss :: [Parser a] -> Parser a
plusss = foldr plus zero

-- space

space, dot :: Parser ()
space = do { sat (`elem` " \n\t") ; return () }
dot = do { char '.' ; return () }

-- spaces :: Parser ()
-- spaces = do {many1 (sat (`elem` " \n\t")); return ()}

parse, token :: Parser a -> Parser a
parse p = do {many space; p}
token p = do {x<- p; many space; return x}

plusst :: Parser a -> Parser a -> Parser a
p `plusst` q = token (p `pluss` q)

strings, tstrings :: [String] -> Parser String
strings strs = plusss (map string strs)
tstrings = token . strings

tstring = token . string

-- optionally applying a function

optional :: (Parser a -> Parser a) -> Parser a -> Parser a
optional f p = f p `pluss` p

-- Pairs

parsePair :: Parser a -> Parser b -> Parser (a,b)
parsePair p q = optional inbrackets $
     do { x <- p; tstrings [":",","] ; y <- q; return (x,y) }

-- Association lists

stringassoc :: (String,a) -> Parser (String,a)
stringassoc (str,x) = string str >> return (str,x)

stringassocs :: [(String,a)] -> Parser (String,a)
stringassocs sxs = plusss (map stringassoc sxs)
tstringassocs = token . stringassocs

tidentifier :: Parser String
tidentifier = token identifier

parseStrings :: [String] -> Parser String
parseStrings strs = plusss (map (parse . string) strs)

spacesOnly :: String -> Bool
spacesOnly = and . map (== ' ') 

-- from now on all parsers p satisfy p = token p, that is, p eats all trailing spaces
-- respectively functions preserve this property

-- chaining, right associative

chainr1 :: Parser a -> Parser (a -> a -> a) -> Parser a
p `chainr1` op = 
  do {x<- p; 
      do {f<- op; y<- p `chainr1` op; return (f x y)} `pluss` return x
     }

-- chaining, with priorities

chainp :: Parser a -> Parser (Int,(a -> a -> a)) -> Parser a
p `chainp` op = 
  do { 
       x<- p; 
       do { 
            (n,f)<- op; 
            do { 
                 y<- p ; 
                 (m,g) <- op; 
                 z <- p `chainp` op; 
                 return (if n<m then (x `f` (y `g` z)) 
                                else ((x `f` y) `g` z))
               } 
            `pluss`
            do {(_,f)<- op; y<- p ; return (f x y)}
          } 
       `pluss` 
       return x
      }

-- bracketing

bracket :: Parser a -> Parser b -> Parser c -> Parser b
bracket open p close = do {open; x<- p; close; return x}

inbrackets :: Parser a -> Parser a
inbrackets p =  bracket (tstring "(") p (tstring ")") `pluss`
                bracket (tstring "[") p (tstring "]") `pluss`
                bracket (tstring "{") p (tstring "}") 

-- separation by symbol (comma, semicolon, etc.)

sepby :: Parser b -> Parser a -> Parser [a]
sepby sep p =
  do { 
       x <- p ;
       do { sep; xs <- sepby sep p; return (x:xs)} `pluss` return [x] }

-- listing

nelisting :: Parser a -> Parser [a]
nelisting p = do { x <- p ; return [x] }
              `plus`
              inbrackets (sepby (parse (tstring ",")) p)

listing :: Parser a -> Parser [a]
listing p = nelisting p `pluss` token (return [])


-- fixed number, separation by symbol (comma, semicolon, etc.)

sepbyn :: Int -> Parser b -> Parser a -> Parser [a]
sepbyn n sep p =
  if n <= 1
  then do { x <- p ; return [x] }
  else do {x <- p; sep; xs <- sepbyn (n-1) sep p; return (x:xs)}

-- fixed number listing

listingn :: Int -> Parser a -> Parser [a]
listingn n = sepbyn n (tstring ",")

-- abstraction

lambdaabstr :: Parser a -> Parser ([String],a)
lambdaabstr p = do {
                     tstrings ["\\","lambda"] ;
                     xs <- nelisting tidentifier ;
                     token (mymaybe dot); 
                     u <- p ;
                     return (xs,u)
                 }

setcompr :: Parser a -> Parser ([String],a)
setcompr p = 
  inbrackets (do { xs <- nelisting tidentifier ; 
                   tstring "|" ; u <- p ; 
                   return (xs,u) })

abstraction :: Parser a -> Parser ([String],a)
abstraction p = lambdaabstr p `plus` setcompr p                  

-- prefix with fixed arity
-- can be written f arg1 ... argn  (without brackets)

prefix :: Parser ([a] -> b , Int) -> Parser a -> Parser b
prefix pref p = do { (f,n) <- pref ; as <- n `times` p ; return (f as) }

-- prefixb no fixed arity 
-- but arguments in brackets exept if zero or one argument

prefixb :: Parser ([a] -> b) -> Parser a -> Parser b
prefixb pref p = do { f <- pref ; as <- listing p ; return (f as) }

-- prefixbn: fixed arity, arguments in brackets 

prefixbn :: Parser (Int,[a] -> b) -> Parser a -> Parser b
prefixbn pref p = do { 
                       (n,f) <- pref ; 
                       as <- inbrackets (listingn n p) ; 
                       return (f as) 
                     }

-- prefix1: arity 1, argument without brackets 

prefix1 :: Parser (a -> b) -> Parser a -> Parser b
prefix1 pref p = do { 
                      f <- pref ; 
                      a <- p ; 
                      return (f a) 
                     }

-- infix, not associative

infixN :: Parser (a -> b -> c) -> Parser a -> Parser b -> Parser c
infixN inf p q = do { x <- p ; f <- inf ; y <- q ; return (f x y) }


-- infix, left associative

infixL :: Parser (a -> b -> a) -> Parser a -> Parser b -> Parser a
infixL inf p q = p >>= aux   where

-- aux :: a -> Parser a
   aux x = do { f <- inf ; y <- q ; aux (f x y) `plus` return (f x y) }

-- infix, right associative

infixR :: Parser (a -> b -> b) -> Parser a -> Parser b -> Parser b
infixR inf p q = do { x <- p ; f <- inf ; 
                      y <- (infixR inf p q `plus` q) ; return (f x y) }

-- infix, any orientation

data Orientation = N | L | R

infi :: Orientation -> 
         Parser (a -> a -> a) -> Parser a -> Parser a -> Parser a
infi o = case o of {N -> infixN ; L -> infixL ; R -> infixR}


-- a parser for 
-- * prioritised infix operations, each either left assoc, right assoc, 
--   or without assoc
-- * prefix operations, each with with fixed arity and with higher priority
--   than any infix operation 
-- * disambiguation by parentheses

-- Example: if A,B,C,D,E are nullary, ! is unary, ->, |, & are 
-- infix prioritised in that order, -> right assoc, |, & left assoc, then
-- !!(!A->B&C|D|E) parses to !(!(->(!(A),|(|(&(B,C),D),E)))) 

p1 :: (Int,Int) -> 
      Parser ([a] -> a, Int) -> 
      [(Parser (a -> a -> a) , Int, Orientation)] ->  
         Parser a
p1 (minBind,maxBind) pref infs = pup minBind  

 where

-- pup :: Int -> Parser a
   pup k = plusss (brack : ppref : 
             [pinf p n o | (p,n,o) <- infs , n >= k, n <= maxBind ])   

-- brack :: Parser a
   brack = inbrackets (pup minBind)

-- ppref :: Parser a
   ppref = prefix pref (brack `plus` ppref)


-- pinf :: Parser a -> Int -> Orientation -> Parser a
   pinf p n o = let {q = pup (n+1)} in infi o p q q

-- same with prefix without fixed number of arguments

p1b :: (Int,Int) -> 
       Parser ([a] -> a) -> 
--       Parser ([b] -> a) -> 
       [(Parser (a -> a -> a) , Int, Orientation)] ->  
          Parser a
p1b (minBind,maxBind) pref infs = pup minBind  
 where
-- pup :: Int -> Parser a
   pup k = plusss (brack : ppref : 
             [pinf p n o | (p,n,o) <- infs , n >= k, n <= maxBind ])   
-- brack :: Parser a
   brack = inbrackets (pup minBind)
-- ppref :: Parser a
   ppref = prefixb pref (brack `plus` ppref)
-- pinf :: Parser a -> Int -> Orientation -> Parser a
   pinf p n o = let {q = pup (n+1)} in infi o p q q

-- same with prefix functions of fixed numbers of arguments
p1bn :: (Int,Int) -> 
        Parser a ->
--        Parser (Int,[a] -> a) -> 
        [(Parser (a -> a -> a) , Int, Orientation)] ->  
           Parser a
p1bn (minBind,maxBind) atom infs = pup minBind  
-- p1bn (minBind,maxBind) atom pref infs = pup minBind  
 where
-- pup :: Int -> Parser a
   pup k = plusss 
             ( 
               inbrackets (pup minBind) :
               atom : 
--               prefixbn pref (pup minBind) : 
               [let {q = pup (n+1)} in infi o p q q | 
                   (p,n,o) <- infs , n >= k, n <= maxBind]
             )   

----------------------------------------
-- Substitution

copy :: Parser String
copy = P(\inp -> [(inp,"")])

(+++) :: Parser String -> Parser String -> Parser String
p +++ q = do { str1 <- p ; str2 <- q ; return (str1 ++ str2) }

shift :: Parser String -> Parser String
shift p = do { c <- item ; str <- p ; return (c:str) }

replacefront1 :: (String,String) -> Parser String
replacefront1 (old,new) = do {string old ; return new} 

replacefront :: [(String,String)] -> Parser String
replacefront subst = first (plusss (map replacefront1 subst))

repl :: [(String,String)] -> Parser String
repl subst =
  (replacefront subst +++ repl subst)
  `pluss`
  shift (repl subst)
  `pluss` 
  copy

-----------------------------
-- Contextual information

-- Pointwise lifting

lift0 :: a -> env -> a
lift0 x = const x

lift1 :: (a -> b) -> ((env -> a) -> (env -> b))
lift1 f x = f . x

lift2 :: (a -> b -> c) -> ((env -> a) -> (env -> b) -> (env -> c))
lift2 f x y = \env-> f (x env) (y env)

liftl :: ([a] -> b) -> ([env -> a] -> (env -> b))
liftl f xs = \env-> f [x env | x <- xs]

lift1l :: (a -> [b] -> c) -> ((env -> a) -> [env -> b] -> (env -> c))
lift1l f x ys = \env-> f (x env) [y env | y <- ys]

-- Modifying the environment

modenv :: (env -> env') -> (env' -> a) -> (env -> a)
modenv phi x = x . phi

-- separation
sepby0 :: Parser a -> Parser b -> Parser [a]
p `sepby0` sep  = (p `sepby1` sep) `plus` return []

sepby1 :: Parser a -> Parser b -> Parser [a]
p `sepby1` sep = do x  <- p
                    xs <- many (do {sep; p})
                    return (x:xs)
     
optlist :: Parser [a] -> Parser [a]
optlist p = p `plus` do {string ""; return []}

-- helper functions for parsing predicates and operators,
anybutcolon :: Parser Char
anybutcolon = sat (\c ->  c /= ':' && c /= ' ')

anybutcomma :: Parser Char
anybutcomma = sat (\c ->  c /= ',' && c /= ' ')

name :: Parser String
name = many1 anybutcolon




